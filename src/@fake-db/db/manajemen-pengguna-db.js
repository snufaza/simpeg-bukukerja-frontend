import mock from './../mock';
import {FuseUtils} from '@fuse';
// import _ from '@lodash';

const manajemenPenggunaDB = {
    mpengguna: [
        {
            'id'                        : '5725a680b3249760ea21de52',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680606588342058356d',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a68009e20d0a9e9acf2a',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6809fdd915739187ed5',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a68007920cf75051da64',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a68031fdbb1db2c1af47',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680bc670af746c435e2',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680e7eb988a58ddf303',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680dcb077889f758961',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6806acf030f9341e925',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680ae1ae9a3c960d487',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680b8d240c011dd224b',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a68034cb3968e1f79eac',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6801146cce777df2a08',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6808a178bfd034d6ecf',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680653c265f5c79b5a9',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680bbcec3cc32a8488a',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6803d87f1b77e17b62b',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680e87cb319bd9bd673',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6802d10e277a0f35775',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680aef1e5cf26dd3d1f',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680cd7efa56a45aea5d',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a680fb65c91a82cb35e2',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a68018c663044be49cbf',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        },
        {
            'id'                        : '5725a6809413bf8a0a5272b1',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        }
    ],
    user    : [
        {
            'id'                        : '5725a6802d10e277a0f35724',
            'nik'                       : '84623868638523537356',
            'nama'                      : 'Dina Sulastri',
            'tempat_lahir'              : 'Jakarta', 
            'tanggal_lahir'             : '1992-11-15', 
            'jenis_kelamin'             : 'Perempuan',
            'alamat'                    : 'Jl. Menteng Raya, No. 10 Jakarta Pusat',
            'telepon'                   : '081536879967',
            'nip'                       : '24514353536454732334',
            'jabatan'                   : 'PNS',
            'email'                     : 'dinasulastri@gmail.com',
            'password'                  : 'dina9872',
            'status_approval'           : 'Disetujui',
            'upload_ktp'                : 'ktpsaya.jpg',
            'upload_surat_rekomendasi'  : 'surat-rekomendasi02838393.pdf',
            'upload_gambar_tandatangan' : 'tandatangan4373489689.jpg'
        }
    ]
};

mock.onGet('/api/manajemen-pengguna-app/manajemen-pengguna').reply((config) => {
    const id = config.params.id;
    let response = [];
    switch ( id )
    {
        case 'frequent':
        {
            response = manajemenPenggunaDB.mpengguna.filter(pengguna =>
                manajemenPenggunaDB.user[0].frequentContacts.includes(pengguna.id)
            );
            break;
        }
        default:
        {
            response = manajemenPenggunaDB.mpengguna;
        }
    }
    return [200, response];
});

mock.onGet('/api/manajemen-pengguna-app/user').reply((config) => {
    return [200, manajemenPenggunaDB.user[0]];
});

mock.onPost('/api/manajemen-pengguna-app/add-per-manajemen-pengguna').reply((request) => {
    const data = JSON.parse(request.data);
    manajemenPenggunaDB.mpengguna = [
        ...manajemenPenggunaDB.mpengguna, {
            ...data.newPerManajemenPengguna,
            id: FuseUtils.generateGUID()
        }
    ];
    return [200, manajemenPenggunaDB.mpengguna];
});

mock.onPost('/api/manajemen-pengguna-app/update-per-manajemen-pengguna').reply((request) => {
    const data = JSON.parse(request.data);

    manajemenPenggunaDB.mpengguna = manajemenPenggunaDB.mpengguna.map((pengguna) => {
        if ( data.pengguna.id === pengguna.id )
        {
            return data.pengguna
        }
        return pengguna
    });

    return [200, manajemenPenggunaDB.mpengguna];
});

mock.onPost('/api/manajemen-pengguna-app/remove-per-manajemen-pengguna').reply((request) => {
    const data = JSON.parse(request.data);

    manajemenPenggunaDB.mpengguna = manajemenPenggunaDB.mpengguna.filter((pengguna) => data.penggunaId !== pengguna.id);

    return [200, manajemenPenggunaDB.mpengguna];
});

mock.onPost('/api/manajemen-pengguna-app/remove-manajemen-pengguna').reply((request) => {
    const data = JSON.parse(request.data);
    manajemenPenggunaDB.mpengguna = manajemenPenggunaDB.mpengguna.filter((pengguna) => !data.penggunaIds.includes(pengguna.id));
    return [200, manajemenPenggunaDB.mpengguna];
});