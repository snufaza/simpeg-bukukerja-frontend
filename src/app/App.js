/** @format */

import "@fake-db";
import React from "react";
import { FuseAuthorization, FuseLayout, FuseTheme } from "@fuse";
import Provider from "react-redux/es/components/Provider";
import { Router } from "react-router-dom";
import jssExtend from "jss-extend";
import history from "@history";
import { Auth } from "./auth";
import store from "./store";
import AppContext from "./AppContext";
import routes from "./fuse-configs/routesConfig";
import { create } from "jss";
import {
	StylesProvider,
	jssPreset,
	createGenerateClassName
} from "@material-ui/styles";
import axios from "axios";

const jss = create({
	...jssPreset(),
	plugins: [...jssPreset().plugins, jssExtend()],
	insertionPoint: document.getElementById("jss-insertion-point")
});

const generateClassName = createGenerateClassName();

// axios.defaults.baseURL = "http://localhost:8000";
axios.defaults.baseURL = "http://apittde.pendidikan.gunungkidulkab.go.id";
// apittde.pendidikan.gunungkidulkab.go.id
// axios.defaults.baseURL = 'http://localhost:8000';
// axios.defaults.baseURL = window.location.port == 3000 ? 'http://localhost:8000' : 'http://simpeg.pendidikan.gunungkidulkab.go.id/buku-kerja-backend/index.php';
//  const baseUrl = window.location.port == 3000 ? 'http://epak.lc' : '';
const App = () => {
	return (
		<AppContext.Provider
			value={{
				routes
			}}>
			<StylesProvider jss={jss} generateClassName={generateClassName}>
				<Provider store={store}>
					<Auth>
						<Router history={history}>
							<FuseAuthorization>
								<FuseTheme>
									<FuseLayout />
								</FuseTheme>
							</FuseAuthorization>
						</Router>
					</Auth>
				</Provider>
			</StylesProvider>
		</AppContext.Provider>
	);
};

export default App;
