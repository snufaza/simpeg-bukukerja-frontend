import React from 'react';

export const RagamHiasConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/ragam-hias/:ragam_hias_id/:nama?',
            component   : React.lazy(() => import('./PerRagamHias'))
        },
        {
            path        : '/data-referensi/ragam-hias/baru',
            component   : React.lazy(() => import('./PerRagamHias'))
        },
        {
            path        : '/data-referensi/ragam-hias',
            component   : React.lazy(() => import('./RagamHias'))
        }
    ]
};