import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[RAGAM HIAS APP] GET OBJECT';
export const SAVE_OBJECT = '[RAGAM HIAS APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[RAGAM HIAS APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/ragam_hias/perragam_hias', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/ragam_hias/create', {...data});
    history.push('/data-referensi/ragam-hias/');
    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Ragam Hias Telah Diubah.'}));

                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/ragam_hias/update', {...data});
    history.push('/data-referensi/ragam-hias/');
    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Ragam Hias Telah Diubah.'}));

                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/ragam_hias/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Ragam Hias Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        ragam_hias_id       : '',
        nama                : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}