import React from 'react';

export const KeutuhanKoleksiConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/keutuhan-koleksi/:keutuhan_koleksi_id/:nama?',
            component   : React.lazy(() => import('./PerKeutuhanKoleksi'))
        },
        {
            path        : '/data-referensi/keutuhan-koleksi/baru',
            component   : React.lazy(() => import('./PerKeutuhanKoleksi'))
        },
        {
            path        : '/data-referensi/keutuhan-koleksi',
            component   : React.lazy(() => import('./KeutuhanKoleksi'))
        }
    ]
};