import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[KEUTUHAN KOLEKSI APP] GET OBJECT';
export const SAVE_OBJECT = '[KEUTUHAN KOLEKSI APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[KEUTUHAN KOLEKSI APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/keutuhan_koleksi/perkeutuhan_koleksi', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/keutuhan_koleksi/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keutuhan Koleksi Telah Diubah.'}));
                history.push('/data-referensi/keutuhan-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/keutuhan_koleksi/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keutuhan Koleksi Telah Diubah.'}));
                history.push('/data-referensi/keutuhan-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/keutuhan_koleksi/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keutuhan Koleksi Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        keutuhan_koleksi_id     : '',
        nama                    : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}