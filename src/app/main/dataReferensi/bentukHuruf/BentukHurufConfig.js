import React from 'react';

export const BentukHurufConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/bentuk-huruf/:bentuk_huruf_id/:nama?',
            component   : React.lazy(() => import('./PerBentukHuruf'))
        },
        {
            path        : '/data-referensi/bentuk-huruf/baru',
            component   : React.lazy(() => import('./PerBentukHuruf'))
        },
        {
            path        : '/data-referensi/bentuk-huruf',
            component   : React.lazy(() => import('./BentukHuruf'))
        }
    ]
};