import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[BAHASA APP] GET OBJECT';
export const SAVE_OBJECT = '[BAHASA APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[BAHASA APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/bentuk_huruf/perbentuk_huruf', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/bentuk_huruf/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Bentuk Huruf Telah Diubah.'}));
                history.push('/data-referensi/bentuk-huruf/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/bentuk_huruf/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Bentuk Huruf Telah Diubah.'}));
                history.push('/data-referensi/bentuk-huruf/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/bentuk_huruf/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Bentuk Huruf Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        bentuk_huruf_id      : '',
        nama                : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}