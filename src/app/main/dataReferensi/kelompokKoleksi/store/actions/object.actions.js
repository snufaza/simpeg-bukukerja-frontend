import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[KELOMPOK KOLEKSI APP] GET OBJECT';
export const SAVE_OBJECT = '[KELOMPOK KOLEKSI APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[KELOMPOK KOLEKSI APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/kolompok_koleksi/perkelompokkoleksi', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/kolompok_koleksi/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Koleksi Telah Diubah.'}));
                history.push('/data-referensi/kelompok-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/kolompok_koleksi/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Koleksi Telah Diubah.'}));
                history.push('/data-referensi/kelompok-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/kolompok_koleksi/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Koleksi Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        klasifikasi_id      : '',
        nama                : '',
        kode_warna          : '',
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}