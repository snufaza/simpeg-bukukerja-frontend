import React from 'react';

export const KelompokKoleksiConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/kelompok-koleksi/:kelompok_koleksi_id/:nama?',
            component   : React.lazy(() => import('./PerKelompokKoleksi'))
        },
        {
            path        : '/data-referensi/kelompok-koleksi/baru',
            component   : React.lazy(() => import('./PerKelompokKoleksi'))
        },
        {
            path        : '/data-referensi/kelompok-koleksi',
            component   : React.lazy(() => import('./KelompokKoleksi'))
        }
    ]
};