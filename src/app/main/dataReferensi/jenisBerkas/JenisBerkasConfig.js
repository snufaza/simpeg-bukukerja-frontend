import React from 'react';

export const JenisBerkasConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/jenis-berkas/:jenis_berkas_id/:nama?',
            component   : React.lazy(() => import('./PerJenisBerkas'))
        },
        {
            path        : '/data-referensi/jenis-berkas/baru',
            component   : React.lazy(() => import('./PerJenisBerkas'))
        },
        {
            path        : '/data-referensi/jenis-berkas',
            component   : React.lazy(() => import('./JenisBerkas'))
        }
    ]
};