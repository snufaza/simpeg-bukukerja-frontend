import React, {useEffect, useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/styles';
import {Table, TableBody, TableCell, TablePagination, TableRow, IconButton, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Tooltip} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';
import JenisBerkasTableHead from './JenisBerkasTableHead';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Moment from 'react-moment';
import 'moment/locale/id';

const ModalDeleteButton = withStyles(theme => ({
    root: {
        color: '#fff',
        backgroundColor: '#ff4569',
        borderRadius: 0,
        '&:hover': {
            backgroundColor: '#ff1744'
        }
    }
}))(Button);

const useStyles = makeStyles(theme => ({
    modalTitle: {
        color: '#ffffff',
        backgroundColor: '#00a65a',
        padding: '8px 16px'
    },
    modalBody: {
        padding: '24px 16px',
        borderBottom: '1px solid #f5f5f5'
    }
}));

const pageSize = 10;

function JenisBerkasTable(props) {
    const dispatch = useDispatch();

    const classes = useStyles(props);
    const objects = useSelector(({JenisBerkasApp}) => JenisBerkasApp.objects.data);
    const count_all = useSelector(({JenisBerkasApp}) => JenisBerkasApp.objects.count_all);
    const pages = useSelector(({JenisBerkasApp}) => JenisBerkasApp.objects.page);
    const searchText = useSelector(({JenisBerkasApp}) => JenisBerkasApp.objects.searchText);
    const loading = useSelector(({JenisBerkasApp}) => JenisBerkasApp.objects.loading);

    const [open, setOpen] = useState(false);
    const [selected, setSelected] = useState([]);
    const [remove, setRemove] = useState(null);
    const [page, setPage] = useState(pages);
    const [rowsPerPage, setRowsPerPage] = useState(pageSize);
    const [order, setOrder] = useState({
        direction: 'asc',
        id       : null
    });

    useEffect(() => {
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: 0, pageSize: pageSize, filter: searchText}));
    }, [dispatch, searchText]);

    function handleRequestSort(event, property) {
        const id = property;
        let direction = 'desc';

        if ( order.id === property && order.direction === 'desc' ) {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleSelectAllClick(event) {
        if ( event.target.checked ) {
            setSelected(objects.map(n => n.jenis_berkas_id));
            return;
        }
        setSelected([]);
    }

    function handleClick(item) {
        props.history.push('/data-referensi/jenis-berkas/' + item.jenis_berkas_id + '/' + item.nama);
    }

    function handleChangePage(event, page) {
        setPage(page);
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: page, pageSize: rowsPerPage, filter: searchText}));
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(event.target.value);
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: page, pageSize: event.target.value, filter: searchText}));
    }

    function handleClickOpen() {
        setOpen(true);
    }
    
    function handleClose() {
        setOpen(false);
    }

    function removeObject() {
        dispatch(Actions.removeObject(remove)).then(res => {
            dispatch(Actions.setLoading(true));
            dispatch(Actions.getObjects({page: 0, pageSize: pageSize, filter: searchText}));
            setPage(0);
            setRowsPerPage(0);
        });
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table className="w-full" aria-labelledby="tableTitle">
                    <JenisBerkasTableHead
                        numSelected={selected.length}
                        order={order}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={count_all}
                    />
                    <TableBody>
                        {loading && (<TableRow><TableCell colSpan={5}><center>Memuat...</center> </TableCell></TableRow>)}
                        {!loading && objects.length === 0 && (<TableRow><TableCell colSpan={4}><center><i>Tidak ada data untuk ditampilkan</i></center></TableCell></TableRow>)}
                        {!loading && objects.map((n, key) => {
                                return (
                                    <TableRow
                                        className="cursor-pointer"
                                        hover
                                        tabIndex={-1}
                                        key={n.jenis_berkas_id}
                                        onClick={event => handleClick(n)}
                                    >
                                        <TableCell component="th" scope="row" className="w-44 min-w-44">
                                            {(page * rowsPerPage) + key + 1}
                                        </TableCell>
                                        <TableCell component="th" scope="row" className="min-w-160 sm:min-w-full">
                                            {n.nama}
                                        </TableCell>
                                        <TableCell component="th" scope="row" className="w-160 min-w-160">
                                            <Moment format="D MMM YYYY" locale="id">
                                                {n.create_date}
                                            </Moment>
                                        </TableCell>
                                        <TableCell component="th" scope="row" className="w-32 min-w-32">
                                            <Tooltip
                                                aria-label="Hapus Jenis Berkas"
                                                title="Hapus Jenis Berkas"
                                            >
                                                <IconButton 
                                                    aria-label="Hapus Jenis Berkas" 
                                                    size="small"
                                                    onClick={(ev) => {
                                                        ev.stopPropagation();
                                                        setRemove({
                                                            jenis_berkas_id: n.jenis_berkas_id
                                                        });
                                                        dispatch(handleClickOpen);
                                                    }}
                                                >
                                                    <DeleteOutlineIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </FuseScrollbars>
            <TablePagination
                component="div"
                count={count_all}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                    'size': 'small'
                }}
                labelDisplayedRows={
                    ({ from, to, count }) => `${from}-${to} dari ${count}`
                }
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                    'size': 'small'
                }}
                labelRowsPerPage={
                    'Menampilkan Data Per Halaman:'
                }
                SelectProps={{
                    native: true
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="jenis-berkas-dialog-title"
                aria-describedby="jenis-berkas-dialog-description"
                classes={{paper: "rounded-none"}}
            >
                <DialogTitle id="jenis-berkas-dialog-title" className={classes.modalTitle}>{"Hapus Jenis Berkas"}</DialogTitle>
                <DialogContent className={classes.modalBody}>
                    <DialogContentText id="jenis-berkas-dialog-description" className="mb-0">
                        Apakah anda yakin ingin menghapus jenis berkas ini?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button 
                        className="rounded-none"
                        onClick={handleClose}
                        size="small"
                        color="primary"
                    >
                        Batal
                    </Button>
                    <ModalDeleteButton 
                        variant="contained"
                        onClick={(ev) => {
                            dispatch(handleClose);
                            dispatch(removeObject);
                        }}
                        size="small"
                        color="primary"
                        autoFocus
                    >
                        Hapus
                    </ModalDeleteButton>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default withRouter(JenisBerkasTable);