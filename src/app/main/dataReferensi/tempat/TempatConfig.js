import React from 'react';

export const TempatConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/tempat/:tempat_id/:nama?',
            component   : React.lazy(() => import('./PerTempat'))
        },
        {
            path        : '/data-referensi/tempat/baru',
            component   : React.lazy(() => import('./PerTempat'))
        },
        {
            path        : '/data-referensi/tempat',
            component   : React.lazy(() => import('./Tempat'))
        }
    ]
};