import React, {useEffect, useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/styles';
import {Table, TableBody, TableCell, TablePagination, TableRow, IconButton, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Tooltip} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';
import TempatTableHead from './TempatTableHead';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import Moment from 'react-moment';
import 'moment/locale/id';
import clsx from 'clsx';

const ModalDeleteButton = withStyles(theme => ({
    root: {
        color: '#fff',
        backgroundColor: '#ff4569',
        borderRadius: 0,
        '&:hover': {
            backgroundColor: '#ff1744'
        }
    }
}))(Button);

const useStyles = makeStyles (theme => ({
    itemRow: {
        cursor: 'pointer',
        '& td': {
            borderBottom: '1px solid #f4f4f478',
            fontWeight: 'bold',
            color: '#ffffff',
            '& .MuiIconButton-root': {
                color: '#eeeeee'
            }
        },
        '&.item-1': {
            backgroundColor: '#546e7a !important',
            '& .level': {
                paddingLeft: 10
            }
        },
        '&.item-2': {
            backgroundColor: "#607d8b !important",
            '& .level': {
                paddingLeft: 25
            }
        },
        '&.item-3': {
            backgroundColor: "#78909c !important",
            '& .level': {
                paddingLeft: 40
            }
        },
        '&.item-4': {
            backgroundColor: "#90a4ae !important",
            '& .level': {
                paddingLeft: 55
            }
        },
        '&.item-5': {
            backgroundColor: "#b0bec5 !important",
            '& td': {
                color: "#000000de"
            },
            '& .level': {
                paddingLeft: 70
            },
            '& button': {
                color: "#000000de !important"
            }
        },
        '&.item-6': {
            backgroundColor: "#cfd8dc !important",
            '& td': {
                color: "#000000de"
            },
            '& .level': {
                paddingLeft: 85
            },
            '& button': {
                color: "#000000de !important"
            }
        },
        '&.item-7': {
            backgroundColor: "#eceff1 !important",
            '& td': {
                color: "#000000de"
            },
            '& .level': {
                paddingLeft: 100
            },
            '& button': {
                color: "#000000de !important"
            }
        }
    },
    modalTitle: {
        color: '#ffffff',
        backgroundColor: '#00a65a',
        padding: '8px 16px'
    },
    modalBody: {
        padding: '24px 16px',
        borderBottom: '1px solid #f5f5f5'
    }
}));

const pageSize = 25;

function TempatTable(props) {
    const dispatch = useDispatch();

    const classes = useStyles(props);
    const objects = useSelector(({tempatApp}) => tempatApp.objects.data);
    const count_all = useSelector(({tempatApp}) => tempatApp.objects.count_all);
    const pages = useSelector(({tempatApp}) => tempatApp.objects.page);
    const searchText = useSelector(({tempatApp}) => tempatApp.objects.searchText);
    const loading = useSelector(({tempatApp}) => tempatApp.objects.loading);

    const [open, setOpen] = useState(false);
    const [selected, setSelected] = useState([]);
    const [remove, setRemove] = useState(null);
    const [page, setPage] = useState(pages);
    const [rowsPerPage, setRowsPerPage] = useState(pageSize);
    const [order, setOrder] = useState({
        direction: 'asc',
        id       : null
    });

    useEffect(() => {
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: 0, pageSize: pageSize, filter: searchText}));
    }, [dispatch, searchText]);

    function handleRequestSort(event, property) {
        const id = property;
        let direction = 'desc';

        if ( order.id === property && order.direction === 'desc' ) {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleSelectAllClick(event) {
        if ( event.target.checked ) {
            setSelected(objects.map(n => n.tempat_id));
            return;
        }
        setSelected([]);
    }

    function handleClick(item) {
        props.history.push('/data-referensi/tempat/' + item.tempat_id + '/' + item.nama);
    }

    function handleChangePage(event, page) {
        setPage(page);
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: page, pageSize: rowsPerPage, filter: searchText}));
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(event.target.value);
        dispatch(Actions.setLoading(true));
        dispatch(Actions.getObjects({page: page, pageSize: event.target.value, filter: searchText}));
    }

    function handleClickOpen() {
        setOpen(true);
    }
    
    function handleClose() {
        setOpen(false);
    }

    function removeObject() {
        dispatch(Actions.removeObject(remove)).then(res => {
            dispatch(Actions.setLoading(true));
            dispatch(Actions.getObjects({page: 0, pageSize: pageSize, filter: searchText}));
        });
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table className="w-full" aria-labelledby="tableTitle">
                    <TempatTableHead
                        numSelected={selected.length}
                        order={order}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={count_all}
                    />
                    <TableBody>
                        {loading && (<TableRow><TableCell colSpan={7}><center>Memuat...</center> </TableCell></TableRow>)}
                        {!loading && objects.length === 0 && (<TableRow><TableCell colSpan={4}><center><i>Tidak ada data untuk ditampilkan</i></center></TableCell></TableRow>)}
                        {!loading && objects.map((n, key) => {
                                return (
                                    <TableRow
                                        className={clsx(classes.itemRow, "item-"+n.tingkat_tempat_id)}
                                        tabIndex={-1}
                                        key={n.tempat_id}
                                        onClick={event => handleClick(n)}
                                    >
                                        <TableCell scope="row" className="w-44 min-w-44">
                                            {(page * rowsPerPage) + key + 1}
                                        </TableCell>
                                        <TableCell scope="row">
                                            {n.nomor_urut}
                                        </TableCell>
                                        <TableCell scope="row" className="min-w-360 sm:min-w-full">
                                            <span className="level flex items-center"><ArrowRightIcon />{n.nama}</span>
                                        </TableCell>
                                        <TableCell scope="row" className="min-w-200 sm:min-w-full">
                                            {n.jenis_tempat}
                                        </TableCell>
                                        <TableCell scope="row" className="min-w-128 sm:min-w-full">
                                            {n.tingkat_tempat}
                                        </TableCell>
                                        <TableCell scope="row" className="w-160 min-w-160">
                                            <Moment format="D MMM YYYY" locale="id">
                                                {n.create_date}
                                            </Moment>
                                        </TableCell>
                                        <TableCell scope="row" className="w-32 min-w-32">
                                            <Tooltip
                                                aria-label="Hapus Tempat"
                                                title="Hapus Tempat"
                                            >
                                                <IconButton 
                                                    aria-label="Hapus Tempat" 
                                                    size="small"
                                                    onClick={(ev) => {
                                                        ev.stopPropagation();
                                                        setRemove({
                                                            tempat_id: n.tempat_id
                                                        });
                                                        dispatch(handleClickOpen);
                                                    }}
                                                >
                                                    <DeleteOutlineIcon fontSize="inherit" />
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </FuseScrollbars>
            <TablePagination
                component="div"
                count={count_all}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                    'size': 'small'
                }}
                labelDisplayedRows={
                    ({ from, to, count }) => `${from}-${to} dari ${count}`
                }
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                    'size': 'small'
                }}
                labelRowsPerPage={
                    'Menampilkan Data Per Halaman:'
                }
                SelectProps={{
                    native: true
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="tempat-dialog-title"
                aria-describedby="tempat-dialog-description"
                classes={{paper: "rounded-none"}}
            >
                <DialogTitle id="tempat-dialog-title" className={classes.modalTitle}>{"Hapus Tempat"}</DialogTitle>
                <DialogContent className={classes.modalBody}>
                    <DialogContentText id="tempat-dialog-description" className="mb-0">
                        Apakah anda yakin ingin menghapus tempat ini?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button 
                        className="rounded-none"
                        onClick={handleClose}
                        size="small"
                        color="primary"
                    >
                        Batal
                    </Button>
                    <ModalDeleteButton 
                        variant="contained"
                        onClick={(ev) => {
                            dispatch(handleClose);
                            dispatch(removeObject);
                        }}
                        size="small"
                        color="primary"
                        autoFocus
                    >
                        Hapus
                    </ModalDeleteButton>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default withRouter(TempatTable);