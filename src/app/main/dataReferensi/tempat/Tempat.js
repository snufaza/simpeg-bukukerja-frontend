import React, {useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {Paper, Typography, Breadcrumbs, Icon, Input, Button, Hidden, IconButton, Divider, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import {FusePageSimple} from '@fuse';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import withReducer from 'app/store/withReducer';
import reducer from './store/reducers';
import TempatTable from './TempatTable';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from './store/actions';
import {ThemeProvider} from '@material-ui/styles';
import AddIcon from '@material-ui/icons/Add';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

const baseUrl = localStorage.getItem("sim-musda-baseUrl");
const token = localStorage.getItem("sim_musda_jwt_access_token");

const MoreVertButton = withStyles({
    root: {
        color: 'rgba(0, 0, 0, 0.54)',
        backgroundColor: '#efefef',
        padding: 6,
        '&:hover': {
            backgroundColor: '#e7e6e6'
        }
    }
})(IconButton);

const AddButton = withStyles({
    root: {
        color: '#ffffff',
        backgroundColor: '#3C8DBC',
        '&:hover': {
            backgroundColor: '#357CA5'
        }
    }
})(Button);

const ExcelButton = withStyles({
    root: {
        color: '#ffffff',
        backgroundColor: '#00a65a',
        '&:hover': {
            backgroundColor: '#019652'
        }
    }
})(Button);

const useStyles = makeStyles (theme => ({
    layoutRoot: {
        flexGrow: 1
    },
    headerTitle: {
        padding: 0,
        height: 44,
        minHeight: 44,
        background: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            height: 'auto'
        }
    },
    detailMaster: {
        padding: 12,
        borderRadius: 0,
        boxShadow: '0px 2px 5px #0000000d'
    },
    mobileMenu: {
        width: 'auto'
    },
    listMenu: {
        minWidth: 32
    },
    colorGrids: {
        [theme.breakpoints.down('sm')]: {
            flexWrap: 'wrap',
            marginTop: '1.2rem'
        },
        '& > div': {
            [theme.breakpoints.down('sm')]: {
                flex: '0 0 33.333%',
                maxWidth: '33.333%',
                marginRight: 0,
                alignItems: 'center'
            }
        }
    },
    itemLevel: {
        width: 18,
        height: 18,
        marginRight: 6,
        '&.item-1': {
            backgroundColor: '#546e7a'
        },
        '&.item-2': {
            backgroundColor: '#607d8b'
        },
        '&.item-3': {
            backgroundColor: '#78909c'
        },
        '&.item-4': {
            backgroundColor: '#90a4ae'
        },
        '&.item-5': {
            backgroundColor: '#b0bec5'
        },
        '&.item-6': {
            backgroundColor: '#cfd8dc'
        },
        '&.item-7': {
            backgroundColor: '#eceff1'
        }
    }
}));

function Tempat(props) {
    const classes = useStyles(props);
    const dispatch = useDispatch();
    const searchText = useSelector(({tempatApp}) => tempatApp.objects.searchText);
    const mainTheme = useSelector(({fuse}) => fuse.settings.mainTheme);
    
    const [menuState, setMenuState] = useState({
        bottom: false
    });

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
    
        setMenuState({ ...menuState, [side]: open });
    };

    const mobileDrawers = side => (
        <div
            className={classes.mobileMenu}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                <ListItem 
                    button
                    component={Link}
                    to="/data-referensi/tempat/baru"
                >
                    <ListItemIcon classes={{root: classes.listMenu}}><AddIcon fontSize="small" /></ListItemIcon>
                    <ListItemText primary="Tambah" />
                </ListItem>
                <Divider />
                <ListItem 
                    button
                    onClick={(e) => {
                        window.open(baseUrl + "api/excel/tempat?token=" + token);
                    }}
                >
                    <ListItemIcon classes={{root: classes.listMenu}}><img src="assets/images/icons/excel.svg" width="20" alt="Unduh Data" /></ListItemIcon>
                    <ListItemText primary="Unduh Data" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <FusePageSimple
            classes={{
                root    : classes.layoutRoot,
                header  : classes.headerTitle
            }}
            header={
                <div className="flex flex-1 flex-col sm:flex-row justify-between items-center px-12 pt-12">
                    <Typography variant="h6" className="text-black">Tempat</Typography>
                    <Hidden smDown>
                        <Breadcrumbs separator="›" aria-label="breadcrumb" style={{color: 'rgba(0, 0, 0, 0.7)'}}>
                            <Link color="textPrimary" to="/beranda">
                                Beranda
                            </Link>
                            <Link color="textPrimary" to="/beranda">
                                Data Referensi
                            </Link>
                            <Typography className="text-grey-darker">Tempat</Typography>
                        </Breadcrumbs>
                    </Hidden>
                </div>
            }
            content={
                <div className="p-12">
                    <div className="flex flex-1 w-full items-center justify-between pb-12">
                        <ThemeProvider theme={mainTheme}>
                            <Paper className="flex items-center w-full sm:max-w-224 mr-12 sm:mr-0 px-6 rounded-none" elevation={0} style={{border: '1px solid #ccc', height: 34}}>
                                <Icon className="mr-8" color="action" fontSize="small">search</Icon>
                                <Input
                                    placeholder="Ketik Tempat..."
                                    className="flex flex-1 text-14"
                                    disableUnderline
                                    fullWidth
                                    value={searchText}
                                    inputProps={{
                                        'aria-label': 'Ketik Tempat...'
                                    }}
                                    onChange={ev => dispatch(Actions.setObjectsSearchText(ev))}
                                />
                            </Paper>
                        </ThemeProvider>
                        <div className="flex flex-1 items-center justify-end p-0">
                            <Hidden smDown>
                                <AddButton 
                                    variant="contained" 
                                    color="secondary" 
                                    size="small" 
                                    className="normal-case rounded-none"
                                    component={Link}
                                    to="/data-referensi/tempat/baru"
                                >
                                    <AddIcon fontSize="small" className="mr-4" /> Tambah
                                </AddButton>
                                <ExcelButton 
                                    variant="contained" 
                                    color="secondary" 
                                    size="small" 
                                    className="normal-case rounded-none ml-6" 
                                    onClick={(e) => {
                                        window.open(baseUrl + "api/excel/tempat?token=" + token);
                                    }} 
                                >
                                    <img src="assets/images/icons/excel.svg" width="16" className="mr-4" alt="Unduh Data" /> Unduh Data
                                </ExcelButton>
                            </Hidden>
                            <Hidden smUp>
                                <MoreVertButton 
                                    aria-label="delete"
                                    size="small"
                                    onClick={
                                        toggleDrawer('bottom', true)
                                    }
                                >
                                    <MoreVertIcon fontSize="inherit" />
                                </MoreVertButton>
                            </Hidden>
                        </div>
                    </div>
                    <Paper className={clsx(classes.detailMaster, "p-0")}>
                        <TempatTable/>
                    </Paper>
                    <div className="mt-12">
                        <Typography variant="subtitle2" className="font-bold text-12 sm:text-14" gutterBottom>*Keterangan Warna Berdasarkan Tingkat Tempat :</Typography>
                        <div className={clsx(classes.colorGrids, "flex flex-1 items-center w-full")}>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-1")}></span>
                                <Typography>Gedung</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-2")}></span>
                                <Typography>Lantai</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-3")}></span>
                                <Typography>Ruang</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-4")}></span>
                                <Typography>Sub Ruang</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-5")}></span>
                                <Typography>Rak</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-6")}></span>
                                <Typography>Vitrin</Typography>
                            </div>
                            <div className="flex mr-12">
                                <span className={clsx(classes.itemLevel, "item-7")}></span>
                                <Typography>Box/Kotak</Typography>
                            </div>
                        </div>
                    </div>
                    <SwipeableDrawer
                        anchor="bottom"
                        open={menuState.bottom}
                        onClose={toggleDrawer('bottom', false)}
                        onOpen={toggleDrawer('bottom', true)}
                    >
                        {mobileDrawers('bottom')}
                    </SwipeableDrawer>
                </div>
            }
        />
    )
}

export default withReducer('tempatApp', reducer)(Tempat);