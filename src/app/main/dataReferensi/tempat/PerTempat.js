import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Tab, Tabs, Typography, Breadcrumbs, Paper, Box, Button, IconButton, Tooltip, FormControl, Grid, InputBase, Select, Hidden} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/styles';
import {FusePageSimple} from '@fuse';
import {useForm} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import SaveIcon from '@material-ui/icons/Save';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

const CustomIconButton = withStyles(theme => ({
    root: {
        color: '#333',
        backgroundColor: '#f6f6f6',
        '&:hover': {
            backgroundColor: '#f6f6f6'
        }
    }
}))(IconButton);

const BootstrapInput = withStyles(theme => ({
    input: {
        borderRadius: 0,
        position: 'relative',
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        fontSize: 14,
        width: '100%',
        padding: '8px 12px 7.38px 12px',
        transition: theme.transitions.create(['border-color']),
        '&:focus': {
            borderColor: '#00a65a'
        }
    }
}))(InputBase);

const BootstrapSelect = withStyles(theme => ({
    input: {
        borderRadius: 0,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 14,
        width: '100%',
        padding: '8px 36px 7.38px 12px',
        transition: theme.transitions.create(['border-color']),
        '&:focus': {
            borderColor: '#00a65a'
        }
    }
}))(InputBase);

const useStyles = makeStyles(theme => ({
    layoutRoot: {
        flexGrow: 1
    },
    headerTitle: {
        padding: 0,
        height: 44,
        minHeight: 44,
        background: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            height: 'auto'
        }
    },
    detailMaster: {
        padding: 12,
        borderRadius: 0,
        boxShadow: '0px 2px 5px #0000000d'
    },
    linkButton: {
        borderBottom: '1px solid #efefef',
        paddingRight: 12
    },
    input: {
        position: 'relative'
    }
}));

const SimTabs = withStyles({
    root: {
        borderBottom: '1px solid #efefef',
        minHeight: 44,
        width: '100%'
    }
})(Tabs);
  
const SimTab = withStyles(theme => ({
    root: {
        textTransform: 'none',
        minWidth: 72,
        fontWeight: theme.typography.fontWeightRegular,
        minHeight: 44
    }
}))(props => <Tab disableRipple {...props} />);  

function TabPanel(props) {
    const {children, value, index, ...other} = props;
  
    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box className="p-12">{children}</Box>
      </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};
  
function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

function PerTempat(props) {
    const dispatch = useDispatch();
    const object = useSelector(({tempatApp}) => tempatApp.object);

    const classes = useStyles(props);
    const [value, setValue] = useState(0);
    const {form, handleChange, setForm} = useForm(null);

    useEffect(() => {
        function updateObjectState() {
            const {tempat_id} = props.match.params;

            if ( tempat_id === 'baru' ) {
                dispatch(Actions.newObject());
            }
            else {
                dispatch(Actions.getObject(props.match.params));
            }
        }

        updateObjectState();
        dispatch(Actions.getTingkatTempat());        
        dispatch(Actions.getJenisTempat());

    }, [dispatch, props.match.params]);

    useEffect(() => {
        if (
            (object.data && !form) ||
            (object.data && form && object.data.tempat_id !== form.tempat_id)
        )
        {
            setForm(object.data);
            const e = {target: { value: object.data.tingkat_tempat_id }};
            getInduk(e);
        }
    }, [form, object, setForm, getInduk]);

    function handleChangeTabs(event, newValue) {
        setValue(newValue);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    function getInduk(e) {
        const params = {tingkat_tempat_id: e.target.value};
        dispatch(Actions.setIndukTempat(params));
    }

    function canBeSubmitted() {
        return (
            form.nama.length > 0 &&
            !_.isEqual(object.data, form)
        );
    }

    return (
        <FusePageSimple
            classes={{
                root    : classes.layoutRoot,
                header  : classes.headerTitle
            }}
            header={
                form && (
                    <div className="flex flex-1 flex-col sm:flex-row justify-between items-center px-12 pt-12">
                        <Typography variant="h6" className="text-black">{form.nama ? form.nama : 'Tambah Tempat'}</Typography>
                        <Hidden smDown>
                            <Breadcrumbs separator="›" aria-label="breadcrumb" style={{color: 'rgba(0, 0, 0, 0.7)'}}>
                                <Link color="textPrimary" to="/beranda">
                                    Beranda
                                </Link>
                                <Link color="textPrimary" to="/beranda">
                                    Data Referensi
                                </Link>
                                <Link color="textPrimary" to="/data-referensi/tempat">
                                    Tempat
                                </Link>
                                <Typography className="text-grey-darker">{form.nama ? form.nama : 'Tambah Tempat'}</Typography>
                            </Breadcrumbs>
                        </Hidden>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-12">
                        <Paper className={clsx(classes.detailMaster, "p-0")}>
                            <div className="flex flex-1 justify-between">
                                <SimTabs value={value} onChange={handleChangeTabs} aria-label="Sim Tabs">
                                    <SimTab label="Rincian Tempat" {...a11yProps(0)} />
                                </SimTabs>
                                <div className={clsx(classes.linkButton, "flex items-center")}>
                                    <Tooltip
                                        aria-label="Kembali ke Daftar Tempat"
                                        title="Kembali ke Daftar Tempat"
                                    >
                                        <CustomIconButton 
                                            aria-label="Kembali ke Daftar Tempat" 
                                            size="small"
                                            className="mr-4"
                                            component={Link}
                                            to="/data-referensi/tempat"
                                        >
                                            <KeyboardBackspaceIcon fontSize="inherit" />
                                        </CustomIconButton>
                                    </Tooltip>
                                </div>
                            </div>
                            <TabPanel value={value} index={0}>
                                <Grid container spacing={1} justify="center">
                                    <Grid item xs={12} sm={6}>
                                        <div className="flex flex-1 flex-col sm:flex-row mb-8">
                                            <Typography className="w-full sm:max-w-136 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">Tingkat Tempat<span className="text-red">*</span></Typography>
                                            <FormControl 
                                                variant="outlined" 
                                                className="w-full"
                                                required
                                            >
                                                <Select
                                                    native
                                                    value={form ? form.tingkat_tempat_id : ''}
                                                    onChange={(e) => {
                                                        getInduk(e)
                                                        handleChange(e)
                                                    }}
                                                    input={<BootstrapSelect name="tingkat_tempat_id" id="tingkat_tempat_id"/>}
                                                >
                                                    <option value="">Pilih</option>
                                                    {object.tingkat_tempat.map((item, key) => 
                                                        <option key={key} value={item.tingkat_tempat_id}>{item.nama}</option>
                                                    )}
                                                </Select>
                                            </FormControl>
                                        </div>
                                        <div className="flex flex-1 flex-col sm:flex-row mb-0" style={{display: (form.tingkat_tempat_id === 1 ? ' none !important' : '')}}>
                                            <Typography className="w-full sm:max-w-136 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">Induk<span className="text-red">*</span></Typography>
                                            <FormControl 
                                                variant="outlined" 
                                                className="w-full"
                                                required
                                            >
                                                <Select
                                                    native
                                                    value={form ? form.induk_tempat_id : ''}
                                                    onChange={handleChange}
                                                    input={<BootstrapSelect name="induk_tempat_id" id="induk_tempat_id"/>}
                                                >
                                                    <option value="">Pilih</option>
                                                    {object.induk_tempat.map((item, key) => 
                                                        <option key={key} value={item.tempat_id}>{item.nama}</option>
                                                    )}
                                                </Select>
                                            </FormControl>
                                        </div>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <div className="flex flex-1 flex-col sm:flex-row mb-8">
                                            <Typography className="w-full sm:max-w-136 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">Tempat<span className="text-red">*</span></Typography>
                                            <FormControl 
                                                className="w-full"
                                                required
                                            >
                                                <BootstrapInput 
                                                    name="nama"
                                                    value={form ? form.nama : ''}
                                                    onChange={handleChange}
                                                    id="nama" 
                                                    autoFocus
                                                />
                                            </FormControl>
                                        </div>
                                        <div className="flex flex-1 flex-col sm:flex-row mb-0">
                                            <Typography className="w-full sm:max-w-136 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">Jenis Tempat<span className="text-red">*</span></Typography>
                                            <FormControl 
                                                variant="outlined"
                                                className="w-full"
                                                required
                                            >
                                                <Select
                                                    native
                                                    value={form ? form.jenis_tempat_id : ''}
                                                    onChange={handleChange}
                                                    input={<BootstrapSelect name="jenis_tempat_id" id="jenis_tempat_id"/>}
                                                >
                                                    <option value="">Pilih</option>
                                                    {object.jenis_tempat.map((item, key) => 
                                                        <option key={key} value={item.jenis_tempat_id}>{item.nama}</option>
                                                    )}
                                                </Select>
                                            </FormControl>
                                        </div>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                        </Paper>
                        <div className="w-full flex flex-1 justify-start sm:justify-end pt-12">
                            <Button 
                                variant="contained" 
                                color="secondary" 
                                size="small" 
                                className="normal-case rounded-none whitespace-no-wrap"
                                disabled={!canBeSubmitted()}
                                onClick={() => {
                                    const { tempat_id } = props.match.params;
                                    if (tempat_id === 'baru') {
                                        dispatch(Actions.saveObject(form))
                                    } else {
                                        dispatch(Actions.updateObject(form))
                                    }
                                }}
                            >
                                <SaveIcon fontSize="small" className="mr-4" /> Simpan Tempat
                            </Button>
                        </div>
                    </div>
                )
            }
        />
    )
}

export default withReducer('tempatApp', reducer)(PerTempat);