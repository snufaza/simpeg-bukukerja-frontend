import * as Actions from '../actions';

const initialState = {
    data            : null,
    tingkat_tempat  : [],
    jenis_tempat    : [],
    induk_tempat    : [],
};

const objectReducer = function (state = initialState, action) {
    switch ( action.type ) {
        case Actions.GET_OBJECT: {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_OBJECT: {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.GET_TINGKAT_TEMPAT: 
        {
            return {
                ...state,
                tingkat_tempat : action.payload,
            }
        }
        case Actions.GET_JENIS_TEMPAT:
        {
            return {
                ...state,
                jenis_tempat : action.payload,
            }
        }
        case Actions.GET_INDUK_TEMPAT:
        {
            return {
                ...state,
                induk_tempat : action.payload,
            }
        }
        default: {
            return state;
        }
    }
};

export default objectReducer;