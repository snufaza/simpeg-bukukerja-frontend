import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[TEMPAT APP] GET OBJECT';
export const SAVE_OBJECT = '[TEMPAT APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[TEMPAT APP] REMOVE OBJECT';
export const GET_TINGKAT_TEMPAT = '[TEMPAT APP] GET TINGKAT TEMPAT';
export const GET_JENIS_TEMPAT = '[TEMPAT APP] GET JENIS TEMPAT';
export const GET_INDUK_TEMPAT = '[TEMPAT APP] GET INDUK TEMPAT';

export function newObject() {
    const data = {
        tempat_id           : '',
        nama                : '',
        induk_tempat_id     : '',
        tingkat_tempat_id   : '',
        jenis_tempat_id     : '',
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}

export function getObject(params) {
    const request = axios.post('/api/ref/tempat/pertempat', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/tempat/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Tempat Telah Diubah.'}));
                history.push('/data-referensi/tempat/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/tempat/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Tempat Telah Diubah.'}));
                history.push('/data-referensi/tempat/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/tempat/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Tempat Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function getTingkatTempat() {
    const request = axios.post('/api/ref/tempat/tingkat_tempat_get');

    return (dispatch) => 
        request.then((response) => {
            return dispatch({
                type: GET_TINGKAT_TEMPAT,
                payload: response.data,
            })
        })
}

export function getJenisTempat() {
    const request = axios.post('/api/ref/tempat/jenis_tempat_get');

    return (dispatch) => 
        request.then((response) => {
            return dispatch({
                type: GET_JENIS_TEMPAT,
                payload: response.data,
            })
        })
}

export function setIndukTempat(params) {
    const link = '/api/ref/tempat/induk_tempat';

    const request = axios.post(link, {
        ...params,
    });

    return (dispatch) =>
        request.then((response) => {
            return dispatch({
                type: GET_INDUK_TEMPAT,
                payload: response.data,
            })
        })
}