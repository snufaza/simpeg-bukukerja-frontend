import React from 'react';
import {TableHead, TableSortLabel, TableCell, TableRow, Tooltip} from '@material-ui/core';

const rows = [
    {
        id              : 'id',
        align           : 'left',
        disablePadding  : false,
        label           : 'No.',
        sort            : true
    },
    {
        id              : 'nomor_urut',
        align           : 'left',
        disablePadding  : false,
        label           : 'Nomor Urut',
        sort            : true
    },
    {
        id              : 'nama',
        align           : 'left',
        disablePadding  : false,
        label           : 'Tempat',
        sort            : true
    },
    {
        id              : 'jenis_tempat',
        align           : 'left',
        disablePadding  : false,
        label           : 'Jenis Tempat',
        sort            : true
    },
    {
        id              : 'tingkat_tempat',
        align           : 'left',
        disablePadding  : false,
        label           : 'Tingkat Tempat',
        sort            : true
    },
    {
        id              : 'tanggal',
        align           : 'left',
        disablePadding  : false,
        label           : 'Tanggal Pembuatan',
        sort            : true
    },
    {
        id              : 'aksi',
        align           : 'left',
        disablePadding  : false,
        label           : 'Aksi',
        sort            : true
    }
];

function TempatTableHead(props) {
    const createSortHandler = property => event => {
        props.onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow className="w-48">
                {rows.map(row => {
                    return (
                        <TableCell
                            key={row.id}
                            align={row.align}
                            padding={row.disablePadding ? 'none' : 'default'}
                            sortDirection={props.order.id === row.id ? props.order.direction : false}
                        >
                            {row.sort && (
                                <Tooltip
                                    title="Sortir"
                                    placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={props.order.id === row.id}
                                        direction={props.order.direction}
                                        onClick={createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            )}
                        </TableCell>
                    );
                }, this)}
            </TableRow>
        </TableHead>
    );
}

export default TempatTableHead;