import React from 'react';

export const FungsiKoleksiConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/fungsi-koleksi/:fungsi_benda_id/:nama?',
            component   : React.lazy(() => import('./PerFungsiKoleksi'))
        },
        {
            path        : '/data-referensi/fungsi-koleksi/baru',
            component   : React.lazy(() => import('./PerFungsiKoleksi'))
        },
        {
            path        : '/data-referensi/fungsi-koleksi',
            component   : React.lazy(() => import('./FungsiKoleksi'))
        }
    ]
};