import axios from 'axios';

export const GET_OBJECTS = '[FUNGSI KOLEKSI APP] GET OBJECTS';
export const SET_OBJECTS_SEARCH_TEXT = '[FUNGSI KOLEKSI APP] SET OBJECTS SEARCH TEXT';
export const SET_LOADING = '[FUNGSI KOELSKI APP] SET LOADING';

export function getObjects(params) {
    const request = axios.post('/api/ref/fungsi_koleksi/server', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECTS,
                payload: response.data
            })
        );
}

export function setObjectsSearchText(event) {
    return {
        type      : SET_OBJECTS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function setLoading(params) {
    return {
        type: SET_LOADING,
        payload: params,
    }
}