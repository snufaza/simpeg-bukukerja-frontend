/** @format */

// import {JenisKoleksiConfig} from './jenisKoleksi/JenisKoleksiConfig';
// import {KelompokKoleksiConfig} from './kelompokKoleksi/KelompokKoleksiConfig';
// import {NegaraAsalConfig} from './negaraAsal/NegaraAsalConfig';
// import {FungsiKoleksiConfig} from './fungsiKoleksi/FungsiKoleksiConfig';
// import {CaraPerolehanConfig} from './caraPerolehan/CaraPerolehanConfig';
// import {KeaslianConfig} from './keaslian/KeaslianConfig';
// import {KeutuhanKoleksiConfig} from './keutuhanKoleksi/KeutuhanKoleksiConfig';
// import {KondisiConfig} from './kondisi/KondisiConfig';
// import {RagamHiasConfig} from './ragamHias/RagamHiasConfig';
// import {BentukHurufConfig} from './bentukHuruf/BentukHurufConfig';
// import {BahasaConfig} from './bahasa/BahasaConfig';
// import {KelompokEtnisConfig} from './kelompokEtnis/KelompokEtnisConfig';
// import {TempatConfig} from './tempat/TempatConfig';
// import {JenisBerkasConfig} from './jenisBerkas/JenisBerkasConfig';
import { LogConfig } from "./log/LogConfig";

export const dataReferensiConfigs = [
	LogConfig
	// JenisKoleksiConfig,
	// KelompokKoleksiConfig,
	// NegaraAsalConfig,
	// FungsiKoleksiConfig,
	// CaraPerolehanConfig,
	// KeaslianConfig,
	// KeutuhanKoleksiConfig,
	// KondisiConfig,
	// RagamHiasConfig,
	// BentukHurufConfig,
	// BahasaConfig,
	// KelompokEtnisConfig,
	// TempatConfig,
	// JenisBerkasConfig
];
