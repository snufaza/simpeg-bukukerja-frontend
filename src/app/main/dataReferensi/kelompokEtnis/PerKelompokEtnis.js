import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Tab, Tabs, Typography, Breadcrumbs, Paper, Box, Button, IconButton, Tooltip, FormControl, Grid, InputBase, Hidden} from '@material-ui/core';
import {makeStyles, withStyles} from '@material-ui/styles';
import {FusePageSimple} from '@fuse';
import {useForm} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import SaveIcon from '@material-ui/icons/Save';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

const CustomIconButton = withStyles(theme => ({
    root: {
        color: '#333',
        backgroundColor: '#f6f6f6',
        '&:hover': {
            backgroundColor: '#f6f6f6'
        }
    }
}))(IconButton);

const BootstrapInput = withStyles(theme => ({
    input: {
        borderRadius: 0,
        position: 'relative',
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        fontSize: 14,
        width: '100%',
        padding: '8px 12px 7.38px 12px',
        transition: theme.transitions.create(['border-color']),
        '&:focus': {
            borderColor: '#00a65a'
        }
    }
}))(InputBase);

const useStyles = makeStyles(theme => ({
    layoutRoot: {
        flexGrow: 1
    },
    headerTitle: {
        padding: 0,
        height: 44,
        minHeight: 44,
        background: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            height: 'auto'
        }
    },
    detailMaster: {
        padding: 12,
        borderRadius: 0,
        boxShadow: '0px 2px 5px #0000000d'
    },
    linkButton: {
        borderBottom: '1px solid #efefef',
        paddingRight: 12
    },
    input: {
        position: 'relative'
    }
}));

const SimTabs = withStyles({
    root: {
        borderBottom: '1px solid #efefef',
        minHeight: 44,
        width: '100%'
    }
})(Tabs);
  
const SimTab = withStyles(theme => ({
    root: {
        textTransform: 'none',
        minWidth: 72,
        fontWeight: theme.typography.fontWeightRegular,
        minHeight: 44
    }
}))(props => <Tab disableRipple {...props} />);  

function TabPanel(props) {
    const {children, value, index, ...other} = props;
  
    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box className="p-12">{children}</Box>
      </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};
  
function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

function PerKelompokEtnis(props) {
    const dispatch = useDispatch();
    const object = useSelector(({kelompokEtnisApp}) => kelompokEtnisApp.object);

    const classes = useStyles(props);
    const [value, setValue] = useState(0);
    const {form, handleChange, setForm} = useForm(null);

    useEffect(() => {
        function updateObjectState() {
            const params = props.match.params;
            const {kelompok_etnis_id} = params;

            if ( kelompok_etnis_id === 'baru' ) {
                dispatch(Actions.newObject());
            }
            else {
                dispatch(Actions.getObject(props.match.params));
            }
        }

        updateObjectState();
    }, [dispatch, props.match.params]);

    useEffect(() => {
        if (
            (object.data && !form) ||
            (object.data && form && object.data.kelompok_etnis_id !== form.kelompok_etnis_id)
        )
        {
            setForm(object.data);
        }
    }, [form, object.data, setForm]);

    function handleChangeTabs(event, newValue) {
        setValue(newValue);
    }

    function canBeSubmitted() {
        return (
            form.nama.length > 0 &&
            !_.isEqual(object.data, form)
        );
    }

    return (
        <FusePageSimple
            classes={{
                root    : classes.layoutRoot,
                header  : classes.headerTitle
            }}
            header={
                form && (
                    <div className="flex flex-1 flex-col sm:flex-row justify-between items-center px-12 pt-12">
                        <Typography variant="h6" className="text-black">{form.nama ? form.nama : 'Tambah Kelompok Etnis'}</Typography>
                        <Hidden smDown>
                            <Breadcrumbs separator="›" aria-label="breadcrumb" style={{color: 'rgba(0, 0, 0, 0.7)'}}>
                                <Link color="textPrimary" to="/beranda">
                                    Beranda
                                </Link>
                                <Link color="textPrimary" to="/beranda">
                                    Data Referensi
                                </Link>
                                <Link color="textPrimary" to="/data-referensi/kelompok-etnis">
                                    Kelompok Etnis
                                </Link>
                                <Typography className="text-grey-darker">{form.nama ? form.nama : 'Tambah Kelompok Etnis'}</Typography>
                            </Breadcrumbs>
                        </Hidden>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-12">
                        <Paper className={clsx(classes.detailMaster, "p-0")}>
                            <div className="flex flex-1 justify-between">
                                <SimTabs value={value} onChange={handleChangeTabs} aria-label="Sim Tabs">
                                    <SimTab label="Rincian Kelompok Etnis" {...a11yProps(0)} />
                                </SimTabs>
                                <div className={clsx(classes.linkButton, "flex items-center")}>
                                    <Tooltip
                                        aria-label="Kembali ke Daftar Kelompok Etnis"
                                        title="Kembali ke Daftar Kelompok Etnis"
                                    >
                                        <CustomIconButton 
                                            aria-label="Kembali ke Daftar Kelompok Etnis" 
                                            size="small"
                                            className="mr-4"
                                            component={Link}
                                            to="/data-referensi/kelompok-etnis"
                                        >
                                            <KeyboardBackspaceIcon fontSize="inherit" />
                                        </CustomIconButton>
                                    </Tooltip>
                                </div>
                            </div>
                            <TabPanel value={value} index={0}>
                                <Grid container spacing={1} justify="center">
                                    <Grid item xs={12} sm={10}>
                                        <div className="flex flex-1 flex-col sm:flex-row mb-0">
                                            <Typography className="w-full sm:max-w-128 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">Kelompok Etnis<span className="text-red">*</span></Typography>
                                            <FormControl 
                                                className="w-full"
                                                required
                                            >
                                                <BootstrapInput 
                                                    name="nama"
                                                    value={form ? form.nama : ''}
                                                    onChange={handleChange}
                                                    id="nama" 
                                                    autoFocus
                                                />
                                            </FormControl>
                                        </div>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                        </Paper>
                        <div className="w-full flex flex-1 justify-start sm:justify-end pt-12">
                            <Button 
                                variant="contained" 
                                color="secondary" 
                                size="small" 
                                className="normal-case rounded-none whitespace-no-wrap"
                                disabled={!canBeSubmitted()}
                                onClick={() => {
                                    const { kelompok_etnis_id } = props.match.params;
                                    if (kelompok_etnis_id === 'baru') {
                                        dispatch(Actions.saveObject(form))
                                    } else {
                                        dispatch(Actions.updateObject(form))
                                    }
                                }}
                            >
                                <SaveIcon fontSize="small" className="mr-4" /> Simpan Kelompok Etnis
                            </Button>
                        </div>
                    </div>
                )
            }
        />
    )
}

export default withReducer('kelompokEtnisApp', reducer)(PerKelompokEtnis);