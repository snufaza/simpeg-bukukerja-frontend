import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[KELOMPOK ETNIS APP] GET OBJECT';
export const SAVE_OBJECT = '[KELOMPOK ETNIS APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[KELOMPOK ETNIS APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/kelompok_etnis/perkelompok_etnis', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/kelompok_etnis/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Etnis Telah Diubah.'}));
                history.push('/data-referensi/kelompok-etnis/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/kelompok_etnis/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Etnis Telah Diubah.'}));
                history.push('/data-referensi/kelompok-etnis/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/kelompok_etnis/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kelompok Etnis Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        kelompok_etnis_id      : '',
        nama                : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}