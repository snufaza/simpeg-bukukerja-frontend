import React from 'react';

export const KelompokEtnisConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/kelompok-etnis/:kelompok_etnis_id/:nama?',
            component   : React.lazy(() => import('./PerKelompokEtnis'))
        },
        {
            path        : '/data-referensi/kelompok-etnis/baru',
            component   : React.lazy(() => import('./PerKelompokEtnis'))
        },
        {
            path        : '/data-referensi/kelompok-etnis',
            component   : React.lazy(() => import('./KelompokEtnis'))
        }
    ]
};