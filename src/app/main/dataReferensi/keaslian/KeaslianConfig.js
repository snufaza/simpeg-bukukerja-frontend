import React from 'react';

export const KeaslianConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/keaslian/:keaslian_id/:nama?',
            component   : React.lazy(() => import('./PerKeaslian'))
        },
        {
            path        : '/data-referensi/keaslian/baru',
            component   : React.lazy(() => import('./PerKeaslian'))
        },
        {
            path        : '/data-referensi/keaslian',
            component   : React.lazy(() => import('./Keaslian'))
        }
    ]
};