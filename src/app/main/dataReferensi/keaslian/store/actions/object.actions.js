import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[KEASLIAN APP] GET OBJECT';
export const SAVE_OBJECT = '[KEASLIAN APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[KEASLIAN APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/keaslian/perkeaslian', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/keaslian/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keaslian Telah Diubah.'}));
                history.push('/data-referensi/keaslian/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/keaslian/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keaslian Telah Diubah.'}));
                history.push('/data-referensi/keaslian/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/keaslian/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Keaslian Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        klasifikasi_id      : '',
        nama                : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}