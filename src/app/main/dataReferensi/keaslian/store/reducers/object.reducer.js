import * as Actions from '../actions';

const initialState = {
    data    : null
};

const objectReducer = function (state = initialState, action) {
    switch ( action.type ) {
        case Actions.GET_OBJECT: {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_OBJECT: {
            return {
                ...state,
                data: action.payload
            };
        }
        default: {
            return state;
        }
    }
};

export default objectReducer;