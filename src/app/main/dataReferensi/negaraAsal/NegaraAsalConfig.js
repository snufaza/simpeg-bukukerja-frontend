import React from 'react';

export const NegaraAsalConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/negara-asal/:negara_asal_id/:nama?',
            component   : React.lazy(() => import('./PerNegaraAsal'))
        },
        {
            path        : '/data-referensi/negara-asal/baru',
            component   : React.lazy(() => import('./PerNegaraAsal'))
        },
        {
            path        : '/data-referensi/negara-asal',
            component   : React.lazy(() => import('./NegaraAsal'))
        }
    ]
};