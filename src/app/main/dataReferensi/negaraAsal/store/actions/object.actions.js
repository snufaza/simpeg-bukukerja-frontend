import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[NEGARA ASAL APP] GET OBJECT';
export const SAVE_OBJECT = '[NEGARA ASAL APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[NEGARA ASAL APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/negara_asal/pernegaraasal', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/negara_asal/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Negara Asal Telah Diubah.'}));
                history.push('/data-referensi/negara-asal/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/negara_asal/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Negara Asal Telah Diubah.'}));
                history.push('/data-referensi/negara-asal/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/negara_asal/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Negara Asal Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        negara_asal_id      : '',
        nama                : '',
        kode_warna          : '',
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}