/** @format */

import React from "react";
import {
	TableHead,
	TableSortLabel,
	TableCell,
	TableRow,
	Tooltip
} from "@material-ui/core";

// "@id": "/api/sign_logs/3",
// "@type": "SignLog",
// "signLogId": "3",
// "nama": "login tandatangan",
// "statusLog": 1,
// "log": "login 1 2 3 4 5 6",
// "createDate": "2019-11-13 14:23:31",
// "lastUpdate": "2019-11-13 14:23:31",
// "expiredDate": null,
// "updaterId": "2E91FC26-4B0F-43E9-BEE6-4B6AE22905D7",
// "dokumen": "/api/dokumens/3E30D265-1088-4E95-8C28-6475EB99E688"

const rows = [
	{
		id: "id",
		align: "left",
		disablePadding: false,
		label: "No.",
		sort: true
	},
	{
		id: "signLogId",
		align: "left",
		disablePadding: false,
		label: "ID LOG",
		sort: true
	},
	{
		id: "nama",
		align: "left",
		disablePadding: false,
		label: "Log",
		sort: true
	},
	{
		id: "statusLog",
		align: "left",
		disablePadding: false,
		label: "Jenis Log",
		sort: true
	},
	{
		id: "createDate",
		align: "left",
		disablePadding: false,
		label: "Tingkat Log",
		sort: true
	},
	{
		id: "dokumenId",
		align: "left",
		disablePadding: false,
		label: "Dokumen",
		sort: true
	},
	// {
	// 	id: "aksi",
	// 	align: "left",
	// 	disablePadding: false,
	// 	label: "Aksi",
	// 	sort: true
	// }
];

function LogTableHead(props) {
	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	return (
		<TableHead>
			<TableRow className="w-48">
				{rows.map(row => {
					return (
						<TableCell
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? "none" : "default"}
							sortDirection={
								props.order.id === row.id ? props.order.direction : false
							}>
							{row.sort && (
								<Tooltip
									title="Sortir"
									placement={
										row.align === "right" ? "bottom-end" : "bottom-start"
									}
									enterDelay={300}>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
}

export default LogTableHead;
