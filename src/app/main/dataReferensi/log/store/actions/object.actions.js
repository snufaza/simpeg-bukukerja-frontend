/** @format */

import axios from "axios";
import { showMessage } from "app/store/actions/fuse";
import history from "@history";

export const GET_OBJECT = "[LOG APP] GET OBJECT";
export const SAVE_OBJECT = "[LOG APP] SAVE OBJECT";
export const REMOVE_OBJECT = "[LOG APP] REMOVE OBJECT";
export const GET_TINGKAT_LOG = "[LOG APP] GET TINGKAT LOG";
export const GET_JENIS_LOG = "[LOG APP] GET JENIS LOG";
export const GET_INDUK_LOG = "[LOG APP] GET INDUK LOG";

export function newObject() {
	const data = {
		log_id: "",
		nama: "",
		induk_log_id: "",
		tingkat_log_id: "",
		jenis_log_id: ""
	};

	return {
		type: GET_OBJECT,
		payload: data
	};
}

export function getObject(params) {
	const request = axios.get("/api/sign_logs", { ...params });

	return dispatch =>
		request.then(response => {
			console.log("getObject -> response", response);
			return dispatch({
				type: GET_OBJECT,
				payload: response.data
			});
		});
}

export function saveObject(data) {
	const request = axios.get("/api/sign_logs", { ...data });

	return dispatch =>
		request.then(response => {
			dispatch(showMessage({ message: "Data Log Telah Diubah." }));
			history.push("/data-referensi/log/");
			return dispatch({
				type: SAVE_OBJECT
			});
		});
}

export function updateObject(data) {
	const request = axios.post("/api/ref/log/update", { ...data });

	return dispatch =>
		request.then(response => {
			dispatch(showMessage({ message: "Data Log Telah Diubah." }));
			history.push("/data-referensi/log/");
			return dispatch({
				type: SAVE_OBJECT
			});
		});
}

export function removeObject(params) {
	const request = axios.post("/api/ref/log/delete", { ...params });

	return dispatch =>
		request.then(response => {
			dispatch(showMessage({ message: "Data Log Telah dihapus." }));

			return dispatch({
				type: REMOVE_OBJECT,
				payload: response.data
			});
		});
}

export function getTingkatLog() {
	const request = axios.post("/api/ref/log/tingkat_log_get");

	return dispatch =>
		request.then(response => {
			return dispatch({
				type: GET_TINGKAT_LOG,
				payload: response.data
			});
		});
}

export function getJenisLog() {
	const request = axios.post("/api/ref/log/jenis_log_get");

	return dispatch =>
		request.then(response => {
			return dispatch({
				type: GET_JENIS_LOG,
				payload: response.data
			});
		});
}

export function setIndukLog(params) {
	const link = "/api/ref/log/induk_log";

	const request = axios.post(link, {
		...params
	});

	return dispatch =>
		request.then(response => {
			return dispatch({
				type: GET_INDUK_LOG,
				payload: response.data
			});
		});
}
