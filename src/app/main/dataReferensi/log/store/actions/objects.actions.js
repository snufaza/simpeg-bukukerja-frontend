/** @format */

import axios from "axios";

export const GET_OBJECTS = "[LOG APP] GET OBJECTS";
export const SET_OBJECTS_SEARCH_TEXT = "[LOG APP] SET OBJECTS SEARCH TEXT";
export const SET_LOADING = "[LOG APP] SET LOADING";

export function getObjects(params) {
	// const request = axios.get("/api/sign_logs/" + params.signLogId);
	// return dispatch =>
	// 	request.then(response => {
	// 		return dispatch({
	// 			type: GET_OBJECTS,
	// 			payload: response.data
	// 		});
	// 	});

	params.page += 1;
	const request = axios.get("/api/sign_logs", { params: params });
	// params: routeParams

	return dispatch =>
		request.then(response => {
			const url = new URL(
				"http://www.abc.com" + response.data["hydra:view"]["@id"]
			);
			const page = url.searchParams.get("page");
			console.log("getUsers -> page", page);
			const count = 30;

			return dispatch({
				type: GET_OBJECTS,
				payload: response.data,
				page: page,
				count: count
			});
		});
}

export function setObjectsSearchText(event) {
	return {
		type: SET_OBJECTS_SEARCH_TEXT,
		searchText: event.target.value
	};
}

export function setLoading(params) {
	return {
		type: SET_LOADING,
		payload: params
	};
}
