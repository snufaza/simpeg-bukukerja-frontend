/** @format */

import * as Actions from "../actions";

const initialState = {
	data: [],
	count: 0,
	count_all: 0,
	page: 0,
	pages: 0,
	searchText: "",
	loading: true
};

const objectsReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_OBJECTS: {
			return {
				...state,
				data: action.payload["hydra:member"],
				count: 30,
				count_all: action.payload["hydra:totalItems"],
				page: action.page,
				pages: Math.floor(action.payload["hydra:totalItems"] / 30),
				loading: false
			};
		}
		case Actions.SET_OBJECTS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		case Actions.SET_LOADING: {
			return {
				...state,
				loading: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default objectsReducer;
