/** @format */

import * as Actions from "../actions";

const initialState = {
	data: null,
	tingkat_log: [],
	jenis_log: [],
	induk_log: []
};

const objectReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_OBJECT: {
			return {
				...state,
				data: action.payload
			};
		}
		case Actions.SAVE_OBJECT: {
			return {
				...state,
				data: action.payload
			};
		}
		case Actions.GET_TINGKAT_LOG: {
			return {
				...state,
				tingkat_log: action.payload
			};
		}
		case Actions.GET_JENIS_LOG: {
			return {
				...state,
				jenis_log: action.payload
			};
		}
		case Actions.GET_INDUK_LOG: {
			return {
				...state,
				induk_log: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default objectReducer;
