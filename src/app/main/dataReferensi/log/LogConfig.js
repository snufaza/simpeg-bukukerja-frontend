/** @format */

import React from "react";
import { authRoles } from "../../../auth";

export const LogConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.admin,
	routes: [
		{
			path: "/data-referensi/log/:log_id/:nama?",
			component: React.lazy(() => import("./PerLog"))
		},
		{
			path: "/data-referensi/log/baru",
			component: React.lazy(() => import("./PerLog"))
		},
		{
			path: "/data-referensi/log",
			component: React.lazy(() => import("./Log"))
		}
	]
};
