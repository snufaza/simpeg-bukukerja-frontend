import React from 'react';

export const BahasaConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/bahasa/:bahasa_id/:nama?',
            component   : React.lazy(() => import('./PerBahasa'))
        },
        {
            path        : '/data-referensi/bahasa/baru',
            component   : React.lazy(() => import('./PerBahasa'))
        },
        {
            path        : '/data-referensi/bahasa',
            component   : React.lazy(() => import('./Bahasa'))
        }
    ]
};