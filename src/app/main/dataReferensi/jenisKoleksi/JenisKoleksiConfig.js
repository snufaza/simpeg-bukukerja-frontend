import React from 'react';

export const JenisKoleksiConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/jenis-koleksi/:klasifikasi_id/:nama?',
            component   : React.lazy(() => import('./PerJenisKoleksi'))
        },
        {
            path        : '/data-referensi/jenis-koleksi/baru',
            component   : React.lazy(() => import('./PerJenisKoleksi'))
        },
        {
            path        : '/data-referensi/jenis-koleksi',
            component   : React.lazy(() => import('./JenisKoleksi'))
        }
    ]
};