import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[JENIS KOLEKSI APP] GET OBJECT';
export const SAVE_OBJECT = '[JENIS KOLEKSI APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[JENIS KOLEKSI APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/klasifikasi/perklasifikasi', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/klasifikasi/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Jenis Koleksi Telah Diubah.'}));
                history.push('/data-referensi/jenis-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/klasifikasi/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Jenis Koleksi Telah Diubah.'}));
                history.push('/data-referensi/jenis-koleksi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/klasifikasi/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Jenis Koleksi Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        klasifikasi_id      : '',
        nama                : '',
        keterangan          : '',
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}