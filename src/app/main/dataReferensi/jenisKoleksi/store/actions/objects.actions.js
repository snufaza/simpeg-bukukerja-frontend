import axios from 'axios';

export const GET_OBJECTS = '[JENIS KOLEKSI APP] GET OBJECTS';
export const SET_OBJECTS_SEARCH_TEXT = '[JENIS KOLEKSI APP] SET OBJECTS SEARCH TEXT';
export const SET_LOADING = '[JENIS KOLEKSI APP] SET LOADING';

export function getObjects(params) {
    const request = axios.post('/api/ref/klasifikasi/server', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECTS,
                payload: response.data
            })
        );
}

export function setObjectsSearchText(event) {
    return {
        type      : SET_OBJECTS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function setLoading(params) {
    return {
        type: SET_LOADING,
        payload: params
    }
}