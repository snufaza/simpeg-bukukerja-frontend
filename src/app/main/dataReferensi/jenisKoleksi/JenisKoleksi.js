import React, {useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {Paper, Typography, Breadcrumbs, Icon, Input, Button, Hidden, IconButton, Divider, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import {FusePageSimple} from '@fuse';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import withReducer from 'app/store/withReducer';
import reducer from './store/reducers';
import JenisKoleksiTable from './JenisKoleksiTable';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from './store/actions';
import {ThemeProvider} from '@material-ui/styles';
import AddIcon from '@material-ui/icons/Add';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

const baseUrl = localStorage.getItem("sim-musda-baseUrl");
const token = localStorage.getItem("sim_musda_jwt_access_token");

const MoreVertButton = withStyles({
    root: {
        color: 'rgba(0, 0, 0, 0.54)',
        backgroundColor: '#efefef',
        padding: 6,
        '&:hover': {
            backgroundColor: '#e7e6e6'
        }
    }
})(IconButton);

const AddButton = withStyles({
    root: {
        color: '#ffffff',
        backgroundColor: '#3C8DBC',
        '&:hover': {
            backgroundColor: '#357CA5'
        }
    }
})(Button);

const ExcelButton = withStyles({
    root: {
        color: '#ffffff',
        backgroundColor: '#00a65a',
        '&:hover': {
            backgroundColor: '#019652'
        }
    }
})(Button);

const useStyles = makeStyles (theme => ({
    layoutRoot: {
        flexGrow: 1
    },
    headerTitle: {
        padding: 0,
        height: 44,
        minHeight: 44,
        background: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            height: 'auto'
        }
    },
    detailMaster: {
        padding: 12,
        borderRadius: 0,
        boxShadow: '0px 2px 5px #0000000d'
    },
    mobileMenu: {
        width: 'auto'
    },
    listMenu: {
        minWidth: 32
    }
}));

function JenisKoleksi(props) {
    const classes = useStyles(props);
    const dispatch = useDispatch();
    const searchText = useSelector(({jenisKoleksiApp}) => jenisKoleksiApp.objects.searchText);
    const mainTheme = useSelector(({fuse}) => fuse.settings.mainTheme);
    
    const [menuState, setMenuState] = useState({
        bottom: false
    });

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
    
        setMenuState({ ...menuState, [side]: open });
    };

    const mobileDrawers = side => (
        <div
            className={classes.mobileMenu}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                <ListItem 
                    button
                    component={Link}
                    to="/data-referensi/jenis-koleksi/baru"
                >
                    <ListItemIcon classes={{root: classes.listMenu}}><AddIcon fontSize="small" /></ListItemIcon>
                    <ListItemText primary="Tambah" />
                </ListItem>
                <Divider />
                <ListItem 
                    button
                    onClick={(e) => {
                        window.open(baseUrl + "api/excel/klasifikasi?token=" + token);
                    }}
                >
                    <ListItemIcon classes={{root: classes.listMenu}}><img src="assets/images/icons/excel.svg" width="20" alt="Unduh Data" /></ListItemIcon>
                    <ListItemText primary="Unduh Data" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <FusePageSimple
            classes={{
                root    : classes.layoutRoot,
                header  : classes.headerTitle
            }}
            header={
                <div className="flex flex-1 flex-col sm:flex-row justify-between items-center px-12 pt-12">
                    <Typography variant="h6" className="text-black">Jenis Koleksi</Typography>
                    <Hidden smDown>
                        <Breadcrumbs separator="›" aria-label="breadcrumb" style={{color: 'rgba(0, 0, 0, 0.7)'}}>
                            <Link color="textPrimary" to="/beranda">
                                Beranda
                            </Link>
                            <Link color="textPrimary" to="/beranda">
                                Data Referensi
                            </Link>
                            <Typography className="text-grey-darker">Jenis Koleksi</Typography>
                        </Breadcrumbs>
                    </Hidden>
                </div>
            }
            content={
                <div className="p-12">
                    <div className="flex flex-1 w-full items-center justify-between pb-12">
                        <ThemeProvider theme={mainTheme}>
                            <Paper className="flex items-center w-full sm:max-w-224 mr-12 sm:mr-0 px-6 rounded-none" elevation={0} style={{border: '1px solid #ccc', height: 34}}>
                                <Icon className="mr-8" color="action" fontSize="small">search</Icon>
                                <Input
                                    placeholder="Ketik Jenis Koleksi..."
                                    className="flex flex-1 text-14"
                                    disableUnderline
                                    fullWidth
                                    value={searchText}
                                    inputProps={{
                                        'aria-label': 'Ketik Jenis Koleksi...'
                                    }}
                                    onChange={ev => dispatch(Actions.setObjectsSearchText(ev))}
                                />
                            </Paper>
                        </ThemeProvider>
                        <div className="flex flex-1 items-center justify-end p-0">
                            <Hidden smDown>
                                <AddButton 
                                    variant="contained" 
                                    color="secondary" 
                                    size="small" 
                                    className="normal-case rounded-none"
                                    component={Link}
                                    to="/data-referensi/jenis-koleksi/baru"
                                >
                                    <AddIcon fontSize="small" className="mr-4" /> Tambah
                                </AddButton>
                                <ExcelButton 
                                    variant="contained" 
                                    color="secondary" 
                                    size="small" 
                                    className="normal-case rounded-none ml-6" 
                                    onClick={(e) => {
                                        window.open(baseUrl + "api/excel/klasifikasi?token=" + token);
                                    }}
                                >
                                    <img src="assets/images/icons/excel.svg" width="16" className="mr-4" alt="Unduh Data" /> Unduh Data
                                </ExcelButton>
                            </Hidden>
                            <Hidden smUp>
                                <MoreVertButton 
                                    aria-label="delete"
                                    size="small"
                                    onClick={
                                        toggleDrawer('bottom', true)
                                    }
                                >
                                    <MoreVertIcon fontSize="inherit" />
                                </MoreVertButton>
                            </Hidden>
                        </div>
                    </div>
                    <Paper className={clsx(classes.detailMaster, "p-0")}>
                        <JenisKoleksiTable/>
                    </Paper>
                    <SwipeableDrawer
                        anchor="bottom"
                        open={menuState.bottom}
                        onClose={toggleDrawer('bottom', false)}
                        onOpen={toggleDrawer('bottom', true)}
                    >
                        {mobileDrawers('bottom')}
                    </SwipeableDrawer>
                </div>
            }
        />
    )
}

export default withReducer('jenisKoleksiApp', reducer)(JenisKoleksi);