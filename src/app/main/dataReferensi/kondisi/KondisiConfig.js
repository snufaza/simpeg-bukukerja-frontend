import React from 'react';

export const KondisiConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/kondisi/:kondisi_id/:nama?',
            component   : React.lazy(() => import('./PerKondisi'))
        },
        {
            path        : '/data-referensi/kondisi/baru',
            component   : React.lazy(() => import('./PerKondisi'))
        },
        {
            path        : '/data-referensi/kondisi',
            component   : React.lazy(() => import('./Kondisi'))
        }
    ]
};