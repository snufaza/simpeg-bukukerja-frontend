import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[KONDISI APP] GET OBJECT';
export const SAVE_OBJECT = '[KONDISI APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[KONDISI APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/kondisi/perkondisi', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/kondisi/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kondisi Telah Diubah.'}));
                history.push('/data-referensi/kondisi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/kondisi/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kondisi Telah Diubah.'}));
                history.push('/data-referensi/kondisi/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/kondisi/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Kondisi Telah dihapus.'}));
                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        kondisi_id          : '',
        nama                : ''
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}