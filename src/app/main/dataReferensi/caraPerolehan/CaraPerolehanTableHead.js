import React from 'react';
import {TableHead, TableSortLabel, TableCell, TableRow, Tooltip} from '@material-ui/core';

const rows = [
    {
        id              : 'id',
        align           : 'left',
        disablePadding  : false,
        label           : 'No.',
        sort            : true
    },
    {
        id              : 'nama',
        align           : 'left',
        disablePadding  : false,
        label           : 'Cara Perolehan',
        sort            : true
    },
    {
        id              : 'kode_warna',
        align           : 'left',
        disablePadding  : false,
        label           : 'Kode Warna',
        sort            : true
    },
    {
        id              : 'tanggal',
        align           : 'left',
        disablePadding  : false,
        label           : 'Tanggal Pembuatan',
        sort            : true
    },
    {
        id              : 'aksi',
        align           : 'left',
        disablePadding  : false,
        label           : 'Aksi',
        sort            : true
    }
];

function CaraPerolehanTableHead(props) {
    const createSortHandler = property => event => {
        props.onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow className="w-48">
                {rows.map(row => {
                    return (
                        <TableCell
                            key={row.id}
                            align={row.align}
                            padding={row.disablePadding ? 'none' : 'default'}
                            sortDirection={props.order.id === row.id ? props.order.direction : false}
                        >
                            {row.sort && (
                                <Tooltip
                                    title="Sortir"
                                    placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={props.order.id === row.id}
                                        direction={props.order.direction}
                                        onClick={createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            )}
                        </TableCell>
                    );
                }, this)}
            </TableRow>
        </TableHead>
    );
}

export default CaraPerolehanTableHead;