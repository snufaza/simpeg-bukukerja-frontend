import React from 'react';

export const CaraPerolehanConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path        : '/data-referensi/cara-perolehan/:jenis_perolehan_id/:nama?',
            component   : React.lazy(() => import('./PerCaraPerolehan'))
        },
        {
            path        : '/data-referensi/cara-perolehan/baru',
            component   : React.lazy(() => import('./PerCaraPerolehan'))
        },
        {
            path        : '/data-referensi/cara-perolehan',
            component   : React.lazy(() => import('./CaraPerolehan'))
        }
    ]
};