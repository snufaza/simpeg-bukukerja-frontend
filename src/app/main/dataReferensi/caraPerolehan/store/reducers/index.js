import {combineReducers} from 'redux';
import objects from './objects.reducer';
import object from './object.reducer';

const reducer = combineReducers({
    objects,
    object
});

export default reducer;