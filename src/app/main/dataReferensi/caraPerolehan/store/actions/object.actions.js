import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';
import history from '@history';

export const GET_OBJECT = '[CARA PEROLEHAN APP] GET OBJECT';
export const SAVE_OBJECT = '[CARA PEROLEHAN APP] SAVE OBJECT';
export const REMOVE_OBJECT = '[CARA PEROLEHAN APP] REMOVE OBJECT';

export function getObject(params) {
    const request = axios.post('/api/ref/jenis_perolehan/perjenisperolehan', {...params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_OBJECT,
                payload: response.data
            })
        );
}

export function saveObject(data) {
    const request = axios.post('/api/ref/jenis_perolehan/create', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Cara Perolehan Telah Diubah.'}));
                history.push('/data-referensi/cara-perolehan/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function updateObject(data) {
    const request = axios.post('/api/ref/jenis_perolehan/update', {...data});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Cara Perolehan Telah Diubah.'}));
                history.push('/data-referensi/cara-perolehan/');
                return dispatch({
                    type   : SAVE_OBJECT
                })
            }
        );
}

export function removeObject(params) {
    const request = axios.post('/api/ref/jenis_perolehan/delete', {...params});

    return (dispatch) =>
        request.then((response) => {
                dispatch(showMessage({message: 'Data Cara Perolehan Telah dihapus.'}));

                return dispatch({
                    type   : REMOVE_OBJECT,
                    payload: response.data
                })
            }
        );
}

export function newObject() {
    const data = {
        jenis_perolehan_id  : '',
        nama                : '',
        kode_warna          : '',
    };

    return {
        type   : GET_OBJECT,
        payload: data
    }
}