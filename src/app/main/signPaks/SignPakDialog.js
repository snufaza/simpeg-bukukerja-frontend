import React, { useEffect, useCallback} from 'react';
import {Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar} from '@material-ui/core';
import {useForm} from '@fuse/hooks';
import FuseUtils from '@fuse/FuseUtils';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';
// import {FuseLoading} from "../../../@fuse";
import axios from "axios";

const defaultFormState = {
    pak_id      : '',
    id          : '',
    name        : '',
    lastName    : '',
    avatar      : 'assets/images/avatars/profile.jpg',
    nickname    : '',
    company     : '',
    jobTitle    : '',
    email       : '',
    phone       : '',
    address     : '',
    birthday    : '',
    notes       : ''
};

function SignPakDialog(props) {
    const dispatch = useDispatch();
    const signPakDialog = useSelector(({signPakApp}) => signPakApp.signPaks.signPakDialog);

    const {form, setForm} = useForm(defaultFormState);
    // console.log(form.dokumenId);
    const urlPak = axios.defaults.baseURL+'/api/dokumen_tandatangans/view/'+form.blobId+'/preview.pdf';
    // const urlPak = 'http://localhost:8000/api/dokumen_tandatangans/view/0249990B-EED2-46AF-83B2-03B47FABD867/preview.pdf';
    // const [open, setOpen] = React.useState(false);

    function signPassPharse() {

        dispatch(Actions.openNewSignPakPassPharseDialog({dokumenId:form.dokumenId}));
    }

    function handleClose() {
        // setOpen(false);
        closeComposeDialog();
    }

    const initDialog = useCallback(
        () => {
            if ( signPakDialog.type === 'edit' && signPakDialog.data )
            {
                setForm({...signPakDialog.data});
            }

            if ( signPakDialog.type === 'new' ) {
                setForm({
                    ...defaultFormState,
                    ...signPakDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [signPakDialog.data, signPakDialog.type, setForm],
    );

    useEffect(() => {
        if ( signPakDialog.props.open ) {
            initDialog();
        }
    }, [signPakDialog.props.open, initDialog]);

    function closeComposeDialog() {
        signPakDialog.type === 'edit' ? dispatch(Actions.closeEditSignPakDialog()) : dispatch(Actions.closeNewSignPakDialog());
    }

    // function canBeSubmitted() {

    // }

    function handleSubmit() {

    }

    function handleRejectPak(id) {
        // console.log('reject');
        dispatch(Actions.showLoading());
        dispatch(Actions.rejectPak({dokumenId:form.dokumenId}));
        closeComposeDialog();
    }

    return (
        <div>
            <Dialog
                classes={{
                    paper: "m-64"
                }}
                {...signPakDialog.props}
                onClose={closeComposeDialog}
                fullWidth
                maxWidth="xl"
            >
                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex flex-1 justify-between w-full">
                        <Typography variant="subtitle1" color="inherit">
                            Pratinjau
                        </Typography>
                        <IconButton aria-label="Tutup" color="inherit" onClick={handleClose}>
                            <Icon fontSize="small">close</Icon>
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <form noValidate onSubmit={handleSubmit} className="flex flex-col overflow-hidden">
                    <DialogContent>
                        {/* <iframe className="w-full" style={{ minHeight: 450 }} src='http://simpeg.pendidikan.gunungkidulkab.lc/Penilaian/getCetakPak?pak_id={form.id}'> */}
                        <iframe title="iframeunixe1" className="w-full" style={{ minHeight: 450 }} src={urlPak}></iframe>
                    </DialogContent>
                    <DialogActions className="flex flex-1 justify-start p-12">
                        <Button 
                            variant="contained" 
                            color="secondary"
                            onClick={signPassPharse}
                        >
                            <Icon className="mr-4">check</Icon> Tandatangani
                        </Button>

                        <Button 
                            variant="contained"
                            onClick={handleRejectPak}
                        >
                            <Icon className="mr-4">restore</Icon> Perbaikan
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
}

export default SignPakDialog;