/** @format */

import React from "react";
import { authRoles } from "app/auth";
import { Redirect } from "react-router-dom";

export const SignPaksAppConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.user,
	routes: [
		{
			path: "/sign-pak/:clientApp/:id",
			component: React.lazy(() => import("./SignPaksApp"))
		},
		{
			path: "/sign-pak",
			component: () => <Redirect to="/profil-pengguna/all" />
		}
	]
};
