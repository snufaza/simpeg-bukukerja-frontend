import React, { useEffect, useCallback, useState} from 'react';
import {Input, Button, Dialog, DialogActions, DialogContent} from '@material-ui/core';
import {useForm} from '@fuse/hooks';
import FuseUtils from '@fuse/FuseUtils';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const defaultFormState = {
    pak_id      : '',
    id          : '',
    name        : '',
    lastName    : '',
    avatar      : 'assets/images/avatars/profile.jpg',
    nickname    : '',
    company     : '',
    jobTitle    : '',
    email       : '',
    phone       : '',
    address     : '',
    birthday    : '',
    notes       : '',
    dokumenId       : null
};

function SignPakPassPharseDialog(props) {
    const dispatch = useDispatch();
    const signPakPassPharseDialog = useSelector(({signPakApp}) => signPakApp.signPaks.signPakPassPharseDialog);
    const selectedSignPakIds = useSelector(({signPakApp}) => signPakApp.signPaks.selectedSignPakIds);

    const {form, setForm} = useForm(defaultFormState);
    // const urlPak = window.location.port == 3000 ? 'http://simpeg.pendidikan.gunungkidulkab.lc/Penilaian/getCetakPak?pak_id='+ form.pak_id : 'http://simpeg.pendidikan.gunungkidulkab.go.id/Penilaian/getCetakPak?pak_id='+form.pak_id;
    // const [open, setOpen] = React.useState(false);
    const [currentPasspharse, setcurrentPasspharse] = useState(1);

    function handleChangeTextField(event) {
        setcurrentPasspharse(event.target.value);
    }

    // function handleClickOpen() {
        // setOpen(true);
    // }

    function handleClose() {
        // setOpen(false);
        dispatch(Actions.closeNewSignPakPassPharseDialog());
    }

    const initDialog = useCallback(
        () => {
            /**
             * Dialog type: 'edit'
             */
            if ( signPakPassPharseDialog.type === 'edit' && signPakPassPharseDialog.data )
            {
                setForm({...signPakPassPharseDialog.data});
            }

            /**
             * Dialog type: 'new'
             */
            if ( signPakPassPharseDialog.type === 'new' )
            {
                setForm({
                    ...defaultFormState,
                    ...signPakPassPharseDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [signPakPassPharseDialog.data, signPakPassPharseDialog.type, setForm],
    );

    useEffect(() => {
        if ( signPakPassPharseDialog.props.open )
        {
            initDialog();
        }

    }, [signPakPassPharseDialog.props.open, initDialog]);

    function closeComposeDialog()
    {
        signPakPassPharseDialog.type === 'edit' ? dispatch(Actions.closeEditSignPakPassPharseDialog()) : dispatch(Actions.closeNewSignPakPassPharseDialog());
    }

    function handleSignPak(id)
    {

        if(selectedSignPakIds.length===0){
            const signPakIds = [form.dokumenId];
            dispatch(Actions.showLoading());
            dispatch(Actions.signPak(signPakIds, currentPasspharse));
        }else {
            dispatch(Actions.showLoading());
            dispatch(Actions.signPak(selectedSignPakIds, currentPasspharse));
        }
        closeComposeDialog();
    }

    // function handleRejectPak(id)
    // {
    //     dispatch(Actions.rejectPak(form.pak_id));
    //     closeComposeDialog();
    // }

    return (
        <div>
            <Dialog 
                {...signPakPassPharseDialog.props} 
                onClose={handleClose} 
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Passpharse</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Untuk Melanjutkan tandatangan digital silakan masukan Passpharse
                    </DialogContentText>
                    <Input
                        onChange={handleChangeTextField}
                        autoFocus
                        margin="dense"
                        id="name"
                        name="passpharse"
                        label="Masukan Passpharse"
                        type="password"
                        autoComplete="new-password"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <div className="flex flex-1 justify-between">
                        <Button variant="contained" onClick={
                            (ev) => {
                                ev.stopPropagation();
                                handleSignPak(form.dokumenId);
                            }} color="primary">
                            Tandatangan
                        </Button>
                        <Button size="small" onClick={handleClose}>
                            Batal
                        </Button>
                    </div>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default SignPakPassPharseDialog;