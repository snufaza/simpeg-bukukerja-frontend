import React, {useEffect, useState} from 'react';
import {Checkbox, Icon, Typography, Tooltip, Fab} from '@material-ui/core';
import {FuseUtils, FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import ReactTable from "react-table";
import * as Actions from './store/actions';
// import SignPaksMultiSelectMenu from './SignPaksMultiSelectMenu';
// import {makeStyles} from '@material-ui/styles';
import Skeleton from "react-loading-skeleton";

// const useStyles = makeStyles({
//     dangerBtn: {
//         display: "block"
//     }
// });

function SignPaksList(props) {
    // const classes = useStyles(props);
    const dispatch = useDispatch();
    const signPaks = useSelector(({signPakApp}) => signPakApp.signPaks.entities);
    const selectedSignPakIds = useSelector(({signPakApp}) => signPakApp.signPaks.selectedSignPakIds);
    const hiddenTandatanganButton = useSelector(({signPakApp}) => signPakApp.signPaks.hiddenTandatanganButton);
    const searchText = useSelector(({signPakApp}) => signPakApp.signPaks.searchText);
    const [filteredData, setFilteredData] = useState(null);
    const signPakLoaded = useSelector(({ signPakApp }) => signPakApp.signPaks.sign_pak_loaded);

    const bulan = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ]

    function format_tanggal(date) {
        const time = new Date(date);
        return time.getDate() +" "+ bulan[time.getMonth()] +" "+ time.getFullYear();
    }


    useEffect(() => {
        function getFilteredArray(entities, searchText)
        {

            // disini untuk menentukan bahwa signpak loaded
            dispatch(Actions.signPakLoaded());
            const arr = Object.keys(entities).map((id) => entities[id]);
            if ( searchText.length === 0 )
            {
                console.log(arr);
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if ( signPaks )
        {
            setFilteredData(getFilteredArray(signPaks, searchText));
        }
    }, [dispatch, signPaks, searchText]);


    if ( !filteredData )
    {
        return (<Skeleton height={30} count={10} />);
    }

    if ( filteredData.length === 0 )
    {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    Belum ada Data
                </Typography>
            </div>
        );
    }
    if (!signPakLoaded)
    {
        return <Skeleton height={30} count={10} />;
    }

    return (
           <FuseAnimate animation="transition.slideUpIn" delay={300}>
                <ReactTable
                    className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                    getTrProps={(state, rowInfo, column) => {
                        return {
                            className: "cursor-pointer",
                            onClick  : (e, handleOriginal) => {
                                if ( rowInfo )
                                {
                                    // console.log(1);
                                    dispatch(Actions.toggleInSelectedSignPaks(rowInfo.original.dokumenId));
                                    dispatch(Actions.openEditSignPakDialog(rowInfo.original));
                                }
                            }
                        }
                    }}
                    data={filteredData}
                    columns={[
                        {
                            Header   : () => (
                            <div>
                                <Checkbox
                                    onClick={(event) => {
                                        event.stopPropagation();
                                    }}
                                    onChange={(event) => {
                                        event.target.checked ? dispatch(Actions.selectAllSignPaks()) : dispatch(Actions.deSelectAllSignPaks());
                                    }}
                                    checked={selectedSignPakIds.length === Object.keys(signPaks).length && selectedSignPakIds.length > 0}
                                    indeterminate={selectedSignPakIds.length !== Object.keys(signPaks).length && selectedSignPakIds.length > 0}
                                />
                                {/* <Button> Sign</Button> */}
                            </div>
                            ),
                            accessor : "",
                            Cell     : row => {
                                return (<Checkbox
                                        onClick={(event) => {
                                            event.stopPropagation();
                                        }}
                                        checked={selectedSignPakIds.includes(row.value.dokumenId)}
                                        onChange={() => dispatch(Actions.toggleInSelectedSignPaks(row.value.dokumenId))}
                                    />
                                )
                            },
                            className: "justify-center",
                            sortable : false,
                            width    : 64
                        },
                        {
                            Header      : "Tanggal",
                            Cell     : row => {
                                return (
                                    <Typography>{format_tanggal(row.original.tanggal)}</Typography>
                                )
                            },
                            filterable  : false,
                            className   : "font-bold"
                        },
                        {
                            Header      : "Nomor",
                            accessor    : "nomer",
                            filterable  : false
                        },
                        {
                            Header      : "Perihal",
                            accessor    : "perihal",
                            filterable  : false,
                            className   : "font-bold"
                        },
                        {
                            Header      : "Aplikasi",
                            accessor    : "app",
                            filterable  : false,
                            className   : "font-bold"
                        },
                        // {
                        //     Header      : "Barcode Tautan",
                        //     accessor    : "barcodeLink",
                        //     filterable  : false
                        // },
                        {
                            Header      : "Aksi",
                            width       : 128,
                            className   : "justify-center",
                            Cell : row => (
                                <div className="flex items-center">
                                    <Tooltip title="Preview  usulan " aria-label="Setujui">
                                        <Fab
                                            size="small"
                                            color="primary"
                                            aria-label="Setujui"
                                            hidden={hiddenTandatanganButton}
                                            style={{margin: "0 4px"}}
                                            onClick={(ev) => {
                                                ev.stopPropagation();
                                                dispatch(Actions.deSelectAllSignPaks());
                                                dispatch(Actions.openEditSignPakDialog(row.original));
                                            }}
                                        >
                                            <Icon>check</Icon>
                                        </Fab>
                                    </Tooltip>
                                    {/*<Tooltip title="Perlu Perbaikan Oleh Operator" aria-label="Pending">*/}
                                    {/*    <Fab*/}
                                    {/*        size="small"*/}
                                    {/*        color="inherit"*/}
                                    {/*        aria-label="Pending"*/}
                                    {/*        className={classes.dangerBtn}*/}
                                    {/*        hidden={hiddenTandatanganButton}*/}
                                    {/*        style={{margin: "0 4px"}}*/}
                                    {/*        onClick={(ev) => {*/}
                                    {/*            ev.stopPropagation();*/}
                                    {/*            // console.log('reject');*/}
                                    {/*            console.log(row.original);*/}
                                    {/*            dispatch(Actions.rejectPak(row.original.dokumenId));*/}
                                    {/*        }}*/}
                                    {/*    >*/}
                                    {/*        <Icon>restore</Icon>*/}
                                    {/*    </Fab>*/}
                                    {/*</Tooltip>*/}
                                </div>
                            )
                        }
                    ]}
                    defaultPageSize={5}
                    noDataText="Belum ada data"
                />
        </FuseAnimate>
    );
}

export default SignPaksList;