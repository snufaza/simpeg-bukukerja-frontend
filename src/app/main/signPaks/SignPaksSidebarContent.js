import React from 'react';
import {Avatar, Divider, Icon, List, ListItem, ListItemText, Paper, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FuseAnimate, NavLinkAdapter} from '@fuse';
import {useSelector} from 'react-redux';
import Button from '@material-ui/core/Button';
import { list } from 'postcss';

const useStyles = makeStyles(theme => ({
    listItem: {
        color              : 'inherit!important',
        textDecoration     : 'none!important',
        height             : 40,
        width              : 'calc(100% - 16px)',
        borderRadius       : '0 20px 20px 0',
        paddingLeft        : 24,
        paddingRight       : 12,
        '&.active'         : {
            backgroundColor    : theme.palette.secondary.main,
            color              : theme.palette.secondary.contrastText + '!important',
            pointerEvents      : 'none',
            '& .list-item-icon': {
                color: 'inherit'
            }
        },
        '& .list-item-icon': {
            marginRight: 16
        }
    }
}));

function SignPaksSidebarContent(props)
{
    const user = useSelector(({signPakApp}) => signPakApp.user);

    const classes = useStyles(props);

    return (
        <div className="p-12">
            <FuseAnimate animation="transition.slideLeftIn" delay={200}>
                <Paper className="rounded-0 shadow-none lg:rounded-8 lg:shadow-1">
                    <div className="p-24 flex items-center">
                        <Typography>Status Tandatangan</Typography>
                    </div>
                    <Divider/>
                    <List>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/sign-pak/diajukan'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">star</Icon>
                            <ListItemText className="truncate pr-0" primary="Diajukan" disableTypography={true}/>
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/sign-pak/ditandatangan'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">people</Icon>
                            <ListItemText className="truncate pr-0" primary="Ditandantangan" disableTypography={true}/>
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/sign-pak/dikembalikan'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">restore</Icon>
                            <ListItemText className="truncate pr-0" primary="Dikembalikan" disableTypography={true}/>
                        </ListItem>
                    </List>
                    <Divider/>
                    {/*<List   className={classes.listItem} >*/}

                    {/*<Button size="small" variant="contained" onClick={() => {*/}
                    {/*    window.location.assign("/logout")*/}
                    {/*}}>*/}
                    {/*        <Icon className="list-item-icon text-12" color="action">call_missed_outgoing</Icon>*/}
                    {/*    Logout*/}
                    {/*</Button>*/}
                    {/*</List>*/}
                </Paper>
            </FuseAnimate>
        </div>
    );
}

export default SignPaksSidebarContent;
