import React from 'react';
import { Icon, Input, Paper, Typography} from '@material-ui/core';
import {ThemeProvider} from '@material-ui/styles';
import {FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from './store/actions';

function SignPaksHeader(props) {
    const dispatch = useDispatch();
    const searchText = useSelector(({signPakApp}) => signPakApp.signPaks.searchText);
    const mainTheme = useSelector(({fuse}) => fuse.settings.mainTheme);

    return (
        <div className="flex flex-1 items-center justify-between p-12">
            <div className="flex flex-shrink items-center sm:w-224">
                <div className="flex items-center">
                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                        <Typography variant="h6" className="hidden sm:flex">Tanda Tangan Digital</Typography>
                    </FuseAnimate>
                </div>
            </div>

            <div className="flex flex-1 justify-end">
                <ThemeProvider theme={mainTheme}>
                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                        <Paper className="flex p-4 items-center w-full max-w-512 px-8 py-4" elevation={1}>
                            <Icon className="mr-8" color="action">search</Icon>
                            <Input
                                placeholder="Pencarian dokumen tanda tangan..."
                                className="flex flex-1"
                                disableUnderline
                                fullWidth
                                value={searchText}
                                inputProps={{
                                    'aria-label': 'Search'
                                }}
                                onChange={ev => dispatch(Actions.setSearchText(ev))}
                                autoComplete="false"
                            />
                        </Paper>
                    </FuseAnimate>
                </ThemeProvider>
            </div>
        </div>
    );
}

export default SignPaksHeader;