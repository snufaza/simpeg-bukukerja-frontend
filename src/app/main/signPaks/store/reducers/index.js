import {combineReducers} from 'redux';
import signPaks from './signPaks.reducer';
import user from './user.reducer';
import berandaReducer from "../../../beranda/store/reducers/beranda.reducer";

const reducer = combineReducers({
    signPaks,
    berandaReducer,
    user
});

export default reducer;
