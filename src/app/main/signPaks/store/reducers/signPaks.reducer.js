import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    sign_pak_loaded : false,
    entities                : null,
    hiddenTandatanganButton : false,
    searchText              : '',
    judulApp                : 'BUKU KERJA',
    selectedSignPakIds      : [],
    routeParams             : {},
    opt_instansi            : [],
    opt_instansi_sel        : null,
    opt_jabatan            : [],
    opt_jabatan_sel        : null,
    loading        : false,
    signPakDialog           : {
        type    : 'new',
        props : {
            open: false
        },
        data    : null
    },
    reqBsreSignDialog           : {
        type    : 'new',
        props : {
            open: false
        },
        data    : null
    },
    signPakPassPharseDialog     : {
        type    : 'new',
        props : {
            open: false
        },
        data    : null
    }
};

const signPaksReducer = function (state = initialState, action) {
    switch ( action.type ) {
        case Actions.GET_SIGN_PAKS:
        {
            return {
                ...state,
                entities: _.keyBy(action.payload, 'dokumenId'),
                hiddenTandatanganButton: action.hiddenTandatanganButton,
                routeParams: action.routeParams
            };
        }
        case Actions.SET_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        case Actions.SET_JUDUL_APP:
        {
            return {
                ...state,
                judulApp: action.judul
            };
        }
        case Actions.TOGGLE_IN_SELECTED_SIGN_PAKS:
        {

            const signPakId = action.signPakId;
            let selectedSignPakIds = [...state.selectedSignPakIds];
        

            if ( selectedSignPakIds.find(id => id === signPakId) !== undefined )
            {
                selectedSignPakIds = selectedSignPakIds.filter(id => id !== signPakId);
            }
            else
            {
                selectedSignPakIds = [...selectedSignPakIds, signPakId];
            }
            return {
                ...state,
                selectedSignPakIds: selectedSignPakIds
            };
        }
        case Actions.SELECT_ALL_SIGN_PAKS:
        {
            const arr = Object.keys(state.entities).map(k => state.entities[k]);

            const selectedSignPakIds = arr.map(signPak => signPak.dokumenId);

            return {
                ...state,
                selectedSignPakIds: selectedSignPakIds
            };
        }
        case Actions.DESELECT_ALL_SIGN_PAKS:
        {
            return {
                ...state,
                selectedSignPakIds: []
            };
        }
        case Actions.OPEN_NEW_SIGN_PAK_DIALOG:
        {
            return {
                ...state,
                signPakDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        case Actions.CLOSE_NEW_SIGN_PAK_DIALOG:
        {
            return {
                ...state,
                signPakDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_EDIT_SIGN_PAK_DIALOG:
        {
            return {
                ...state,
                signPakDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : action.data
                }
            };
        }
        case Actions.CLOSE_EDIT_SIGN_PAK_DIALOG:
        {
            return {
                ...state,
                signPakDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_NEW_REQ_SIGN_DIALOG:
        {
            return {
                ...state,
                reqBsreSignDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        case Actions.CLOSE_NEW_REQ_SIGN_DIALOG:
        {
            return {
                ...state,
                reqBsreSignDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_EDIT_REQ_SIGN_DIALOG:
        {
            return {
                ...state,
                reqBsreSignDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : action.data
                }
            };
        }
        case Actions.CLOSE_EDIT_REQ_SIGN_DIALOG:
        {
            return {
                ...state,
                reqBsreSignDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_NEW_SIGN_PAK_PASS_PHARSE_DIALOG:
        {

            return {
                ...state,
                signPakPassPharseDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : action.data
                }
            };
        }
        case Actions.CLOSE_NEW_SIGN_PAK_PASS_PHARSE_DIALOG:
        {
            return {
                ...state,
                signPakPassPharseDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG:
        {
            return {
                ...state,
                signPakPassPharseDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : action.data
                }
            };
        }
        case Actions.CLOSE_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG:
        {
            return {
                ...state,
                signPakPassPharseDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.SIGN_PAK_LOAD:
        {
            return {
                ...state,
                sign_pak_loaded: true
            };
        }
        case Actions.SIGN_PAK_STOP_LOAD:
        {
            return {
                ...state,
                sign_pak_loaded: false
            };
        }
        case Actions.LOAD_INSTANSI:
        {
            return {
                ...state,
                opt_instansi: action.data
            };
        }
        case Actions.SET_INSTANSI:
        {
            return {
                ...state,
                opt_instansi_sel: action.data
            };
        }
        case Actions.LOAD_JABATAN:
        {
            return {
                ...state,
                opt_jabatan: action.data
            };
        }
        case Actions.SET_JABATAN:
        {
            return {
                ...state,
                opt_jabatan_sel: action.data
            };
        }
        case Actions.SHOW_LOADING:
        {
            return {
                ...state,
                loading: true
            };
        }
        case Actions.HIDE_LOADING:
        {
            return {
                ...state,
                loading: false
            };
        }
        default:
        {
            return state;
        }
    }
};

export default signPaksReducer;