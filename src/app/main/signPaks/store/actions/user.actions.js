import axios from 'axios';

export const GET_USER_DATA = '[CONTACTS APP] GET USER DATA';

export function getUserData()
{
    const request = axios.get('/api/token');

    return (dispatch) =>
        request.then((response) => {
            return dispatch({
                type   : GET_USER_DATA,
                payload: response.data.user.data
            })
        }
        ).catch((error)=>{
            console.log('ahmad');
        });
}