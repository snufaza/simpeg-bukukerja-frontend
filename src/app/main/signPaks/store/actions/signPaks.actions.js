import axios from 'axios';
import {getUserData} from 'app/main/signPaks/store/actions/user.actions';
import * as Actions from 'app/store/actions';
// import {GET_USER_DATA} from "../../../profilPengguna/store/actions";

export const GET_SIGN_PAKS = '[SIGN_PAKS APP] GET SIGN_PAKS';
export const SET_SEARCH_TEXT = '[SIGN_PAKS APP] SET SEARCH TEXT';
export const SET_JUDUL_APP = '[SIGN_PAKS APP] SET JUDUL APP';
export const TOGGLE_IN_SELECTED_SIGN_PAKS = '[SIGN_PAKS APP] TOGGLE IN SELECTED SIGN_PAKS';
export const SELECT_ALL_SIGN_PAKS = '[SIGN_PAKS APP] SELECT ALL SIGN_PAKS';
export const DESELECT_ALL_SIGN_PAKS = '[SIGN_PAKS APP] DESELECT ALL SIGN_PAKS';

export const OPEN_NEW_SIGN_PAK_DIALOG = '[SIGN_PAKS APP] OPEN NEW SIGN_PAK DIALOG';
export const CLOSE_NEW_SIGN_PAK_DIALOG = '[SIGN_PAKS APP] CLOSE NEW SIGN_PAK DIALOG';
export const OPEN_EDIT_SIGN_PAK_DIALOG = '[SIGN_PAKS APP] OPEN EDIT SIGN_PAK DIALOG';
export const CLOSE_EDIT_SIGN_PAK_DIALOG = '[SIGN_PAKS APP] CLOSE EDIT SIGN_PAK DIALOG';

export const OPEN_NEW_REQ_SIGN_DIALOG = '[SIGN_PAKS APP] OPEN_NEW_REQ_SIGN_DIALOG';
export const CLOSE_NEW_REQ_SIGN_DIALOG = '[SIGN_PAKS APP] CLOSE_NEW_REQ_SIGN_DIALOG';
export const OPEN_EDIT_REQ_SIGN_DIALOG = '[SIGN_PAKS APP] OPEN_EDIT_REQ_SIGN_DIALOG';
export const CLOSE_EDIT_REQ_SIGN_DIALOG = '[SIGN_PAKS APP] CLOSE_EDIT_REQ_SIGN_DIALOG';

export const OPEN_NEW_SIGN_PAK_PASS_PHARSE_DIALOG = '[SIGN_PAKS APP] OPEN NEW SIGN_PAK PASS PHARSE DIALOG';
export const CLOSE_NEW_SIGN_PAK_PASS_PHARSE_DIALOG = '[SIGN_PAKS APP] CLOSE NEW SIGN_PAK PASS PHARSE DIALOG';
export const OPEN_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG = '[SIGN_PAKS APP] OPEN EDIT SIGN_PAK PASS PHARSE DIALOG';
export const CLOSE_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG = '[SIGN_PAKS APP] CLOSE EDIT SIGN_PAK PASS PHARSE DIALOG';

export const SEND_REQUEST_SIGN_BSRE = '[SEND REQUEST BSRE] REQUEST';

export const ADD_SIGN_PAK = '[SIGN_PAKS APP] ADD SIGN_PAK';
export const UPDATE_SIGN_PAK = '[SIGN_PAKS APP] UPDATE SIGN_PAK';
export const REMOVE_SIGN_PAK = '[SIGN_PAKS APP] REMOVE SIGN_PAK';
export const REMOVE_SIGN_PAKS = '[SIGN_PAKS APP] REMOVE SIGN_PAKS';
export const TOGGLE_STARRED_SIGN_PAK = '[SIGN_PAKS APP] TOGGLE STARRED SIGN_PAK';
export const TOGGLE_STARRED_SIGN_PAKS = '[SIGN_PAKS APP] TOGGLE STARRED SIGN_PAKS';
export const SET_SIGN_PAKS_STARRED = '[SIGN_PAKS APP] SET SIGN_PAKS STARRED ';
export const SIGN_PAK_LOAD = '[SIGN_PAKS APP] SET SIGN PAK LOAD';
export const SIGN_PAK_STOP_LOAD = '[SIGN_PAKS APP] SET SIGN PAK STOP LOAD';

export const LOAD_INSTANSI = '[DAFTAR] LOAD INSTANSI';
export const SET_INSTANSI = '[DAFTAR] SET INSTANSI';

export const LOAD_JABATAN = '[DAFTAR] LOAD JABATAN';
export const SET_JABATAN = '[DAFTAR] SET JABATAN';

export const SHOW_LOADING = '[SIGN_PAK] SHOW LOADING';
export const HIDE_LOADING = '[SIGN_PAK] HIDE LOADING';

// export const baseUrl = window.location.origin.match("(?=localhost)") ? 'http://localhost:8000' : 'http://simpeg.pendidikan.gunungkidulkab.go.id';

export function getSignPaks(routeParams)
{
    // delete axios.defaults.headers.common["Authorization"];
    const request = axios.get(axios.defaults.baseURL+ '/api/dokumen_tandatangans?', {
        params: routeParams
    });

    let hiddenTandatanganButton;
    if(routeParams.id==="diajukan"){
        hiddenTandatanganButton=false;
    }else {
        hiddenTandatanganButton=true;
    }
    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_SIGN_PAKS,
                // payload: response.data['hydra:member'],
                payload: response.data.data,
                routeParams,
                hiddenTandatanganButton:hiddenTandatanganButton
            });
        }).catch((error)=>{
            dispatch({
                type: GET_SIGN_PAKS,
                payload: {},
                routeParams,
                hiddenTandatanganButton:hiddenTandatanganButton
            });
        });
}

export function setJudulApp(judul)
{
    return {
        type: SET_JUDUL_APP,
        judul: judul
    }
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedSignPaks(signPakId)
{
    return {
        type: TOGGLE_IN_SELECTED_SIGN_PAKS,
        signPakId
    }
}

export function selectAllSignPaks()
{
    return {
        type: SELECT_ALL_SIGN_PAKS
    }
}

export function deSelectAllSignPaks()
{
    return {
        type: DESELECT_ALL_SIGN_PAKS
    }
}

export function openNewSignPakDialog()
{
    return {
        type: OPEN_NEW_SIGN_PAK_DIALOG
    }
}

export function closeNewSignPakDialog()
{
    return {
        type: CLOSE_NEW_SIGN_PAK_DIALOG
    }
}

export function openEditSignPakDialog(data)
{
    return {
        type: OPEN_EDIT_SIGN_PAK_DIALOG,
        data
    }
}

export function closeEditSignPakDialog()
{
    return {
        type: CLOSE_EDIT_SIGN_PAK_DIALOG
    }
}

export function openNewReqSignDialog()
{
    return {
        type: OPEN_NEW_REQ_SIGN_DIALOG
    }
}

export function closeNewReqSignDialog()
{
    return {
        type: CLOSE_NEW_REQ_SIGN_DIALOG
    }
}

export function openEditReqSignDialog(data)
{
    return {
        type: OPEN_EDIT_REQ_SIGN_DIALOG,
        data
    }
}

export function closeEditReqSignDialog()
{
    return {
        type: CLOSE_EDIT_REQ_SIGN_DIALOG
    }
}

export function sendRequestSignBsre(data)
{
    // console.log(data);

    // /api/registerTtdeBsre/2E91FC26-4B0F-43E9-BEE6-4B6AE22905D7
    return (dispatch, getState) => {
        const request = axios.post('/api/register_bsre/'+data.id);
        request.then((response) =>{
            dispatch(hideLoading());
            dispatch(Actions.showMessage({ message: response.data.message }));
            dispatch(Actions.setDefaultSettings({}));
        });
    }
}

export function addSignPak(newSignPak)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/add-signPak', {
            newSignPak
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: ADD_SIGN_PAK
                })
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function updateSignPak(signPak)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/update-signPak', {
            signPak
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: UPDATE_SIGN_PAK
                })
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function removeSignPak(signPakId)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/remove-signPak', {
            signPakId
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_SIGN_PAK
                })
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}


export function removeSignPaks(signPakIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/remove-signPaks', {
            signPakIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_SIGN_PAKS
                }),
                dispatch({
                    type: DESELECT_ALL_SIGN_PAKS
                })
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function signPak(pakIds,passPhrase)
{
    return (dispatch, getState) => {
        const {routeParams} = getState().signPakApp.signPaks;
        const request = axios.post( '/api/sign/post', {
            params: {
                passphrase : passPhrase,
                sign_paks: pakIds
            }
        });

        return request.then((response) => {
            return Promise.all([dispatch(getUserData(), dispatch(Actions.showMessage({ message: response.data.message })))
            ,dispatch(hideLoading()),dispatch(getSignPaks(routeParams)),dispatch(signPakLoad())]);
        }
            
        );
    };
}

export function signPaks(pakId,passPhrase)
{
    return (dispatch, getState) => {
        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.get( '/Tandatangan/signs', {
            params: {
                passphrase : passPhrase,
                file_names : pakId
            }
        });
        return request.then((response) =>{
            return Promise.all([dispatch(getUserData(), dispatch(Actions.showMessage({ message: response.data.Message })))
            ]).then(() => dispatch(getSignPaks(routeParams)));
        }
            
        );
    };
}

export function rejectPak(pakId)
{

    return (dispatch, getState) => {
        const {routeParams} = getState().signPakApp.signPaks;
        console.log(pakId);
        const request = axios.post('/api/sign/reject/post', {
            params: {
                sign_paks: pakId
            }
        });
        return request.then((response) =>{
            return Promise.all([dispatch(getUserData(), dispatch(Actions.showMessage({ message: response.data.message }))),dispatch(getSignPaks(routeParams))
            ,dispatch(hideLoading()),dispatch(signPakLoad())]);
        }
            
        );
    };
}
export function toggleStarredSignPak(signPakId)
{
    return (dispatch, getState) => {
        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/toggle-starred-signPak', {
            signPakId
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_SIGN_PAK
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function toggleStarredSignPaks(signPakIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/toggle-starred-signPaks', {
            signPakIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_SIGN_PAKS
                }),
                dispatch({
                    type: DESELECT_ALL_SIGN_PAKS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function setSignPaksStarred(signPakIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/set-signPaks-starred', {
            signPakIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_SIGN_PAKS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_SIGN_PAKS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function setSignPaksUnstarred(signPakIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().signPaksApp.signPaks;

        const request = axios.post('/api/signPaks-app/set-signPaks-unstarred', {
            signPakIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_SIGN_PAKS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_SIGN_PAKS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getSignPaks(routeParams)))
        );
    };
}

export function openNewSignPakPassPharseDialog(data)
{
    return {
        type: OPEN_NEW_SIGN_PAK_PASS_PHARSE_DIALOG,
        data
    }
}

export function closeNewSignPakPassPharseDialog()
{
    return {
        type: CLOSE_NEW_SIGN_PAK_PASS_PHARSE_DIALOG
    }
}

export function openEditSignPakPassPharseDialog(data)
{
    return {
        type: OPEN_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG,
        data
    }
}

export function closeEditSignPakPassPharseDialog()
{
    return {
        type: CLOSE_EDIT_SIGN_PAK_PASS_PHARSE_DIALOG
    }
}

export function signPakLoaded()
{
    return {
        type: SIGN_PAK_LOAD
    }
}
export function signPakLoad()
{
    return {
        type: SIGN_PAK_STOP_LOAD
    }
}


export function getInstansis(routeParams)
{
    // delete axios.defaults.headers.common["Authorization"];
    const request = axios.get('/api/ref_instansis', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) => {
           const optInstansi = response.data.data.map(r=>{
               return {label:r.nama,value:r.instansiId};
            });
            dispatch({
                type: LOAD_INSTANSI,
                data: optInstansi
            });
        }).catch((error)=>{
            dispatch({
                type: LOAD_INSTANSI,
                data: []
            });
        });
}
export function getJabatans(routeParams)
{
    // delete axios.defaults.headers.common["Authorization"];
    const request = axios.get('/api/ref_jabatans', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) => {
           const optJabatan = response.data.data.map(r=>{
               return {label:r.nama,value:r.jabatanId};
            });
            dispatch({
                type: LOAD_JABATAN,
                data: optJabatan
            });
        }).catch((error)=>{
            dispatch({
                type: LOAD_JABATAN,
                data: []
            });
        });
}
export function setInstansi(instansi)
{
    return (dispatch)=> dispatch({
                type: SET_INSTANSI,
                data: instansi
            });
}
export function setJabatan(jabatan)
{
    return (dispatch)=> dispatch({
                type: SET_JABATAN,
                data: jabatan
            });
}
export function showLoading()
{
    console.log('show loading');
    return (dispatch)=> dispatch({
                type: SHOW_LOADING
            });
}
export function hideLoading()
{
    return (dispatch)=> dispatch({
                type: HIDE_LOADING
            });
}