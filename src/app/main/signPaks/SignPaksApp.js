/** @format */

import React, { useEffect, useRef } from "react";
import {
	Fab,
	Icon,
	FormControl,
	InputLabel,
	Select,
	OutlinedInput,
	MenuItem
} from "@material-ui/core";
import { FusePageSimple, FuseAnimate } from "@fuse";
import { useDispatch, useSelector } from "react-redux";
import withReducer from "app/store/withReducer";
import SignPaksList from "./SignPaksList";
import SignPaksHeader from "./SignPaksHeader";
import SignPakDialog from "./SignPakDialog";
import * as Actions from "./store/actions";
import reducer from "./store/reducers";
import { makeStyles } from "@material-ui/styles";
import SignPakPassPharseDialog from "./SignPakPassPharseDialog";
import { createBrowserHistory } from "history";
// import Skeleton from 'react-loading-skeleton';
// import ReqBsreSignDialog from "./ReqBsreSignDialog";
// import {FuseLoading} from "../../../@fuse";
// import LoadingDialog from "../component/LoadingDialog";
import SignLoadingDialog from "../component/SignLoadingDialog";

const history = createBrowserHistory();

const useStyles = makeStyles({
	addButton: {
		position: "absolute",
		right: 12,
		bottom: 12,
		zIndex: 99
	},
	formControl: {
		margin: "6px 6px 6px auto",
		minWidth: 200
	},
	selectEmpty: {
		marginTop: 12
	}
});

function SignPaksApp(props) {
	const dispatch = useDispatch();
	const classes = useStyles(props);
	const pageLayout = useRef(null);
	// const signPakLoaded = useSelector(({ signPakApp }) => signPakApp.signPaks.sign_pak_loaded);
	// const selectedSignPakIds = useSelector(({ signPakApp }) => signPakApp.signPaks.selectedSignPakIds);
	const hiddenTandatanganButton = useSelector(
		({ signPakApp }) => signPakApp.signPaks.hiddenTandatanganButton
	);
	const loading = useSelector(({ signPakApp }) => signPakApp.signPaks.loading);
	const [values, setValues] = React.useState({
		statusTandatangan: ""
	});

	const inputLabel = React.useRef(null);
	const [labelWidth, setLabelWidth] = React.useState(0);

	React.useEffect(() => {
		setLabelWidth(inputLabel.current.offsetWidth);
	}, []);

	function handleChange(event) {
		setValues(oldValues => ({
			...oldValues,
			[event.target.name]: event.target.value
		}));

		const urlBaseModul = props.match.path.split(":")[0];
		const urlParams = props.match.params;
		dispatch(
			Actions.getSignPaks({
				id: event.target.value,
				clientApp: urlParams.clientApp
			})
		);
		history.push(urlBaseModul + urlParams.clientApp + "/" + event.target.value);

		dispatch(Actions.signPakLoad());
	}

	useEffect(() => {
		dispatch(Actions.getSignPaks(props.match.params));
		dispatch(Actions.getUserData());

		setValues({
			statusTandatangan: props.match.params.id
		});

		dispatch(Actions.getSignPaks(props.match.params));
	}, [dispatch, props.match.params]);

	function dialogOpen(params) {
		dispatch(Actions.openNewSignPakPassPharseDialog());
	}
	return (
		<React.Fragment>
			<SignLoadingDialog open={loading} />
			<FusePageSimple
				classes={{
					contentWrapper: "p-12 h-full",
					content: "flex flex-col h-full",
					header: "min-h-72 h-72"
				}}
				header={<SignPaksHeader pageLayout={pageLayout} />}
				content={
					<div>
						<div className="flex flex-1 items-center justify-between mb-12">
							<FuseAnimate animation="transition.slideUpIn" delay={100}>
								<Fab
									hidden={hiddenTandatanganButton}
									size="small"
									onClick={dialogOpen}
									color="secondary"
									variant="extended"
									aria-label="Sign Checked">
									<Icon className="mr-4" fontSize="small">
										edit_icon
									</Icon>{" "}
									Tandatangani Ceklis
								</Fab>
							</FuseAnimate>

							<FuseAnimate animation="transition.slideUpIn" delay={200}>
								<div className="flex flex-1 items-center justify-end">
									<FormControl
										variant="outlined"
										className={classes.formControl}>
										<InputLabel ref={inputLabel} htmlFor="status-tandatangan">
											Pilih Status Dokumen
										</InputLabel>
										<Select
											value={values.statusTandatangan}
											onChange={handleChange}
											input={
												<OutlinedInput
													labelWidth={labelWidth}
													name="statusTandatangan"
													id="status-tandatangan"
												/>
											}>
											<MenuItem value="diajukan">Dokumen Usulan</MenuItem>
											<MenuItem value="ditandatangan">Ditandatangani</MenuItem>
											<MenuItem value="dikembalikan">Perbaikan</MenuItem>
										</Select>
									</FormControl>

									<Fab
										size="small"
										color="primary"
										variant="extended"
										aria-label="Unduh XLS"
										className="sm:ml-8">
										<Icon className="mr-4" fontSize="small">
											insert_drive_file
										</Icon>{" "}
										XLS
									</Fab>
								</div>
							</FuseAnimate>
						</div>

						{/*<Typography hidden={signPakLoaded}>*/}
						{/*    <Skeleton height={30} count={10} />;*/}
						{/*</Typography>*/}

						<SignPaksList />
					</div>
				}
				sidebarInner
				ref={pageLayout}
			/>
			{/*<LoadingDialog open={loading}/>*/}
			<SignPakDialog />
			<SignPakPassPharseDialog />
		</React.Fragment>
	);
}

export default withReducer("signPakApp", reducer)(SignPaksApp);
