import React, {useState} from 'react';
import {Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList} from '@material-ui/core';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';

function SignPaksMultiSelectMenu(props)
{
    const dispatch = useDispatch();
    const selectedSignPakIds = useSelector(({signPaksApp}) => signPaksApp.signPaks.selectedSignPakIds);

    const [anchorEl, setAnchorEl] = useState(null);

    function openSelectedSignPakMenu(event)
    {
        setAnchorEl(event.currentTarget);
    }

    function closeSelectedSignPaksMenu()
    {
        setAnchorEl(null);
    }

    return (
        <React.Fragment>
            <IconButton
                className="p-0"
                aria-owns={anchorEl ? 'selectedSignPaksMenu' : null}
                aria-haspopup="true"
                onClick={openSelectedSignPakMenu}
            >
                <Icon>more_horiz</Icon>
            </IconButton>
            <Menu
                id="selectedSignPaksMenu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={closeSelectedSignPaksMenu}
            >
                <MenuList>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.removeSignPaks(selectedSignPakIds));
                            closeSelectedSignPaksMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>delete</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Remove"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setSignPaksStarred(selectedSignPakIds));
                            closeSelectedSignPaksMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Starred"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setSignPaksUnstarred(selectedSignPakIds));
                            closeSelectedSignPaksMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star_border</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Unstarred"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            window.location('/logout');
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>logout</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Logout"/>
                    </MenuItem>
                </MenuList>
            </Menu>
        </React.Fragment>
    );
}

export default SignPaksMultiSelectMenu;

