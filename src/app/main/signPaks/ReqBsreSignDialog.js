/** @format */

import React, { useEffect, useCallback } from "react";
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	Icon,
	IconButton,
	Typography,
	Toolbar,
	AppBar
} from "@material-ui/core";
// import {useForm} from '@fuse/hooks';
// import FuseUtils from '@fuse/FuseUtils';
import * as Actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";

// const defaultFormState = {
//     pak_id      : '',
//     id          : '',
//     name        : '',
//     lastName    : '',
//     avatar      : 'assets/images/avatars/profile.jpg',
//     nickname    : '',
//     company     : '',
//     jobTitle    : '',
//     email       : '',
//     phone       : '',
//     address     : '',
//     birthday    : '',
//     notes       : ''
// };

function ReqBsreSignDialog(props) {
	const user = useSelector(({ ProfilPengguna }) => ProfilPengguna.user);
	const dispatch = useDispatch();
	const reqBsreSignDialog = useSelector(
		({ signPakApp }) => signPakApp.signPaks.reqBsreSignDialog
	);

	// const {form, setForm} = useForm(defaultFormState);
	// const [open, setOpen] = React.useState(false);

	function sendReqSign() {
		dispatch(Actions.showLoading());
		console.log("showloading");
		dispatch(Actions.sendRequestSignBsre(user));
	}

	function handleClose() {
		// setOpen(false);
		closeComposeDialog();
	}

	const initDialog = useCallback(() => {
		if (reqBsreSignDialog.type === "edit" && reqBsreSignDialog.data) {
			// setForm({...reqBsreSignDialog.data});
		}

		if (reqBsreSignDialog.type === "new") {
			// setForm({
			//     ...defaultFormState,
			//     ...reqBsreSignDialog.data,
			//     id: FuseUtils.generateGUID()
			// });
		}
	}, [reqBsreSignDialog.data, reqBsreSignDialog.type]);

	useEffect(() => {
		if (reqBsreSignDialog.props.open) {
			initDialog();
		}
	}, [reqBsreSignDialog.props.open, initDialog]);

	function closeComposeDialog() {
		reqBsreSignDialog.type === "edit"
			? dispatch(Actions.closeEditReqSignDialog())
			: dispatch(Actions.closeNewReqSignDialog());
	}

	// function canBeSubmitted() {

	// }

	function handleSubmit() {}

	// function handleRejectPak(id) {
	//     // dispatch(Actions.rejectPak(form.pak_id));
	//     closeComposeDialog();
	// }

	return (
		<div>
			<Dialog
				classes={{
					paper: "m-64"
				}}
				{...reqBsreSignDialog.props}
				onClose={closeComposeDialog}
				fullWidth
				maxWidth="sm">
				<AppBar position="static" elevation={1}>
					<Toolbar className="flex flex-1 justify-between w-full">
						<Typography variant="subtitle1" color="inherit">
							Pratinjau
						</Typography>
						<IconButton
							aria-label="Tutup"
							color="inherit"
							onClick={handleClose}>
							<Icon fontSize="small">close</Icon>
						</IconButton>
					</Toolbar>
				</AppBar>
				<form
					noValidate
					onSubmit={handleSubmit}
					className="flex flex-col overflow-hidden">
					<DialogContent>
						<p>Tekan tombol dibawah untuk menyetujui pengajuan</p>
					</DialogContent>
					<DialogActions className="flex flex-1 justify-start p-12">
						<Button variant="contained" color="secondary" onClick={sendReqSign}>
							<Icon className="mr-4">check</Icon> Ajukan Bsre
						</Button>

						<Button variant="contained" onClick={closeComposeDialog}>
							<Icon className="mr-4">cancel</Icon> tutup
						</Button>
					</DialogActions>
				</form>
			</Dialog>
		</div>
	);
}

export default ReqBsreSignDialog;
