import React from 'react';
import { Dialog, DialogContent} from '@material-ui/core';
import {FuseLoading} from "../../../@fuse";


function LoadingDialog(props) {
    const styles = theme => ({
        separator: {
            width          : 1,
            height         : 50,
            backgroundColor: theme.palette.divider
        },
        title: {
            display         : 'block',
            width           : 200,
            position        : 'relative',
            whiteSpace      : 'nowrap',
            textOverflow    : 'ellipsis',
            overflow        : 'hidden',
            textTransform   : 'capitalize'
        },
        paperModal: {
            letterSpacing: 1,
            boxShadow: 'none',
            backgroundColor: 'transparent',
            color: '#fff',
            fontSize: 14
        },
        backdrop: {
            color: '#fff',
            background: 'linear-gradient(-45deg, #ee775259, #3c80e759, #23a6d559, #23d5ab59)',
            backgroundSize: '400% 400%',
            animation: 'gradientBG 15s ease infinite'
        },
        iconLoading: {
            display: 'block',
            borderRadius: '50%',
            marginBottom: 8,
            opacity: '.85'
        }
    });
    return (
        <Dialog
            open={props.loading}
            style={{boxShadow: 'none'}}
        >
            <DialogContent id="loading-dialog" style={{textAlign: 'center', padding: 0, color: '#0a5da0'}}>
                <div>
                    <FuseLoading></FuseLoading>
                </div>
            </DialogContent>
        </Dialog>
    );
}

export default LoadingDialog;
{/*<LoadingDialog loading={true} message={"mohon tunggu sebentar"} />*/}
