import React from 'react';
import { Dialog, DialogContent} from '@material-ui/core';
import DialogContentText from '@material-ui/core/DialogContentText';
import {FuseLoading} from "../../../@fuse";
function SignLoadingDialog(props) {
    return (
        <div>
            <Dialog 
                open={props.open}
                aria-labelledby="form-dialog-title"
            >
                <DialogContent>
                    <DialogContentText>

                    </DialogContentText>
                    <FuseLoading/>
                </DialogContent>
            </Dialog>
        </div>
    );
}

export default SignLoadingDialog;