/** @format */

import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
	Tab,
	Tabs,
	Typography,
	Breadcrumbs,
	Paper,
	Box,
	Button,
	IconButton,
	Tooltip,
	FormControl,
	Grid,
	InputBase,
	Select,
	Hidden,
	FormHelperText
} from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import { FusePageSimple } from "@fuse";
import { useForm } from "@fuse/hooks";
import { Link } from "react-router-dom";
import clsx from "clsx";
import _ from "@lodash";
import { useDispatch, useSelector } from "react-redux";
import withReducer from "app/store/withReducer";
import * as Actions from "./store/actions";
import reducer from "./store/reducers";
import SaveIcon from "@material-ui/icons/Save";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";

const CustomIconButton = withStyles(theme => ({
	root: {
		color: "#333",
		backgroundColor: "#f6f6f6",
		"&:hover": {
			backgroundColor: "#f6f6f6"
		}
	}
}))(IconButton);

const BootstrapInput = withStyles(theme => ({
	input: {
		borderRadius: 0,
		position: "relative",
		backgroundColor: theme.palette.common.white,
		border: "1px solid #ced4da",
		fontSize: 14,
		width: "100%",
		padding: "8px 12px 7.38px 12px",
		transition: theme.transitions.create(["border-color"]),
		"&:focus": {
			borderColor: "#00a65a"
		}
	}
}))(InputBase);

const BootstrapSelect = withStyles(theme => ({
	input: {
		borderRadius: 0,
		position: "relative",
		backgroundColor: theme.palette.background.paper,
		border: "1px solid #ced4da",
		fontSize: 14,
		width: "100%",
		padding: "8px 36px 7.38px 12px",
		transition: theme.transitions.create(["border-color"]),
		"&:focus": {
			borderColor: "#00a65a"
		}
	}
}))(InputBase);

const useStyles = makeStyles(theme => ({
	layoutRoot: {
		flexGrow: 1
	},
	headerTitle: {
		padding: 0,
		height: 44,
		minHeight: 44,
		background: "#fafafa",
		[theme.breakpoints.down("sm")]: {
			height: "auto"
		}
	},
	detailMaster: {
		padding: 12,
		borderRadius: 0,
		boxShadow: "0px 2px 5px #0000000d"
	},
	linkButton: {
		borderBottom: "1px solid #efefef",
		paddingRight: 12
	},
	input: {
		position: "relative"
	}
}));

const SimTabs = withStyles({
	root: {
		borderBottom: "1px solid #efefef",
		minHeight: 44,
		width: "100%"
	}
})(Tabs);

const SimTab = withStyles(theme => ({
	root: {
		textTransform: "none",
		minWidth: 72,
		fontWeight: theme.typography.fontWeightRegular,
		minHeight: 44
	}
}))(props => <Tab disableRipple {...props} />);

function TabPanel(props) {
	const { children, value, index, ...other } = props;

	return (
		<Typography
			component="div"
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}>
			<Box className="p-12">{children}</Box>
		</Typography>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired
};

function a11yProps(index) {
	return {
		id: `simple-tab-${index}`,
		"aria-controls": `simple-tabpanel-${index}`
	};
}

function PerManajemenPengguna(props) {
	const dispatch = useDispatch();
	const user = useSelector(
		({ manajemenPenggunaApp }) => manajemenPenggunaApp.user
	);

	const classes = useStyles(props);
	const [value, setValue] = useState(0);
	const { form, handleChange, setForm } = useForm(null);

	useEffect(() => {
		function updateUserState() {
			const params = props.match.params;
			const { penggunaId } = params;

			if (penggunaId === "baru") {
				dispatch(Actions.newUser());
			} else {
				dispatch(Actions.getUser(props.match.params));
			}
		}

		updateUserState();

		if (user.peran.length === 0) {
			dispatch(Actions.getPeran());
		}
	}, [dispatch, props.match.params, user.peran.length]);

	useEffect(() => {
		if (
			(user.data && !form) ||
			(user.data && form && user.data.penggunaId !== form.penggunaId)
		) {
			delete user.data.password;
			setForm(user.data);
		}
	}, [form, user.data, setForm]);

	function handleChangeTabs(event, newValue) {
		setValue(newValue);
	}

	function canBeSubmitted() {
		return form.nama.length > 0 && !_.isEqual(user.data, form);
	}

	return (
		<FusePageSimple
			classes={{
				root: classes.layoutRoot,
				header: classes.headerTitle
			}}
			header={
				form && (
					<div className="flex flex-1 flex-col sm:flex-row justify-between items-center px-12 pt-12">
						<Typography variant="h6" className="text-black">
							{form.penggunaId ? "Edit Pengguna" : "Tambah Pengguna"}
						</Typography>
						<Hidden smDown>
							<Breadcrumbs
								separator="›"
								aria-label="breadcrumb"
								style={{ color: "rgba(0, 0, 0, 0.7)" }}>
								<Link color="textPrimary" to="/beranda">
									Beranda
								</Link>
								<Link color="textPrimary" to="/manajemen-pengguna">
									Manajemen Pengguna
								</Link>
								<Typography className="text-grey-darker">
									{form.penggunaId ? "Edit Pengguna" : "Tambah Pengguna"}
								</Typography>
							</Breadcrumbs>
						</Hidden>
					</div>
				)
			}
			content={
				form && (
					<div className="p-12">
						<div>
							<Paper className={clsx(classes.detailMaster, "p-0")}>
								<div className="flex flex-1 justify-between">
									<SimTabs
										value={value}
										onChange={handleChangeTabs}
										aria-label="Sim Tabs">
										<SimTab label="Rincian Pengguna" {...a11yProps(0)} />
									</SimTabs>
									<div
										className={clsx(classes.linkButton, "flex items-center")}>
										<Tooltip
											aria-label="Kembali ke Daftar Pengguna"
											title="Kembali ke Daftar Pengguna">
											<CustomIconButton
												aria-label="Kembali ke Daftar Pengguna"
												size="small"
												className="mr-4"
												component={Link}
												to="/manajemen-pengguna">
												<KeyboardBackspaceIcon fontSize="inherit" />
											</CustomIconButton>
										</Tooltip>
									</div>
								</div>
								<TabPanel value={value} index={0}>
									<div>
										<Grid container spacing={1}>
											<Grid item xs={12} sm={6}>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">
														Nama Pengguna<span className="text-red">*</span>
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															name="nama"
															value={form ? form.nama : ""}
															onChange={handleChange}
															id="nama"
															autoFocus
														/>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">
														Email<span className="text-red">*</span>
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															type="text"
															name="username"
															value={form ? form.email : ""}
															onChange={handleChange}
															id="username"
														/>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-6 sm:mb-0">
														Konfirmasi Email
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															type="text"
															name="statusApproval"
															value={form ? form.statusApproval : ""}
															onChange={handleChange}
															id="statusApproval"
														/>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-6 sm:mb-0">
														Nik
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															type="text"
															name="nik"
															value={form ? form.nik : ""}
															onChange={handleChange}
															id="nik"
														/>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-6 sm:mb-0">
														Nip
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															type="text"
															name="nip"
															value={form ? form.nip : ""}
															onChange={handleChange}
															id="nip"
														/>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-0">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-6 sm:mb-0">
														Kata Sandi Baru
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															type="text"
															name="password"
															value={form ? form.password : ""}
															onChange={handleChange}
															id="password"
														/>
														<FormHelperText>
															*Jika anda ingin mengubah kata sandi, ketik kolom
															kata sandi diatas!
														</FormHelperText>
													</FormControl>
												</div>
											</Grid>
											<Grid item xs={12} sm={6}>
												<div className="flex flex-1 flex-col sm:flex-row mb-8">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">
														Jabatan<span className="text-red">*</span>
													</Typography>
													<FormControl variant="outlined" className="w-full">
														<Select
															native
															value={form ? form.jabatanId : ""}
															onChange={handleChange}
															input={
																<BootstrapSelect
																	name="jabatanId"
																	id="jabatanId"
																/>
															}>
															<option value="0">Pilih Jabatan</option>
															{user.peran.map((item, key) => (
																<option key={key} value={item.jabatanId}>
																	{item.nama}
																</option>
															))}
														</Select>
													</FormControl>
												</div>
												<div className="flex flex-1 flex-col sm:flex-row mb-0">
													<Typography className="w-full sm:max-w-160 text-left sm:text-right sm:mt-6 mr-0 sm:mr-8 mb-4 sm:mb-0">
														Alamat
													</Typography>
													<FormControl className="w-full">
														<BootstrapInput
															className="p-0"
															name="alamat"
															value={form ? form.alamat : ""}
															onChange={handleChange}
															id="alamat"
															multiline
															rows="3"
														/>
													</FormControl>
												</div>
											</Grid>
										</Grid>
									</div>
								</TabPanel>
							</Paper>
							<div className="w-full flex flex-1 justify-start sm:justify-end pt-12">
								<Button
									variant="contained"
									color="secondary"
									size="small"
									className="normal-case rounded-none whitespace-no-wrap"
									disabled={!canBeSubmitted()}
									onClick={() => dispatch(Actions.saveUser(form))}>
									<SaveIcon fontSize="small" className="mr-4" /> Simpan Pengguna
								</Button>
							</div>
						</div>
					</div>
				)
			}
		/>
	);
}

export default withReducer(
	"manajemenPenggunaApp",
	reducer
)(PerManajemenPengguna);
