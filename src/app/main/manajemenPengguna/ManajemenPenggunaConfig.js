/** @format */

import React from "react";
import { authRoles } from "../../auth";

export const ManajemenPenggunaConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.admin,
	routes: [
		{
			path: "/manajemen-pengguna/user/:penggunaId?",
			component: React.lazy(() => import("./PerManajemenPengguna"))
		},
		{
			path: "/manajemen-pengguna",
			component: React.lazy(() => import("./ManajemenPengguna"))
		}
	]
};
