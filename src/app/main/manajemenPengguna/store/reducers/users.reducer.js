/** @format */

import * as Actions from "../actions";

const initialState = {
	data: [],
	count: 0,
	count_all: 0,
	page: 0,
	pages: 0,
	searchText: ""
};

const usersReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_USERS: {
			return {
				...state,
				data: action.payload["hydra:member"],
				count: 30,
				count_all: action.payload["hydra:totalItems"],
				page: action.page,
				pages: Math.floor(action.payload["hydra:totalItems"] / 30)
			};
		}
		case Actions.SET_USERS_SEARCH_TEXT: {
			return {
				...state,
				searchText: action.searchText
			};
		}
		default: {
			return state;
		}
	}
};

export default usersReducer;
