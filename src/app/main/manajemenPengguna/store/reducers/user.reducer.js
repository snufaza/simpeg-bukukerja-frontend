/** @format */

import * as Actions from "../actions";

const initialState = {
	data: null,
	peran: []
};

const userReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_USER: {
			return {
				...state,
				data: action.payload
			};
		}
		case Actions.SAVE_USER: {
			return {
				...state
			};
		}
		case Actions.GET_PERAN: {
			return {
				...state,
				peran: action.payload.data
			};
		}
		default: {
			return state;
		}
	}
};

export default userReducer;
