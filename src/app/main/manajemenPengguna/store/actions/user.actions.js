/** @format */

import axios from "axios";
import { showMessage } from "app/store/actions/fuse";
import history from "@history";

export const GET_USER = "[MANAJEMEN PENGGUNA] GET USER";
export const SAVE_USER = "[MANAJEMEN PENGGUNA] SAVE USER";
export const DELETE_USER = "[MANAJEMEN PENGGUNA] DELETE USER";
export const GET_PERAN = "[MANAJEMEN PENGGUNA] GET PERAN";

export function getUser(params) {
	// console.log("getUser -> params", params);
	const request = axios.get("/api/penggunas/" + params.penggunaId);

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_USER,
				payload: response.data
			})
		);
}

export function saveUser(data) {
	const formData = new FormData();
	// for (var key in data) {
	// 	if (data.hasOwnProperty(key)) {
	// 		formData.set(key, data[key]);
	// 	}
	// }
	// console.log(data);
	// const putData = {
	// 	nik: data.nik,
	// 	nip: data.nip,
	// 	nama: data.nama,
	// 	alamat: data.alamat
	formData.set("nik", data.nik);
	formData.set("nama", data.nama);
	formData.set("tempat_lahir", data.tempatLahir);
	formData.set("tanggal_lahir", data.tanggalLahir);
	formData.set("jenis_kelamin", data.jenisKelamin);
	formData.set("alamat", data.alamat);
	formData.set("telepon", data.telepon);
	formData.set("nip", data.nip);
	// formData.set("jabatan",data.jabatan
	formData.set("email", data.email);

	const request = axios.post(
		"/api/update_pengguna/" + data.penggunaId,
		formData,
		{
			headers: {
				"content-type": "multipart/form-data"
			}
		}
	);

	return dispatch =>
		request.then(response => {
			if (response.data.msg === "username_sudah_digunakan") {
				dispatch(showMessage({ message: "Username Sudah Digunakan." }));
			} else if (response.data.msg === "simpan_berhasil") {
				dispatch(showMessage({ message: "Data Pengguna Telah Ditambahkan." }));
			} else {
				dispatch(showMessage({ message: "Data Pengguna Telah Diubah." }));
				history.push("/manajemen-pengguna/");
			}
			return dispatch({
				type: SAVE_USER
			});
		});
}

export function deleteUsers(params) {
	const request = axios.post("/api/pengguna/delete", { ...params });

	return dispatch =>
		request.then(response => {
			dispatch(showMessage({ message: "Data Pengguna Telah dihapus." }));

			return dispatch({
				type: DELETE_USER,
				payload: response.data
			});
		});
}

export function getPeran() {
	const request = axios.get("api/ref_jabatans");

	return dispatch =>
		request.then(response => {
			return dispatch({
				type: GET_PERAN,
				payload: response.data
			});
		});
}

export function newUser() {
	const data = {
		penggunaId: "",
		nama: "",
		username: "",
		peran: 0,
		keterangan: ""
	};

	return {
		type: GET_USER,
		payload: data
	};
}
