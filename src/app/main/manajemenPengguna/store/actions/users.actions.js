/** @format */

import axios from "axios";

export const GET_USERS = "[MANAJEMEN PENGGUNA] GET USERS";
export const SET_USERS_SEARCH_TEXT =
	"[MANAJEMEN PENGGUNA] SET USERS SEARCH TEXT";

export function getUsers(params) {
	params.page += 1;
	const request = axios.get("/api/penggunas", { params: params });
	// params: routeParams

	return dispatch =>
		request.then(response => {
			const url = new URL(
				"http://www.abc.com" + response.data["hydra:view"]["@id"]
			);
			const page = url.searchParams.get("page");
			console.log("getUsers -> page", page);
			const count = 30;

			return dispatch({
				type: GET_USERS,
				payload: response.data,
				page: page,
				count: count
			});
		});
}

export function setUsersSearchText(event) {
	return {
		type: SET_USERS_SEARCH_TEXT,
		searchText: event.target.value
	};
}
