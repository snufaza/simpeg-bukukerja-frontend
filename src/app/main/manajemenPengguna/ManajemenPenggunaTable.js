/** @format */

import React, { useEffect, useState } from "react";
import { makeStyles, withStyles } from "@material-ui/styles";
import {
	Table,
	TableBody,
	TableCell,
	TablePagination,
	TableRow,
	Dialog,
	DialogTitle,
	DialogContent,
	DialogContentText,
	DialogActions,
	Button,
} from "@material-ui/core";
import { FuseScrollbars } from "@fuse";
import { withRouter } from "react-router-dom";
import _ from "@lodash";
import ManajemenPenggunaTableHead from "./ManajemenPenggunaTableHead";
import * as Actions from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
// import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

const ModalDeleteButton = withStyles(theme => ({
	root: {
		color: "#fff",
		backgroundColor: "#ff4569",
		borderRadius: 0,
		"&:hover": {
			backgroundColor: "#ff1744"
		}
	}
}))(Button);

const useStyles = makeStyles(theme => ({
	modalTitle: {
		color: "#ffffff",
		backgroundColor: "#00a65a",
		padding: "8px 16px"
	},
	modalBody: {
		padding: "24px 16px",
		borderBottom: "1px solid #f5f5f5"
	}
}));

const pageSize = 30;

function ManajemenPenggunaTable(props) {
	const dispatch = useDispatch();

	const classes = useStyles(props);
	const users = useSelector(
		({ manajemenPenggunaApp }) => manajemenPenggunaApp.users.data
	);
	const count_all = useSelector(
		({ manajemenPenggunaApp }) => manajemenPenggunaApp.users.count_all
	);
	const pages = useSelector(
		({ manajemenPenggunaApp }) => manajemenPenggunaApp.users.page
	);
	const searchText = useSelector(
		({ manajemenPenggunaApp }) => manajemenPenggunaApp.users.searchText
	);

	const [loading, setLoading] = useState(true);
	const [open, setOpen] = useState(false);
	const [selected, setSelected] = useState([]);
	const [hapus /*, setHapus*/] = useState(null);
	const [data] = useState(users);
	const [page, setPage] = useState(pages);
	const [rowsPerPage, setRowsPerPage] = useState(pageSize);
	const [order, setOrder] = useState({
		direction: "asc",
		id: null
	});

	useEffect(() => {
		setLoading(true);
		dispatch(
			Actions.getUsers({ page: 0, pageSize: pageSize, nama: searchText })
		).then(res => {
			setLoading(false);
		});
	}, [dispatch, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = "desc";

		if (order.id === property && order.direction === "desc") {
			direction = "asc";
		}

		setOrder({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleClick(item) {
		console.log("handleClick -> item", item);
		props.history.push("/manajemen-pengguna/user/" + item.penggunaId);
	}

	function handleChangePage(event, page) {
		setPage(page);
		dispatch(
			Actions.getUsers({
				page: page,
				pageSize: rowsPerPage,
				nama: searchText
			})
		);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
		dispatch(
			Actions.getUsers({
				page: page,
				pageSize: event.target.value,
				nama: searchText
			})
		);
	}

	// function handleClickOpen() {
	// 	setOpen(true);
	// }

	function handleClose() {
		setOpen(false);
	}

	function hapusPengguna() {
		dispatch(Actions.deleteUsers(hapus)).then(res => {
			dispatch(
				Actions.getUsers({ page: 0, pageSize: pageSize, nama: searchText })
			);
			setPage(0);
			setRowsPerPage(0);
		});
	}

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
				<Table className="w-full" aria-labelledby="tableTitle">
					<ManajemenPenggunaTableHead
						numSelected={selected.length}
						order={order}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={count_all}
					/>
					<TableBody>
						{loading && (
							<TableRow>
								<TableCell colSpan={6} align="center">
									Memuat...
								</TableCell>
							</TableRow>
						)}
						{!loading &&
							_.orderBy(users, [order.penggunaId], [order.direction]).map(
								(n, key) => {
									const isSelected = selected.indexOf(n.penggunaId) !== -1;
									return (
										<TableRow
											className="cursor-pointer"
											hover
											aria-checked={isSelected}
											tabIndex={-1}
											key={key}
											selected={isSelected}
											onClick={event => handleClick(n)}>
											<TableCell
												component="th"
												scope="row"
												className="w-44 min-w-44">
												{page * rowsPerPage + key + 1}
											</TableCell>
											<TableCell
												component="th"
												scope="row"
												className="min-w-160 sm:min-w-full">
												{n.nama}
											</TableCell>
											<TableCell
												component="th"
												scope="row"
												className="w-256 min-w-256">
												{n.email}
											</TableCell>
											<TableCell
												component="th"
												scope="row"
												className="w-128 min-w-128">
												{n.nik}
											</TableCell>
											<TableCell
												component="th"
												scope="row"
												className="w-256 min-w-256">
												{n.nip}
											</TableCell>
											<TableCell
												component="th"
												scope="row"
												className="w-256 min-w-256">
												{n.statusApproval === "1" ? "Sudah" : "Belum"}
											</TableCell>
											{/* <TableCell
												component="th"
												scope="row"
												className="w-32 min-w-32">
												<Tooltip
													aria-label="Hapus Pengguna"
													title="Hapus Pengguna">
													<IconButton
														aria-label="Hapus Pengguna"
														size="small"
														onClick={ev => {
															ev.stopPropagation();
															setHapus({ penggunaId: n.penggunaId });
															dispatch(handleClickOpen);
														}}>
														<DeleteOutlineIcon fontSize="inherit" />
													</IconButton>
												</Tooltip>
											</TableCell> */}
										</TableRow>
									);
								}
							)}
					</TableBody>
				</Table>
			</FuseScrollbars>
			<TablePagination
				component="div"
				count={count_all}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					"aria-label": "Previous Page",
					size: "small"
				}}
				labelDisplayedRows={({ from, to, count }) =>
					`${from}-${to} dari ${count}`
				}
				nextIconButtonProps={{
					"aria-label": "Next Page",
					size: "small"
				}}
				labelRowsPerPage={"Menampilkan Data Per Halaman:"}
				SelectProps={{
					native: true
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="manajemen-pengguna-dialog-title"
				aria-describedby="manajemen-pengguna-dialog-description"
				classes={{ paper: "rounded-none" }}>
				<DialogTitle
					id="manajemen-pengguna-dialog-title"
					className={classes.modalTitle}>
					{"Hapus Data Pengguna"}
				</DialogTitle>
				<DialogContent className={classes.modalBody}>
					<DialogContentText
						id="manajemen-pengguna-dialog-description"
						className="mb-0">
						Apakah anda yakin ingin menghapus data pengguna ini?
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button
						className="rounded-none"
						onClick={handleClose}
						size="small"
						color="primary">
						Batal
					</Button>
					<ModalDeleteButton
						variant="contained"
						onClick={ev => {
							dispatch(handleClose);
							dispatch(hapusPengguna);
						}}
						size="small"
						color="primary"
						autoFocus>
						Hapus
					</ModalDeleteButton>
				</DialogActions>
			</Dialog>
		</div>
	);
}

export default withRouter(ManajemenPenggunaTable);
