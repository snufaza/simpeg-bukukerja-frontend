/** @format */

import React from "react";
import {
	TableHead,
	TableSortLabel,
	TableCell,
	TableRow,
	Tooltip
} from "@material-ui/core";

const rows = [
	{
		id: "id",
		align: "left",
		disablePadding: false,
		label: "No.",
		sort: true
	},
	{
		id: "nama",
		align: "left",
		disablePadding: false,
		label: "Nama Pengguna",
		sort: true
	},
	{
		id: "email",
		align: "left",
		disablePadding: false,
		label: "Email",
		sort: true
	},
	{
		id: "nip",
		align: "left",
		disablePadding: false,
		label: "nip",
		sort: true
	},
	{
		id: "nik",
		align: "left",
		disablePadding: false,
		label: "nik",
		sort: true
	},
	{
		id: "statusApproval",
		align: "left",
		disablePadding: false,
		label: "Konfirmasi email",
		sort: true
	}
	// ,
	// {
	// 	id: "aksi",
	// 	align: "left",
	// 	disablePadding: false,
	// 	label: "Aksi",
	// 	sort: true
	// }
];

function ManajemenPenggunaTableHead(props) {
	const createSortHandler = property => event => {
		props.onRequestSort(event, property);
	};

	return (
		<TableHead>
			<TableRow className="w-48">
				{rows.map(row => {
					return (
						<TableCell
							key={row.id}
							align={row.align}
							padding={row.disablePadding ? "none" : "default"}
							sortDirection={
								props.order.id === row.id ? props.order.direction : false
							}>
							{row.sort && (
								<Tooltip
									title="Sortir"
									placement={
										row.align === "right" ? "bottom-end" : "bottom-start"
									}
									enterDelay={300}>
									<TableSortLabel
										active={props.order.id === row.id}
										direction={props.order.direction}
										onClick={createSortHandler(row.id)}>
										{row.label}
									</TableSortLabel>
								</Tooltip>
							)}
						</TableCell>
					);
				}, this)}
			</TableRow>
		</TableHead>
	);
}

export default ManajemenPenggunaTableHead;
