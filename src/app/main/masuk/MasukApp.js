/** @format */

import React, { useEffect, useRef, useState } from "react";
import {
	Hidden,
	Card,
	CardContent,
	Typography,
	Button,
	InputAdornment,
	Icon
} from "@material-ui/core";
import { FuseAnimate, TextFieldFormsy } from "@fuse";
import Formsy from "formsy-react";
import { Link } from "react-router-dom";
import * as authActions from "app/auth/store/actions";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import ReCAPTCHA from "react-google-recaptcha";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
	root: {
		backgroundImage: "url(assets/images/backgrounds/bg-esign.jpg)",
		backgroundSize: "cover",
		backgroundPosition: "right center",
		color: theme.palette.primary.contrastText
	},
	wrapper: {
		margin: theme.spacing(1),
		position: "relative"
	},
	buttonSuccess: {
		backgroundColor: green[500],
		"&:hover": {
			backgroundColor: green[700]
		}
	},
	buttonProgress: {
		color: green[500],
		position: "absolute",
		top: "50%",
		left: "50%",
		marginTop: -12,
		marginLeft: -12
	}
}));

function MasukApp() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const login = useSelector(({ auth }) => auth.login);

	// const [isFormValid, setIsFormValid] = useState(false);
	const [valueRecaptcha, setValueRecaptcha] = useState(null);
	// const [disableButtonMasuk, setDisableButtonMasuk] = useState(true);
	const formRef = useRef(null);
	// create a variable to store the component instance
	const recaptchaRef = React.createRef();

	useEffect(() => {
		if (login.error && (login.error.email || login.error.password)) {
			formRef.current.updateInputsWithError({
				...login.error
			});
			disableButton();
		}
	}, [login.error]);

	function disableButton() {
		// setDisableButtonMasuk(true);
	}

	function onChangeReCaptcha(value) {
		setValueRecaptcha(value);
		// setDisableButtonMasuk(false);
	}

	function enableButton() {
		// setIsFormValid(true);
		if (valueRecaptcha) {
			// setDisableButtonMasuk(false);
		}
	}

	function handleSubmit(model) {
		model.captcha = valueRecaptcha;
		dispatch(authActions.submitLogin(model));
		recaptchaRef.current.reset();
	}

	const [loading, setLoading] = React.useState(false);
	const [success, setSuccess] = React.useState(false);
	const timer = React.useRef();

	const buttonClassname = clsx({
		[classes.buttonSuccess]: success
	});

	React.useEffect(() => {
		return () => {
			clearTimeout(timer.current);
		};
	}, []);

	function handleButtonClick() {
		if (!loading) {
			setSuccess(false);
			setLoading(true);
			timer.current = setTimeout(() => {
				setSuccess(true);
				setLoading(false);
			}, 2000);
		}
	}

	return (
		<div
			className={clsx(
				classes.root,
				"flex flex-col flex-1 flex-shrink-0 p-24 md:flex-row md:p-0 justify-center"
			)}>
			<Hidden mdDown>
				<div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">
					<FuseAnimate animation="transition.slideUpIn" delay={300}>
						<Typography variant="h3" color="inherit" className="text-white">
							Aplikasi Portal e-Sign{" "}
						</Typography>
					</FuseAnimate>
					<FuseAnimate delay={500}>
						<Typography
							component="p"
							color="inherit"
							className="max-w-640 mt-16 text-justify">
							Aplikasi Portal e-Sign dibuat untuk penerapan tanda tangan
							elektronik berbasis cloud dalam rangka meningkatkan pelayanan
							kepegawaian, integrasi data dan sistem serta mendukung
							terlaksananya e-Goverment di Dinas Pendidikan Pemuda dan Olahraga
							Kabupaten Gunungkidul.
						</Typography>
					</FuseAnimate>
				</div>
			</Hidden>
			<FuseAnimate animation={{ translateX: [0, "100%"] }}>
				<Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>
					<CardContent className="flex flex-col items-center justify-center p-32 md:p-48 ">
						<img
							className="w-88 mb-10"
							src="assets/images/logos/gunungkidul.png"
							alt="Gunungkidul"
						/>

						<Typography variant="h6" className="text-center md:w-full">
							PORTAL E-SIGN
						</Typography>
						<Typography
							variant="subtitle2"
							className="text-center md:w-full mb-48">
							DISDIKPORA KAB. GUNUNGKIDUL
						</Typography>

						<div className="w-full">
							<Formsy
								onValidSubmit={handleSubmit}
								onValid={enableButton}
								onInvalid={disableButton}
								ref={formRef}
								className="flex flex-col justify-center w-full">
								<TextFieldFormsy
									className="mb-16"
									type="text"
									name="email"
									label="Nama Pengguna / Email"
									validations={{
										minLength: 4
									}}
									validationErrors={{
										minLength: "Min character length is 4"
									}}
									InputProps={{
										endAdornment: (
											<InputAdornment position="end">
												<Icon className="text-20" color="action">
													email
												</Icon>
											</InputAdornment>
										)
									}}
									variant="outlined"
									required
								/>
								<TextFieldFormsy
									className="mb-16"
									type="password"
									name="password"
									label="Kata Sandi"
									validations={{
										minLength: 4
									}}
									validationErrors={{
										minLength: "Min character length is 4"
									}}
									InputProps={{
										endAdornment: (
											<InputAdornment position="end">
												<Icon className="text-20" color="action">
													vpn_key
												</Icon>
											</InputAdornment>
										)
									}}
									variant="outlined"
									required
								/>
								<ReCAPTCHA
									className="mb-16"
									sitekey="6Lc9H7EUAAAAAPdswXRZgyasPOZ35fOSLdtpmb4E"
									onChange={onChangeReCaptcha}
									ref={recaptchaRef}
								/>
								<div className={classes.wrapper}>
									<Button
										type="submit"
										variant="contained"
										color="primary"
										className={clsx(
											buttonClassname,
											"w-full mx-auto normal-case"
										)}
										// disabled={disableButtonMasuk}
										onClick={handleButtonClick}>
										Masuk
									</Button>
									{loading && (
										<CircularProgress
											size={24}
											className={classes.buttonProgress}
										/>
									)}
								</div>
							</Formsy>
						</div>

						<div className="flex flex-col items-center justify-center pt-24">
							<span className="font-medium">Belum punya akun?</span>
							<Link className="font-medium" to="/daftar">
								Silakan registrasi disini.
							</Link>
						</div>
					</CardContent>
				</Card>
			</FuseAnimate>

			<div
				className="flex flex-1 items-center"
				style={{ position: "absolute", bottom: 40, left: 40 }}>
				<Typography variant="subtitle2" className="mr-4">
					Didukung oleh :
				</Typography>

				<img
					className="w-128 mr-6"
					src="assets/images/logos/bse-white-logo.png"
					alt="BSE"
				/>

				<img className="w-64" src="assets/images/logos/bssn.png" alt="BSSN" />
			</div>
		</div>
	);
}

export default MasukApp;
