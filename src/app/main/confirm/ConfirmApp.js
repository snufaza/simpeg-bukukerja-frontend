import React, {useEffect, useRef, useState} from 'react';
import {Button, Card, CardContent, Typography, InputAdornment, Icon, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FuseAnimate, TextFieldFormsy} from '@fuse';
import Formsy from 'formsy-react';
import * as authActions from 'app/auth/store/actions';
import clsx from 'clsx';
// import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import ReCAPTCHA from "react-google-recaptcha";
import CircularProgress from '@material-ui/core/CircularProgress';
import {green} from '@material-ui/core/colors';
import history from '@history';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundImage     : 'url(assets/images/backgrounds/bg-esign.jpg)',
        backgroundSize      : 'cover',
        backgroundPosition  : 'center',
        color               : theme.palette.primary.contrastText
    },
    wrapper: {
        position: 'relative'
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700]
        }
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12
    },
}));

function ConfirmApp() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const register = useSelector(({auth}) => auth.register);

    const [isFormValid, setIsFormValid] = useState(false);
    const [valueRecaptcha, setValueRecaptcha] = useState(null);
    const [disableButtonConfirm, setDisableButtonConfirm] = useState(true);
    const formRef = useRef(null);
    const [codeActivasi, setCodeActivasi ] = useState(null);

    useEffect(() => {

        const query = new URLSearchParams(history.location.search);
        const token = query.get('id');
        setCodeActivasi(token);
        if (register.error && (register.error.username || register.error.password || register.error.email)){
         console.log(register.error);
            // formRef.current.updateInputsWithError({
            //     ...register.error
            // });
            disableButton();
        }
    }, [register.error]);

    function disableButton() {
        setIsFormValid(false);
        setDisableButtonConfirm(true);
    }

    function onChangeReCaptcha(value) {
        setValueRecaptcha(value);
        if(isFormValid){
            setDisableButtonConfirm(false);
        }
      }

    function enableButton() {
        setIsFormValid(true);
        if(valueRecaptcha){
            setDisableButtonConfirm(false);
        }
    }

    function handleSubmit(model) {
        dispatch(authActions.submitRegister(model));
    }

    const [loading, setLoading] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const timer = React.useRef();

    const buttonClassname = clsx({
        [classes.buttonSuccess]: success
    });

    React.useEffect(() => {
        return () => {
            clearTimeout(timer.current);
        };
    }, []);

    function handleButtonClick() {
        if (!loading) {
            setSuccess(false);
            setLoading(true);
            timer.current = setTimeout(() => {
                setSuccess(true);
                setLoading(false);
            }, 2000);
        }
    }

    return (
        <div className={clsx(classes.root, "flex flex-col flex-auto flex-shrink-0 items-center justify-center")}>
            <div className="flex flex-col items-center justify-center w-full">
                <FuseAnimate animation="transition.expandIn">
                    <Card className="w-full max-w-xl">
                        <CardContent className="flex flex-col items-center justify-center p-0">
                            <div className="w-full p-32" style={{backgroundColor: "#1d69ff",borderBottom: "3px solid #1358e0"}}>
                                <Grid container spacing={3} justify="center" alignItems="center">
                                    <Grid item xs={2}>
                                        <img className="w-88" src="assets/images/logos/gunungkidul.png" alt="logo" style={{margin: "0 0 0 auto", display: "block"}} />
                                    </Grid>

                                    <Grid item xs={10}>
                                        <Typography variant="h3" className="text-center md:w-full text-white">PORTAL e-SIGN</Typography>

                                        <Typography variant="subtitle2" className="text-center md:w-full text-white">DINAS PENDIDIKAN PEMUDA DAN OLAHRAGA KABUPATEN GUNUNGKIDUL</Typography>
                                    </Grid>
                                </Grid>
                            </div>

                            <div className="w-full p-20">
                                <Formsy
                                    onValidSubmit={handleSubmit}
                                    onValid={enableButton}
                                    onInvalid={disableButton}
                                    ref={formRef}
                                    className="flex flex-col justify-center w-full"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item xs={12} sm={12}>
                                            <div>
                                                <Typography variant="subtitle1" className="mb-24 font-bold" style={{borderBottom: "1px solid rgb(222, 222, 222)"}}>Enter Your Code Activasi</Typography>
                                                <TextFieldFormsy
                                                    type="text"
                                                    name="code"
                                                    label="code"
                                                    InputProps={{
                                                        endAdornment: <InputAdornment position="end"><Icon className="text-20" color="action">vpn_key</Icon></InputAdornment>
                                                    }}
                                                    variant="outlined"
                                                    required
                                                    value={codeActivasi}
                                                    className="w-full mb-20"
                                                />
                                            </div>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                        <ReCAPTCHA
                                            sitekey="6Lc9H7EUAAAAAPdswXRZgyasPOZ35fOSLdtpmb4E"
                                            onChange={onChangeReCaptcha}
                                        />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <div className={classes.wrapper}>
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    className={clsx(buttonClassname, "w-full mx-auto normal-case")}
                                                    disabled={disableButtonConfirm}
                                                    onClick={handleButtonClick}
                                                >
                                                    Activasi Your acount
                                                </Button>
                                                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                                            </div>
                                        </Grid>
                                    </Grid>
                                </Formsy>
                            </div>
                        </CardContent>
                    </Card>
                </FuseAnimate>
            </div>
        </div>
    );
}

export default ConfirmApp;