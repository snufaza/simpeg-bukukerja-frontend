import axios from 'axios';

export const GET_PEGAWAI = '[E-COMMERCE APP] GET PEGAWAI';
export const GET_ORDERS = '[E-COMMERCE APP] GET ORDERS';
export const GET_USER_SIGN_STATUS = '[E-COMMERCE APP] GET USER SIGN STATUS';
export const SET_ORDERS_SEARCH_TEXT = '[E-COMMERCE APP] SET ORDERS SEARCH TEXT';

export function getPegawai() {
    const request = axios.get('/api/pegawais');

    return (dispatch) =>
        request.then((response) => dispatch({
                type   : GET_PEGAWAI,
                payload: response.data['hydra:member'][0]
            })
        );
}

export function setOrdersSearchText(event) {
    return {
        type      : SET_ORDERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function getUserSignStatus(nik) {
    const request = axios.get('/api/cek_user_esign',{
        params :{
            nik:nik
        }
    });

    return (dispatch) =>
        request.then((response) =>{
            return dispatch({
                type   : GET_USER_SIGN_STATUS,
                payload: response.data,
                nik:nik
            });
}
        );
}