import {combineReducers} from 'redux';
import beranda from './beranda.reducer';

const berandaReducer = combineReducers({
    beranda
});

export default berandaReducer;
