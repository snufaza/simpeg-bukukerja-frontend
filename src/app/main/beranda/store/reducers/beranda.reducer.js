import * as Actions from '../actions';

const initialState = {
    data      : {
        "@id":"",
        "@type":"",
        jabatan: "",
        jenisKelamin: "",
        jenjangPendidikan: "",
        karpeg: "",
        lastUpdate: "",
        nama: "",
        nik: null,
        nip: "",
        npwp: null,
        pangkatGolongan: {},
        pegawaiId: "",
        softDelete: 0,
        statusKepegawaian: {},
        tempatLahir: "",
        tempatTugas: "",
        tglLahir: "",
        tmtPangkat: "",
        tmtPengangkatan: null
    },
    searchText: 'abc',
    statusUserBsre:{
        "status_code": 0,
        "status": "",
        "message": "",
        "nik":null
    }
};

const berandaReducer = function (state = initialState, action) {
   
    switch ( action.type )
    {
        case Actions.GET_PEGAWAI:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_ORDERS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        case Actions.GET_USER_SIGN_STATUS:
        {
            return {...state,
                statusUserBsre: 
                { ...action.payload, ...{"nik":action.nik}
                }
            }
        }
        default:
        {
            return state;
        }
    }
};

export default berandaReducer;
