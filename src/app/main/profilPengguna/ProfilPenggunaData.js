/** @format */

import React, { useEffect, useState } from "react";
import {
	makeStyles,
	Typography,
	Grid,
	Paper,
	List,
	ListItem,
	Divider,
	ListItemText,
	Icon
} from "@material-ui/core";
import { FuseUtils, FuseAnimate } from "@fuse";
import { useSelector, useDispatch } from "react-redux";
import Skeleton from "react-loading-skeleton";
import { setDefaultSettings } from "app/store/actions/fuse";
// import ReactTable from "react-table";
// import * as Actions from './store/actions';
import Moment from "moment";
import * as ActionsBer from "../beranda/store/actions";
import axios from "axios";

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1
	},
	paper: {
		padding: 0,
		textAlign: "center",
		color: theme.palette.text.secondary
	},
	listItem: {
		padding: "0 12px"
	},
	success: {
		color: "green",
		marginLeft: 4
	},
	danger: {
		transform: "rotate(45deg)",
		color: "red",
		marginLeft: 4
	},
	card_amd: {
		minHeight: "120px",
		maxHeight: "120px"
	}
}));

function ProfilPenggunaData(props) {
	const classes = useStyles();

	const dispatch = useDispatch();
	// const settings = useSelector(({ ProfilPengguna }) => ProfilPengguna.settings);
	const mpengguna = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.mpengguna.entities
	);
	const searchText = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.mpengguna.searchText
	);
	const user = useSelector(({ ProfilPengguna }) => ProfilPengguna.user);

	const [filteredData, setFilteredData] = useState(null);
	const statusUserBsreReducer = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.berandaReducer.statusUserBsre
	);
	const statusUserBsre = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.berandaReducer.statusUserBsre.message
	);
	const statusUserBsreCode = useSelector(
		({ ProfilPengguna }) =>
			ProfilPengguna.berandaReducer.statusUserBsre.status_code
	);
	useEffect(() => {
		function getFilteredArray(entities, searchText) {
			const arr = Object.keys(entities).map(id => entities[id]);
			if (searchText.length === 0) {
				return arr;
			}
			return FuseUtils.filterArrayByString(arr, searchText);
		}

		if (mpengguna) {
			setFilteredData(getFilteredArray(mpengguna, searchText));
		}
	}, [mpengguna, searchText]);
	useEffect(() => {
		if (user.nik) {
			if (user.nik.length > 3) {
				if (statusUserBsreReducer.nik !== user.nik) {
					dispatch(ActionsBer.getUserSignStatus(user.nik));
				}
			}
		}
	}, [dispatch, user, statusUserBsreReducer]);

	// useEffect(()=>{
	//     console.log('statusUserBsreReducer :', statusUserBsreReducer);
	// });

	useEffect(() => {
		if (statusUserBsreCode === 1111) {
			dispatch(
				setDefaultSettings({
					// ...settings.current,
					layout: {
						config: {
							verifyMessage: {
								display: false,
								action: "Mohon cek kembali ke bsre!",
								variant: "success",
								message:
									"Status anda belum terverifikasinya silakan verifikasi email anda"
							}
						}
					}
				})
			);
		} else if (statusUserBsreCode === 1001) {
			dispatch(
				setDefaultSettings({
					// ...settings.current,
					layout: {
						config: {
							verifyMessage: {
								display: true,
								action: "Silakan cek email anda dan buka email dari BSRE",
								variant: "warning",
								message: "Status anda dalam proses pendaftaran ke BSRE"
							}
						}
					}
				})
			);
		} else if (statusUserBsreCode === 1100) {
			dispatch(
				setDefaultSettings({
					// ...settings.current,
					layout: {
						config: {
							verifyMessage: {
								display: true,
								action: "Silakan Hubungi Dikpora untuk Info lebih lanjut",
								variant: "warning",
								message:
									"User telah memiliki sertifikat dan terdaftar pada RA lain."
							}
						}
					}
				})
			);
		} else {
			dispatch(
				setDefaultSettings({
					// ...settings.current,
					layout: {
						config: {
							verifyMessage: {
								display: true,
								action: "Silakan daftar di Menu Profil Pengguna",
								variant: "warning",
								message: "Status anda belum terdaftar di BSRE"
							}
						}
					}
				})
			);
		}
		// dispatch(setDefaultSettings(statusUserBsreCode));
	}, [dispatch, statusUserBsreCode]);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<div className="flex flex-1 items-center justify-center h-full">
				<Typography color="textSecondary" variant="h5">
					Tidak ada Pengguna
				</Typography>
			</div>
		);
	}

	return (
		<FuseAnimate animation="transition.slideUpIn" delay={300}>
			<Grid container spacing={2}>
				<Grid item xs={12} sm={6}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Nama"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.nama || <Skeleton></Skeleton>}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="NIK"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.nik || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="NIP"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.nip || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Tempat Lahir"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.tempat_lahir || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Tanggal Lahir"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{(user.tanggal_lahir
													? Moment(user.tanggal_lahir).format("D MMMM Y")
													: null) || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12} sm={6}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Jenis Kelamin"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.jenis_kelamin || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Alamat"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.alamat || <Skeleton count={3} />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Jabatan"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.jabatan || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Telepon"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.telepon || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							<Divider component="li" />
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Email"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{user.email || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									primary="Status User BSRE"
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold">
												{statusUserBsre || <Skeleton />}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12} sm={4}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold flex flex-1 items-center">
												Upload KTP{" "}
												{user.image_ktp ? (
													<Icon className={classes.success}>
														check_circle_outline
													</Icon>
												) : (
													<Icon className={classes.danger}>
														add_circle_outline
													</Icon>
												)}
											</Typography>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className={
													"w-full font-bold flex flex-1 items-center " +
													classes.card_amd
												}>
												{user.id ? (
													<img
														src={
															axios.defaults.baseURL +
															"/api/get_image/" +
															user.id +
															":ktp"
														}
														className={
															"w-full block items-center center " +
															classes.card_amd
														}
														alt="ktp"
													/>
												) : (
													<Skeleton />
												)}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12} sm={4}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold flex flex-1 items-center">
												Upload Surat Rekomendasi{" "}
												{user.surat_rekomendasi ? (
													<Icon className={classes.success}>
														check_circle_outline
													</Icon>
												) : (
													<Icon className={classes.danger}>
														add_circle_outline
													</Icon>
												)}
											</Typography>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className={
													"w-full font-bold flex flex-1 items-center " +
													classes.card_amd
												}>
												{user.id ? (
													<a
														href={
															axios.defaults.baseURL +
															"/api/get_image/" +
															user.id +
															":surat"
														}
														target={"_blank"}>
														<img
															src={
																axios.defaults.baseURL +
																"/api/get_image/" +
																user.id +
																":suratrekomendasi"
															}
															className={
																"w-full block items-center center " +
																classes.card_amd
															}
															alt="surat"
														/>{" "}
													</a>
												) : (
													<Skeleton />
												)}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12} sm={4}>
					<Paper className={classes.paper}>
						<List>
							<ListItem className={classes.listItem}>
								<ListItemText
									secondary={
										<React.Fragment>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className="font-bold flex flex-1 items-center">
												Upload Gambar Tanda Tangan{" "}
												{user.image_ttd ? (
													<Icon className={classes.success}>
														check_circle_outline
													</Icon>
												) : (
													<Icon className={classes.danger}>
														add_circle_outline
													</Icon>
												)}
											</Typography>
											<Typography
												component="span"
												variant="subtitle1"
												color="textPrimary"
												className={
													"w-full font-bold flex flex-1 items-center " +
													classes.card_amd
												}>
												{user.id ? (
													<img
														src={
															axios.defaults.baseURL +
															"/api/get_image/" +
															user.id +
															":ttd"
														}
														className={
															"w-full block items-center center " +
															classes.card_amd
														}
														alt="ttd"
													/>
												) : (
													<Skeleton />
												)}
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
						</List>
					</Paper>
				</Grid>
			</Grid>
		</FuseAnimate>
	);
}

export default ProfilPenggunaData;
