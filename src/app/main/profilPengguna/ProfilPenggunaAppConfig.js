/** @format */

import React from "react";
import { Redirect } from "react-router-dom";
import { authRoles } from "../../auth";

export const ProfilPenggunaAppConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.user,
	routes: [
		{
			path: "/profil-pengguna/:id",
			component: React.lazy(() => import("./ProfilPenggunaApp"))
		},
		{
			path: "/profil-pengguna",
			component: () => <Redirect to="/profil-pengguna/all" />
		}
	]
};
