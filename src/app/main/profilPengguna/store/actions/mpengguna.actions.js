import axios from 'axios';
import * as Actions from "./index";
import {hideLoading} from "../../../signPaks/store/actions";
import {showMessage} from "../../../../store/actions/fuse";

export const GET_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] GET MANAJEMEN PENGGUNA';
export const SET_SEARCH_TEXT = '[MANAJEMEN PENGGUNA APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] TOGGLE IN SELECTED MANAJEMEN PENGGUNA';
export const SELECT_ALL_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] SELECT ALL MANAJEMEN PENGGUNA';
export const DESELECT_ALL_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] DESELECT ALL MANAJEMEN PENGGUNA';
export const OPEN_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG = '[MANAJEMEN PENGGUNA APP] OPEN NEW PER MANAJEMEN PENGGUNA DIALOG';
export const CLOSE_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG = '[MANAJEMEN PENGGUNA APP] CLOSE NEW PER MANAJEMEN PENGGUNA DIALOG';
export const OPEN_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG = '[MANAJEMEN PENGGUNA APP] OPEN EDIT PER MANAJEMEN PENGGUNA DIALOG';
export const CLOSE_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG = '[MANAJEMEN PENGGUNA APP] CLOSE EDIT PER MANAJEMEN PENGGUNA DIALOG';
export const ADD_PER_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] ADD PER MANAJEMEN PENGGUNA';
export const UPDATE_PER_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] UPDATE PER MANAJEMEN PENGGUNA';
export const REMOVE_PER_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] REMOVE PER MANAJEMEN PENGGUNA';
export const REMOVE_MANAJEMEN_PENGGUNA = '[MANAJEMEN PENGGUNA APP] REMOVE MANAJEMEN PENGGUNA';

export function getManajemenPengguna(routeParams)
{
    const request = axios.get('/api/manajemen-pengguna-app/manajemen-pengguna', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MANAJEMEN_PENGGUNA,
                payload: response.data,
                routeParams
            })
        );
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedManajemenPengguna(penggunaId)
{
    return {
        type: TOGGLE_IN_SELECTED_MANAJEMEN_PENGGUNA,
        penggunaId
    }
}

export function selectAllManajemenPengguna()
{
    return {
        type: SELECT_ALL_MANAJEMEN_PENGGUNA
    }
}

export function deSelectAllManajemenPengguna()
{
    return {
        type: DESELECT_ALL_MANAJEMEN_PENGGUNA
    }
}

export function openNewPerManajemenPenggunaDialog()
{
    return {
        type: OPEN_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG
    }
}

export function closeNewPerManajemenPenggunaDialog()
{
    return {
        type: CLOSE_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG
    }
}

export function openEditPerManajemenPenggunaDialog(data)
{
    return {
        type: OPEN_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG,
        data
    }
}

export function closeEditPerManajemenPenggunaDialog()
{
    return {
        type: CLOSE_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG
    }
}

export function addPerManajemenPengguna(newPerManajemenPengguna)
{
    return (dispatch, getState) => {

        // const {routeParams} = getState().manajemenPenggunaApp.mpengguna;
        console.log(newPerManajemenPengguna);
        const formData = new FormData();
        for (var key in newPerManajemenPengguna) {
            if (newPerManajemenPengguna.hasOwnProperty(key)) {
                formData.set(key,newPerManajemenPengguna[key]);
            }
        }
        // formData.set(newPerManajemenPengguna);
        // console.log(newPerManajemenPengguna);
        // formData.from(newPerManajemenPengguna)
        // formData.append('image_ttd', newPerManajemenPengguna.image_ttd);
        // formData.append('image_ktp', newPerManajemenPengguna.image_ktp);
        // formData.append('surat_rekomendasi', newPerManajemenPengguna.surat_rekomendasi);
        // if (isNaN(formData.get('image_ttd'))){
        //     formData.delete('image_ttd')
        // }
        // if (isNaN(formData.get('image_ktp'))){
        //     formData.delete('image_ktp')
        // }
        // if (isNaN(formData.get('surat_rekomendasi'))){
        //     formData.delete('surat_rekomendasi')
        // }
        // console.log(formData.get('image_ktp'));
        // console.log(formData);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };

        const request = axios.post('/api/update_pengguna/'+newPerManajemenPengguna['id'], formData,config);

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: ADD_PER_MANAJEMEN_PENGGUNA
                }),dispatch(hideLoading()),dispatch(showMessage({message:response.data.message}))
            ]).then(() => dispatch(Actions.getUserData()))
        );
        // dispatch(Actions.getUserData());
    };
}

export function updatePerManajemenPengguna(pengguna)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().manajemenPenggunaApp.mpengguna;

        const request = axios.post('/api/manajemen-pengguna-app/update-per-manajemen-pengguna', {
            pengguna
        });
        console.log('hideloading');
        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: UPDATE_PER_MANAJEMEN_PENGGUNA
                },dispatch(hideLoading())
                    )
            ]).then(() => dispatch(getManajemenPengguna(routeParams)))
        );
    };
}

export function removePerManajemenPengguna(penggunaId)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().manajemenPenggunaApp.mpengguna;

        const request = axios.post('/api/manajemen-pengguna-app/remove-per-manajemen-pengguna', {
            penggunaId
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_PER_MANAJEMEN_PENGGUNA
                })
            ]).then(() => dispatch(getManajemenPengguna(routeParams)))
        );
    };
}


export function removeManajemenPengguna(penggunaIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().manajemenPenggunaApp.mpengguna;

        const request = axios.post('/api/manajemen-pengguna-app/remove-per-manajemen-pengguna', {
            penggunaIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_MANAJEMEN_PENGGUNA
                }),
                dispatch({
                    type: DESELECT_ALL_MANAJEMEN_PENGGUNA
                })
            ]).then(() => dispatch(getManajemenPengguna(routeParams)))
        );
    };
}