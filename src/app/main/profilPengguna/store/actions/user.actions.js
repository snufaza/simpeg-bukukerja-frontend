import axios from 'axios';

export const GET_USER_DATA = '[MANAJEMEN PENGGUNA APP] GET USER DATA';

export function getUserData()
{
    const request = axios.get('/api/profil_pengguna');

    return (dispatch) =>
        request.then((response) =>{
            return dispatch({
                    type   : GET_USER_DATA,
                    payload: response.data.data
                })
        }

        );
}
