import {combineReducers} from 'redux';
import mpengguna from './mpengguna.reducer';
import user from './user.reducer';
import berandaReducer from "../../../beranda/store/reducers/beranda.reducer";
import signPaks from "../../../signPaks/store/reducers/signPaks.reducer";
import settings from "../../../../store/reducers/fuse/settings.reducer";

const reducer = combineReducers({
    mpengguna,
    user,
    berandaReducer,
    signPaks,
    settings

});

export default reducer;
