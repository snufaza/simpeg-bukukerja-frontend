import * as Actions from '../actions';

const userReducer = function (state = {
    id                          : "",
    nik                         : "",
    nama                        : "",
    tempat_lahir                : "",
    tanggal_lahir               : new Date(),
    jenis_kelamin               : "",
    alamat                      : "",
    telepon                     : "",
    nip                         : "",
    jabatan                     : "",
    email                       : "",
    password                    : "",
    status_approval             : "",
    upload_ktp                  : "",
    upload_surat_rekomendasi    : "",
    upload_gambar_tandatangan   : ""
}, action) {
    switch ( action.type )
    {
        case Actions.GET_USER_DATA:
            return action.payload;
        default:
            return state;
    }
};

export default userReducer;