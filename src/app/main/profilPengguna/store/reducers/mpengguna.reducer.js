import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities                        : null,
    searchText                      : '',
    selectedPerManajemenPenggunaIds : [],
    routeParams                     : {},
    penggunaDialog                  : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    }
};

const manajemenPenggunaReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MANAJEMEN_PENGGUNA:
        {
            return {
                ...state,
                entities   : _.keyBy(action.payload, 'id'),
                routeParams: action.routeParams
            };
        }
        case Actions.SET_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        case Actions.TOGGLE_IN_SELECTED_MANAJEMEN_PENGGUNA:
        {

            const penggunaId = action.penggunaId;

            let selectedPerManajemenPenggunaIds = [...state.selectedPerManajemenPenggunaIds];

            if ( selectedPerManajemenPenggunaIds.find(id => id === penggunaId) !== undefined )
            {
                selectedPerManajemenPenggunaIds = selectedPerManajemenPenggunaIds.filter(id => id !== penggunaId);
            }
            else
            {
                selectedPerManajemenPenggunaIds = [...selectedPerManajemenPenggunaIds, penggunaId];
            }

            return {
                ...state,
                selectedPerManajemenPenggunaIds: selectedPerManajemenPenggunaIds
            };
        }
        case Actions.SELECT_ALL_MANAJEMEN_PENGGUNA:
        {
            const arr = Object.keys(state.entities).map(k => state.entities[k]);

            const selectedPerManajemenPenggunaIds = arr.map(pengguna => pengguna.id);

            return {
                ...state,
                selectedPerManajemenPenggunaIds: selectedPerManajemenPenggunaIds
            };
        }
        case Actions.DESELECT_ALL_MANAJEMEN_PENGGUNA:
        {
            return {
                ...state,
                selectedPerManajemenPenggunaIds: []
            };
        }
        case Actions.OPEN_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG:
        {
            return {
                ...state,
                penggunaDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        case Actions.CLOSE_NEW_PER_MANAJEMEN_PENGGUNA_DIALOG:
        {
            return {
                ...state,
                penggunaDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        case Actions.OPEN_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG:
        {
            return {
                ...state,
                penggunaDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : action.data
                }
            };
        }
        case Actions.CLOSE_EDIT_PER_MANAJEMEN_PENGGUNA_DIALOG:
        {
            return {
                ...state,
                penggunaDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        default:
        {
            return state;
        }
    }
};

export default manajemenPenggunaReducer;
