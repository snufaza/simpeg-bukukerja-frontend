/** @format */

import React, { useEffect } from "react";
// import React, {useEffect, useCallback} from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import {
	// FormControlLabel,
	// RadioGroup,
	Radio,
	TextField,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	Icon,
	IconButton,
	Typography,
	Toolbar,
	AppBar,
	Grid
} from "@material-ui/core";
import { useForm } from "@fuse/hooks";
// import FuseUtils from '@fuse/FuseUtils';
import * as Actions from "./store/actions";
import * as ActionsSign from "../signPaks/store/actions";
import { useDispatch, useSelector } from "react-redux";
//
// const useStyles = makeStyles(theme => ({
//     root: {
//         display: 'flex',
//         flexWrap: 'wrap'
//     },
//     formControl: {
//         marginBottom: 16,
//         minWidth: "100%"
//     },
//     selectEmpty: {
//         marginTop: 12
//     },
// }));

// const defaultFormState = {
//     id                          : "5725a680b3249760ea21de52",
//     nik                         : "84623868638523537356",
//     nama                        : "Dina Sulastri sadsadsa",
//     tempat_lahir                : "Jakarta",
//     tanggal_lahir               : "1992-11-15",
//     jenis_kelamin               : "20",
//     alamat                      : "Jl. Menteng Raya, No. 10 Jakarta Pusat",
//     telepon                     : "081536879967",
//     nip                         : "24514353536454732334",
//     jabatan                     : "PNS",
//     email                       : "dinasulastri@gmail.com",
//     password                    : "dina9872",
//     status_approval             : "30",
//     upload_ktp                  : "",
//     upload_surat_rekomendasi    : "",
//     upload_gambar_tandatangan   : ""
// };

function PerProfilPenggunaDialog(props) {
	// const classes = useStyles();
	const dispatch = useDispatch();
	const penggunaDialog = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.mpengguna.penggunaDialog
	);
	const user = useSelector(({ ProfilPengguna }) => ProfilPengguna.user);

	// const inputLabel = React.useRef(null);
	// const [labelWidth, setLabelWidth] = React.useState(0);
	// const [setLabelWidth] = React.useState(0);
	// React.useEffect(() => {
	//     setLabelWidth();
	// }, []);
	//

	const { form, handleChange, setForm } = useForm(user);

	// console.log('penggunaDialog.props.open :', penggunaDialog.props.open);
	// useEffect(() => {
	//   if (penggunaDialog.props.open) {
	//     setForm(user);
	//   }
	// }, [penggunaDialog.props.open]);
	// useEffect(() => {
	//   if (penggunaDialog.props.open) {
	//     setForm(user);
	//   }
	// }, [penggunaDialog.props.open]);

	useEffect(() => {
		setForm(user);
	}, [user, setForm]);

	function closeComposeDialog() {
		penggunaDialog.type === "edit"
			? dispatch(Actions.closeEditPerManajemenPenggunaDialog())
			: dispatch(Actions.closeNewPerManajemenPenggunaDialog());
	}

	function handleChangeFile(file) {
		if (file.target.name === "upload_ktp") {
			// console.log(1);
			form.image_ktp = file.target.files[0];
		}
		// console.log(file.target.name);
		if (file.target.name === "upload_gambar_tandatangan") {
			// console.log(2);
			form.image_ttd = file.target.files[0];
		}
		if (file.target.name === "upload_surat_rekomendasi") {
			// console.log(3);
			form.surat_rekomendasi = file.target.files[0];
		}
	}

	function canBeSubmitted() {
		return form.nik.length > 0;
	}

	function handleSubmit(event) {
		event.preventDefault();
		dispatch(ActionsSign.showLoading());
		dispatch(Actions.addPerManajemenPengguna(form));

		// console.log('showloading dualig');
		// if ( penggunaDialog.type === 'new' )
		// {
		//     dispatch(Actions.addPerManajemenPengguna(form));
		// }
		// else
		// {
		//     dispatch(Actions.updatePerManajemenPengguna(form));
		// }
		// closeComposeDialog();
	}

	return (
		<Dialog
			classes={{
				paper: "m-24"
			}}
			{...penggunaDialog.props}
			onClose={closeComposeDialog}
			fullWidth
			maxWidth="lg">
			<AppBar position="static" elevation={1}>
				<Toolbar className="flex flex-1 justify-between w-full">
					<Typography variant="subtitle1" color="inherit">
						Ubah Profil Pengguna
					</Typography>
					<IconButton
						color="inherit"
						aria-label="Tutup"
						onClick={closeComposeDialog}>
						<Icon fontSize="small">close</Icon>
					</IconButton>
				</Toolbar>
			</AppBar>
			<form onSubmit={handleSubmit} className="flex flex-col overflow-hidden">
				<DialogContent classes={{ root: "p-24" }}>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<TextField
								className="mb-16"
								autoFocus
								label="Nama"
								id="nama"
								name="nama"
								value={form.nama}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
							/>
							<TextField
								className="mb-16"
								label="NIK"
								id="nik"
								name="nik"
								value={form.nik}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
								disabled={true}
							/>
							<TextField
								className="mb-16"
								label="NIP"
								id="nip"
								name="nip"
								value={form.nip}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
							/>
							<TextField
								className="mb-16"
								label="Tempat Lahir"
								id="tempat_lahir"
								name="tempat_lahir"
								value={form.tempat_lahir}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
							/>
							<TextField
								className="mb-0"
								name="tanggal_lahir"
								id="tanggal_lahir"
								label="Tanggal Lahir"
								type="date"
								value={form.tanggal_lahir}
								// format={'DD/MM/YYYY'}
								onChange={handleChange}
								InputLabelProps={{
									shrink: true
								}}
								variant="outlined"
								fullWidth
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							{/* <TextField
                                    className="mb-16"
                                    label="Jenis Kelamin"
                                    id="jenis-kelamin"
                                    name="jenis_kelamin"
                                    value={form.jenis_kelamin}
                                    onChange={handleChange}
                                    variant="outlined"
                                    disabled
                                    fullWidth
                                    Perempuan' else 'Laki-Laki'
                            /> */}
							<div className="mb-16">
								<div>
									<Radio
										checked={form.jenis_kelamin === "Laki-Laki"}
										onChange={handleChange}
										value="Laki-Laki"
										color="default"
										name="jenis_kelamin"
									/>
									Laki-Laki
								</div>
								<div>
									<Radio
										checked={form.jenis_kelamin === "Perempuan"}
										onChange={handleChange}
										value="Perempuan"
										color="default"
										name="jenis_kelamin"
									/>
									Perempuan
								</div>
							</div>
							<TextField
								className="mb-16"
								label="Alamat"
								id="alamat"
								name="alamat"
								value={form.alamat}
								onChange={handleChange}
								variant="outlined"
								fullWidth
							/>
							<TextField
								className="mb-16"
								label="Jabatan"
								id="jabatan"
								name="jabatan"
								value={form.jabatan}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
								disabled
							/>
							{/*<FormControl */}
							{/*    variant="outlined" */}
							{/*    className={classes.formControl}*/}
							{/*>*/}
							{/*    <InputLabel ref={inputLabel} htmlFor="status-approval">*/}
							{/*        Status*/}
							{/*    </InputLabel>*/}
							{/*    <Select*/}
							{/*        value={form.status_approval}*/}
							{/*        onChange={handleChange}*/}
							{/*        input={<OutlinedInput labelWidth={labelWidth} name="status_approval" id="status-approval" />}*/}
							{/*    >*/}
							{/*        <MenuItem value="">*/}
							{/*            <em>Pilih Salah Satu</em>*/}
							{/*        </MenuItem>*/}
							{/*        <MenuItem value={30}>Disetujui</MenuItem>*/}
							{/*        <MenuItem value={40}>Tidak Disetujui</MenuItem>*/}
							{/*    </Select>*/}
							{/*</FormControl>*/}
							<TextField
								className="mb-16"
								label="Telepon"
								id="telepon"
								name="telepon"
								value={form.telepon}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
							/>
							<TextField
								className="mb-16"
								label="Email"
								id="email"
								name="email"
								value={form.email}
								onChange={handleChange}
								variant="outlined"
								required
								fullWidth
								disabled
							/>
						</Grid>

						{/*<Grid item xs={12} sm={6}>*/}
						{/*    <TextField*/}
						{/*        label="Kata Sandi"*/}
						{/*        id="password"*/}
						{/*        name="password"*/}
						{/*        value={form.password}*/}
						{/*        type="password"*/}
						{/*        onChange={handleChange}*/}
						{/*        variant="outlined"*/}
						{/*        required*/}
						{/*        fullWidth*/}
						{/*    />*/}
						{/*</Grid>*/}
						<Grid item xs={12} sm={4}>
							<TextField
								id="upload_ktp"
								name="upload_ktp"
								label="Upload KTP"
								type="file"
								value={form.upload_ktp}
								onChange={handleChangeFile}
								InputLabelProps={{
									shrink: true
								}}
								variant="outlined"
								fullWidth
								helperText="*Upload hanya format JPG / PNG"
							/>
						</Grid>
						<Grid item xs={12} sm={4}>
							<TextField
								id="upload_surat_rekomendasi"
								name="upload_surat_rekomendasi"
								label="Upload Surat Rekomendasi"
								type="file"
								value={form.upload_surat_rekomendasi}
								onChange={handleChangeFile}
								InputLabelProps={{
									shrink: true
								}}
								variant="outlined"
								fullWidth
								helperText="*Upload hanya format PDF"
							/>
						</Grid>
						<Grid item xs={12} sm={4}>
							<TextField
								id="upload_gambar_tandatangan"
								name="upload_gambar_tandatangan"
								label="Upload Gambar Tanda Tangan"
								type="file"
								value={form.upload_gambar_tandatangan}
								onChange={handleChangeFile}
								InputLabelProps={{
									shrink: true
								}}
								variant="outlined"
								fullWidth
								helperText="*Upload hanya format JPG / PNG"
							/>
						</Grid>
					</Grid>
				</DialogContent>

				<DialogActions className="justify-start">
					<Button
						variant="contained"
						color="primary"
						type="submit"
						onClick={handleSubmit}
						disabled={!canBeSubmitted()}>
						Simpan
					</Button>
				</DialogActions>
			</form>
		</Dialog>
	);
}

export default PerProfilPenggunaDialog;
