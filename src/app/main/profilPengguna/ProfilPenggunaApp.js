/** @format */

import React, { useEffect, useRef } from "react";
import { Icon, Typography, Tooltip, Fab } from "@material-ui/core";
import { FusePageSimple, FuseAnimate } from "@fuse";
import { useDispatch, useSelector } from "react-redux";
import withReducer from "app/store/withReducer";
import ProfilPenggunaData from "./ProfilPenggunaData";
import PerProfilPenggunaDialog from "./PerProfilPenggunaDialog";
import * as Actions from "./store/actions";
import * as ActionsReq from "../signPaks/store/actions";
// import * as ActionsBer from '../beranda/store/actions';
import * as ActionsPusat from "../../store/actions";

import reducer from "./store/reducers";
import ReqBsreSignDialog from "../signPaks/ReqBsreSignDialog";
import SignLoadingDialog from "../component/SignLoadingDialog";

function ProfilPenggunaApp(props) {
	const loading = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.signPaks.loading
	);
	const dispatch = useDispatch();
	const pageLayout = useRef(null);
	const statusUserBsre = useSelector(
		({ ProfilPengguna }) => ProfilPengguna.berandaReducer.statusUserBsre
	);

	useEffect(() => {
		dispatch(Actions.getManajemenPengguna(props.match.params));
		dispatch(Actions.getUserData());
	}, [dispatch, props.match.params]);

	useEffect(() => {
		dispatch(Actions.getManajemenPengguna(props.match.params));
	}, [dispatch, props.match.params]);

	function openSignDialog() {
		if (statusUserBsre.status_code === 1111) {
			dispatch(
				ActionsPusat.showMessage({ message: "Anda telah terdaftar di BSRE" })
			);
		} else if (statusUserBsre.status_code === 0) {
			dispatch(
				ActionsPusat.showMessage({
					message:
						"Dalam proses pengecekan ke server bsre mohon coba beberapa saat lagi"
				})
			);
		} else {
			dispatch(ActionsReq.openNewReqSignDialog());
		}
	}
	return (
		<React.Fragment>
			<SignLoadingDialog open={loading} />
			<FusePageSimple
				classes={{
					contentWrapper: "p-12 h-full",
					content: "flex flex-col h-full",
					header: "min-h-72 h-72"
				}}
				header={
					<div className="flex flex-1 items-center justify-between p-12">
						<div className="flex flex-shrink items-center sm:w-224">
							<div className="flex items-center">
								<FuseAnimate animation="transition.slideLeftIn" delay={300}>
									<Typography variant="h6" className="hidden sm:flex">
										Profil Pengguna
									</Typography>
								</FuseAnimate>
							</div>
						</div>

						<div className="flex flex-1 justify-end">
							<FuseAnimate animation="transition.slideLeftIn" delay={300}>
								<Tooltip title="Ubah Profil" aria-label="Ubah Profil">
									<Fab
										size="small"
										color="secondary"
										aria-label="Ubah Profil"
										onClick={ev =>
											dispatch(Actions.openNewPerManajemenPenggunaDialog())
										}>
										<Icon>edit</Icon>
									</Fab>
								</Tooltip>
							</FuseAnimate>
							<FuseAnimate animation="transition.slideLeftIn" delay={300}>
								<Tooltip
									title="Ajukan Akun Bsre"
									aria-label="Daftar TTDE digital">
									<Fab
										size="small"
										color="secondary"
										aria-label="Assignment"
										onClick={ev => openSignDialog()}>
										<Icon>assignment</Icon>
									</Fab>
								</Tooltip>
							</FuseAnimate>
						</div>
					</div>
				}
				content={
					<div>
						<ProfilPenggunaData />
					</div>
				}
				sidebarInner
				ref={pageLayout}
			/>
			<PerProfilPenggunaDialog />
			<ReqBsreSignDialog />
		</React.Fragment>
	);
}

export default withReducer("ProfilPengguna", reducer)(ProfilPenggunaApp);
