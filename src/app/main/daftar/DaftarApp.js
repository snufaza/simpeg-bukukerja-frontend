/** @format */

import React, { useEffect, useRef, useState } from "react";
import {
	Button,
	Card,
	CardContent,
	Typography,
	InputAdornment,
	Icon,
	Grid
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { FuseAnimate, TextFieldFormsy } from "@fuse";
import Formsy from "formsy-react";
import * as authActions from "app/auth/store/actions";
import clsx from "clsx";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import ReCAPTCHA from "react-google-recaptcha";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import Select from "react-select";
// import {useForm} from "../../../@fuse/hooks";
import * as Actions from "../signPaks/store/actions";
import withReducer from "../../store/withReducer";
import reducer from "../signPaks/store/reducers";
// import SelectSearch from 'react-select-search';

const useStyles = makeStyles(theme => ({
	root: {
		backgroundImage: "url(assets/images/backgrounds/bg-esign.jpg)",
		backgroundSize: "cover",
		backgroundPosition: "center",
		color: theme.palette.primary.contrastText
	},
	wrapper: {
		position: "relative"
	},
	buttonSuccess: {
		backgroundColor: green[500],
		"&:hover": {
			backgroundColor: green[700]
		}
	},
	buttonProgress: {
		color: green[500],
		position: "absolute",
		top: "50%",
		left: "50%",
		marginTop: -12,
		marginLeft: -12
	}
}));

function DaftarApp() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const register = useSelector(({ auth }) => auth.register);
	// const {form, handleChange, setForm} = useForm(null);
	// const signPakLoaded = useSelector(({ signPakApp }) => signPakApp.signPaks.sign_pak_loaded);
	const opt_instansi = useSelector(
		({ signPakApp }) => signPakApp.signPaks.opt_instansi
	);
	const opt_instansi_sel = useSelector(
		({ signPakApp }) => signPakApp.signPaks.opt_instansi_sel
	);
	const opt_jabatan = useSelector(
		({ signPakApp }) => signPakApp.signPaks.opt_jabatan
	);
	const opt_jabatan_sel = useSelector(
		({ signPakApp }) => signPakApp.signPaks.opt_jabatan_sel
	);
	// /**
	//  * The options array should contain objects.
	//  * Required keys are "name" and "value" but you can have and use any number of key/value pairs.
	//  */
	// const options = [
	//     {name: 'Swedish', value: 'sv'},
	//     {name: 'English', value: 'en'},
	//     {
	//         type: 'group',
	//         name: 'Group name',
	//         items: [
	//             {name: 'Spanish', value: 'es'},
	//         ]
	//     },
	// ];

	const [isFormValid, setIsFormValid] = useState(false);
	const [valueRecaptcha, setValueRecaptcha] = useState(null);
	// const [disableButtonDaftar, setDisableButtonDaftar] = useState(true);
	const formRef = useRef(null);
	const recaptchaRef = React.createRef();

	function handleChangeOpt(val) {
		if (val.length > 3) {
			dispatch(Actions.getInstansis({ nama: val }));
		}
	}
	function handleChangeOptJabatan(val) {
		if (val.length > 3) {
			dispatch(Actions.getJabatans({ nama: val }));
		}
	}

	function onSetChange(val) {
		dispatch(Actions.setInstansi(val));
	}

	function onSetChangeJabatan(val) {
		dispatch(Actions.setJabatan(val));
	}

	useEffect(() => {
		dispatch(Actions.getInstansis());
		dispatch(Actions.getJabatans());
	}, [dispatch, opt_instansi_sel]);
	useEffect(() => {
		if (
			register.error &&
			(register.error.username ||
				register.error.password ||
				register.error.email)
		) {
			console.log(register.error);
			// formRef.current.updateInputsWithError({
			//     ...register.error
			// });
			disableButton();
		}
	}, [register.error]);

	function disableButton() {
		setIsFormValid(false);
		// setDisableButtonDaftar(true);
	}

	function onChangeReCaptcha(value) {
		setValueRecaptcha(value);
		if (isFormValid) {
			// setDisableButtonDaftar(false);
		}
	}

	function enableButton() {
		setIsFormValid(true);
		// if(valueRecaptcha){
		// setDisableButtonDaftar(false);
		// }
	}

	function handleSubmit(model) {
		recaptchaRef.current.reset();
		model.valueRecaptcha = valueRecaptcha;
		model.email = model.email + "@gunungkidulkab.go.id";
		model.jabatan_id = opt_jabatan_sel;
		model.instansi_id = opt_instansi_sel;
		dispatch(authActions.submitRegister(model));
	}

	const [loading, setLoading] = React.useState(false);
	const [success, setSuccess] = React.useState(false);
	const timer = React.useRef();

	const buttonClassname = clsx({
		[classes.buttonSuccess]: success
	});

	React.useEffect(() => {
		return () => {
			clearTimeout(timer.current);
		};
	}, []);

	function handleButtonClick() {
		if (!loading) {
			setSuccess(false);
			setLoading(true);
			timer.current = setTimeout(() => {
				setSuccess(true);
				setLoading(false);
			}, 2000);
		}
	}

	return (
		<div
			className={clsx(
				classes.root,
				"flex flex-col flex-auto flex-shrink-0 items-center justify-center"
			)}>
			<div className="flex flex-col items-center justify-center w-full">
				<FuseAnimate animation="transition.expandIn">
					<Card className="w-full max-w-xl">
						<CardContent className="flex flex-col items-center justify-center p-0">
							<div
								className="w-full p-32"
								style={{
									backgroundColor: "#1d69ff",
									borderBottom: "3px solid #1358e0"
								}}>
								<Grid
									container
									spacing={3}
									justify="center"
									alignItems="center">
									<Grid item xs={2}>
										<img
											className="w-88"
											src="assets/images/logos/gunungkidul.png"
											alt="logo"
											style={{ margin: "0 0 0 auto", display: "block" }}
										/>
									</Grid>

									<Grid item xs={10}>
										<Typography
											variant="h3"
											className="text-center md:w-full text-white">
											PORTAL e-SIGN
										</Typography>

										<Typography
											variant="subtitle2"
											className="text-center md:w-full text-white">
											DINAS PENDIDIKAN PEMUDA DAN OLAHRAGA KABUPATEN GUNUNGKIDUL
										</Typography>
									</Grid>
								</Grid>
							</div>

							<div className="w-full p-20">
								<Formsy
									onValidSubmit={handleSubmit}
									onValid={enableButton}
									onInvalid={disableButton}
									ref={formRef}
									className="flex flex-col justify-center w-full">
									<Grid container spacing={3}>
										<Grid item xs={12} sm={6}>
											<div>
												<Typography
													variant="subtitle1"
													className="mb-24 font-bold"
													style={{
														borderBottom: "1px solid rgb(222, 222, 222)"
													}}>
													Biodata Pengguna
												</Typography>
												<TextFieldFormsy
													type="text"
													name="nama"
													label="Nama"
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																<Icon className="text-20" color="action">
																	person
																</Icon>
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<TextFieldFormsy
													type="number"
													name="nik"
													label="NIK"
													validations={{
														maxLength: 16
													}}
													validationErrors={{
														maxLength: "Maksimum Karakter 16"
													}}
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																<Icon className="text-20" color="action">
																	credit_card
																</Icon>
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<TextFieldFormsy
													type="number"
													name="nip"
													label="NIP"
													validations={{
														maxLength: 18
													}}
													validationErrors={{
														maxLength: "Min character length is 18"
													}}
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																<Icon className="text-20" color="action">
																	credit_card
																</Icon>
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<Select
													id="instansi_id"
													name="instansi_id"
													placeholder="Instansi"
													label="Instansi"
													onInputChange={handleChangeOpt}
													onChange={onSetChange}
													value={opt_instansi_sel}
													options={opt_instansi}
												/>

												{/*<SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" />*/}

												{/*value={form.optKoleksi}*/}
												{/*options={objects.cmb_koleksi}*/}
												{/*isLoading={loading0}*/}

												{/*<SelectFormsy*/}
												{/*    name="instansi_id"*/}
												{/*    label="Pilih Unit Kerja"*/}
												{/*    value="none"*/}
												{/*    variant="outlined"*/}
												{/*    className="w-full"*/}
												{/*>*/}
												{/*    <MenuItem value="none">*/}
												{/*        <em>Pilih Unit Kerja</em>*/}
												{/*    </MenuItem>*/}
												{/*    <MenuItem value="unit1">Unit 1</MenuItem>*/}
												{/*    <MenuItem value="unit2">Unit 2</MenuItem>*/}
												{/*</SelectFormsy>*/}
											</div>
										</Grid>
										<Grid item xs={12} sm={6}>
											<div>
												<Typography
													variant="subtitle1"
													className="mb-24 font-bold"
													style={{
														borderBottom: "1px solid rgb(222, 222, 222)"
													}}>
													Akun Pengguna
												</Typography>
												<TextFieldFormsy
													type="text"
													name="email"
													label="Email"
													// validations="isEmail"
													// validationErrors={{
													//     isEmail: 'Mohon masukkan email dengan benar'
													// }}
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																@gunungkidulkab.go.id
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<TextFieldFormsy
													type="password"
													name="password"
													label="Kata Sandi"
													// validations="equalsField:password-confirm"
													// validationErrors={{
													//     equalsField: 'Kata sandi tidak cocok'
													// }}
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																<Icon className="text-20" color="action">
																	vpn_key
																</Icon>
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<TextFieldFormsy
													type="password"
													name="password_confirm"
													label="Masukan Ulang Kata Sandi"
													// validations="equalsField:password"
													// validationErrors={{
													//     equalsField: 'Kata sandi tidak cocok'
													// }}
													InputProps={{
														endAdornment: (
															<InputAdornment position="end">
																<Icon className="text-20" color="action">
																	vpn_key
																</Icon>
															</InputAdornment>
														)
													}}
													variant="outlined"
													required
													className="w-full mb-20"
												/>
												<Select
													id="jabatan_id"
													name="jabatan_id"
													placeholder="Jabatan"
													label="Jabatan"
													onInputChange={handleChangeOptJabatan}
													onChange={onSetChangeJabatan}
													value={opt_jabatan_sel}
													options={opt_jabatan}
												/>
											</div>
										</Grid>
										<Grid item xs={12} sm={6}>
											<ReCAPTCHA
												sitekey="6Lc9H7EUAAAAAPdswXRZgyasPOZ35fOSLdtpmb4E"
												onChange={onChangeReCaptcha}
												ref={recaptchaRef}
											/>
										</Grid>
										<Grid item xs={12} sm={6}>
											<div className={classes.wrapper}>
												<Button
													type="submit"
													variant="contained"
													color="primary"
													className={clsx(
														buttonClassname,
														"w-full mx-auto normal-case"
													)}
													// disabled={disableButtonDaftar}
													onClick={handleButtonClick}>
													Daftar Pengguna Baru
												</Button>
												{loading && (
													<CircularProgress
														size={24}
														className={classes.buttonProgress}
													/>
												)}
											</div>
											<Button
												component={Link}
												to="/login"
												color="primary"
												className="w-full mx-auto normal-case"
												aria-label="Kembali ke Login"
												value="legacy">
												Kembali ke Login
											</Button>
										</Grid>
									</Grid>
								</Formsy>
							</div>
						</CardContent>
					</Card>
				</FuseAnimate>
			</div>
		</div>
	);
}

export default withReducer("signPakApp", reducer)(DaftarApp);
