/** @format */

import React from "react";
import { Redirect } from "react-router-dom";
import { FuseUtils } from "@fuse";
import { BerandaConfig } from "app/main/beranda/BerandaConfig";
import { MasukAppConfig } from "app/main/masuk/MasukAppConfig";
import { DaftarAppConfig } from "app/main/daftar/DaftarAppConfig";
import { ConfirmAppConfig } from "app/main/confirm/ConfirmAppConfig";
import { SignPaksAppConfig } from "app/main/signPaks/SignPaksAppConfig";
import { ManajemenPenggunaConfig } from "app/main/manajemenPengguna/ManajemenPenggunaConfig";
import { ProfilPenggunaAppConfig } from "app/main/profilPengguna/ProfilPenggunaAppConfig";
// import { dataReferensiConfigs } from "app/main/dataReferensi/dataReferensiConfigs";
import { /*logConfig,*/ LogConfig } from "app/main/dataReferensi/log/LogConfig";

const routeConfigs = [
	BerandaConfig,
	MasukAppConfig,
	DaftarAppConfig,
	ConfirmAppConfig,
	SignPaksAppConfig,
	ManajemenPenggunaConfig,
	ProfilPenggunaAppConfig,
	LogConfig
	// ,
	// dataReferensiConfigs
];

const routes = [
	...FuseUtils.generateRoutesFromConfigs(routeConfigs, null),
	{
		path: "/",
		component: () => <Redirect to="/sign-pak/diajukan" />
	}
];

export default routes;
