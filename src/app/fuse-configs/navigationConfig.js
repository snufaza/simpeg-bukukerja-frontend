/** @format */

import { authRoles } from "app/auth";

const navigationConfig = [
	{
		id: "profil-pengguna",
		title: "Profil Pengguna",
		type: "item",
		icon: "person",
		url: "/profil-pengguna",
		auth: authRoles.user
	},
	{
		id: "group-admin",
		type: "group",
		icon: "insert_chart",
		title: "Admin Page",
		auth: authRoles.admin,
		children: [
			{
				id: "manajemen-pengguna",
				title: "Manajemen Pengguna",
				type: "item",
				icon: "group",
				url: "/manajemen-pengguna",
				auth: authRoles.admin
			},
			{
				id: "data-log",
				title: "Data Log",
				type: "item",
				icon: "group",
				url: "/data-referensi/log",
				auth: authRoles.admin
			}
		]
	},
	{
		id: "group-sign-digital",
		type: "group",
		icon: "insert_chart",
		title: "Tanda Tangan Digital",
		children: [
			{
				id: "rkas-digital-sign",
				title: "RKAS",
				type: "item",
				icon: "insert_chart",
				url: "/sign-pak/rkas/diajukan",
				auth: authRoles.user
			},
			{
				id: "eds-digital-sign",
				title: "EDS",
				type: "item",
				icon: "insert_chart",
				url: "/sign-pak/eds/diajukan",
				auth: authRoles.user
			},
			{
				id: "simpeg-digital-sign",
				title: "Simpeg - PAK",
				type: "item",
				icon: "insert_chart",
				url: "/sign-pak/simpeg-pak/diajukan",
				auth: authRoles.user
			}
		]
	}
];

export default navigationConfig;
