import React from 'react';
import {Typography} from '@material-ui/core';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/styles';
// import { useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import reducer from "../../main/signPaks/store/reducers";

const useStyles = makeStyles(theme => ({
    root      : {
        '& .logo-icon'                : {
            width     : 28,
            height    : "auto",
            transition: theme.transitions.create(['width', 'height'], {
                duration: theme.transitions.duration.shortest,
                easing  : theme.transitions.easing.easeInOut
            })
        },
        '& .react-badge, & .logo-text': {
            transition: theme.transitions.create('opacity', {
                duration: theme.transitions.duration.shortest,
                easing  : theme.transitions.easing.easeInOut
            })
        }
    },
    reactBadge: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        color          : '#61DAFB'
    }
}));

function Logo() {
    // const user = useSelector(({ auth }) => auth.user);
    const classes = useStyles();
    // const judulApp = useSelector(({ signPakApp }) => signPakApp.signPaks.judulApp);
    
    return (
        <div className={clsx(classes.root, "flex items-center")}>
            <img className="logo-icon" src="assets/images/logos/bse.png" alt="logo" />
            <Typography className="text-16 ml-12 font-light logo-text"><strong>PORTAL e-SIGN</strong></Typography>
        </div>
    );
}

export default withReducer('signPakApp', reducer)(Logo);