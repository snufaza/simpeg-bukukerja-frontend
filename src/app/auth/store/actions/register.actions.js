import firebaseService from 'app/services/firebaseService';
import * as UserActions from './user.actions';
import * as Actions from 'app/store/actions';
import jwtService from 'app/services/jwtService';
// import axios from "axios";

export const REGISTER_ERROR = 'REGISTER_ERROR';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';

export function submitRegister({nama, nik, nip, unit_kerja,email, password,password_confirm, jabatan_id, valueRecaptcha,instansi_id})
{

    return (dispatch) =>
        jwtService.createUser({
            nama,
            nik,
            nip,
            unit_kerja,
            email,
            password,
            password_confirm,
            jabatan_id,
            instansi_id,
            valueRecaptcha
        })
            .then((user) => {

                    dispatch(UserActions.setUserData(user));
                    return dispatch({
                        type: REGISTER_SUCCESS
                    });
                }
            )
            .catch(error => {
                dispatch(Actions.showMessage({message: error.message}));
                return dispatch({
                    type   : REGISTER_ERROR,
                    payload: error
                });
            });
}

export function registerWithFirebase(model)
{
    const {email, password, displayName} = model;
    return (dispatch) =>
        firebaseService.auth && firebaseService.auth.createUserWithEmailAndPassword(email, password)
            .then(response => {

                dispatch(UserActions.createUserSettingsFirebase({
                    ...response.user,
                    displayName,
                    email
                }));

                return dispatch({
                    type: REGISTER_SUCCESS
                });
            })
            .catch(error => {
                const usernameErrorCodes = [
                    'auth/operation-not-allowed',
                    'auth/user-not-found',
                    'auth/user-disabled'
                ];

                const emailErrorCodes = [
                    'auth/email-already-in-use',
                    'auth/invalid-email'
                ];

                const passwordErrorCodes = [
                    'auth/weak-password',
                    'auth/wrong-password'
                ];

                const response = {
                    email      : emailErrorCodes.includes(error.code) ? error.message : null,
                    displayName: usernameErrorCodes.includes(error.code) ? error.message : null,
                    password   : passwordErrorCodes.includes(error.code) ? error.message : null
                };

                if ( error.code === 'auth/invalid-api-key' )
                {
                    dispatch(Actions.showMessage({message: error.message}));
                }

                return dispatch({
                    type   : REGISTER_ERROR,
                    payload: response
                });
            });
}


export function submitRegisterBsre({displayName, password, email})
{
    return (dispatch) =>
        jwtService.createUser({
            displayName,
            password,
            email
        })
            .then((user) => {
                    dispatch(UserActions.setUserData(user));
                    return dispatch({
                        type: REGISTER_SUCCESS
                    });
                }
            )
            .catch(error => {
                console.log(error);
                dispatch(Actions.showMessage('a1'));
                return dispatch({
                    type   : REGISTER_ERROR,
                    payload: error
                });
            });

    // let data = {};
        // return new Promise((resolve, reject) => {
        //     axios.post('/api/auth/register', {})
        //         .then(response => {
        //             console.log(response);
        //             if ( response.data.user )
        //             {
        //                 this.setSession(response.data.access_token);
        //                 resolve(response.data.user);
        //             }
        //             else
        //             {
        //                 console.log(response);
        //                 Actions.showMessage('a1');
        //                 reject(response.data.error);
        //             }
        //         });
        // });
}