/**
 * Authorization Roles
 *
 * @format
 */

const authRoles = {
	admin: ["admin"],
	staff: ["admin", "staff"],
	user: ["admin", "staff", "user", "pejabat", "ptk", "penandatangan"],
	nonpns: ["admin", "staff", "user", "ptk"],
	penandatangan: ["admin", "pejabat"],
	onlyGuest: []
};

export default authRoles;
