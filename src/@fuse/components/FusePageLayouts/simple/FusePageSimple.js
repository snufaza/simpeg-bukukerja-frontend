import React, {useRef} from 'react';
import {makeStyles} from '@material-ui/styles';
import {FuseScrollbars} from '@fuse';
import clsx from 'clsx';
import {useSelector} from 'react-redux';
import FusePageSimpleSidebar from './FusePageSimpleSidebar';
import FusePageSimpleHeader from './FusePageSimpleHeader';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import EmailIcon from '@material-ui/icons/Email';
import {amber, green} from '@material-ui/core/colors';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';

const headerHeight = 120;
const toolbarHeight = 64;
const drawerWidth = 240;

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon
};

const useStyles = makeStyles(theme => ({
    root                     : {
        display        : 'flex',
        flexDirection  : 'column',
        minHeight      : '100%',
        position       : 'relative',
        flex           : '1 0 auto',
        height         : 'auto',
        backgroundColor: theme.palette.background.default
    },
    innerScroll              : {
        flex  : '1 1 auto',
        height: '100%'
    },
    wrapper                  : {
        display        : 'flex',
        flexDirection  : 'row',
        flex           : '1 1 auto',
        zIndex         : 2,
        maxWidth       : '100%',
        minWidth       : 0,
        height         : '100%',
        backgroundColor: theme.palette.background.default
    },
    header                   : {
        height         : headerHeight,
        minHeight      : headerHeight,
        display        : 'flex',
        background     : 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + theme.palette.primary.main + ' 100%)',
        color          : theme.palette.primary.contrastText,
        backgroundSize : 'cover',
        backgroundColor: theme.palette.primary.dark
    },
    topBg                    : {
        position     : 'absolute',
        left         : 0,
        right        : 0,
        top          : 0,
        height       : headerHeight,
        pointerEvents: 'none'
    },
    contentWrapper           : {
        display                     : 'flex',
        flexDirection               : 'column',
        flex                        : '1 1 auto',
        overflow                    : 'auto',
        '-webkit-overflow-scrolling': 'touch',
        zIndex                      : 9999
    },
    toolbar                  : {
        height    : toolbarHeight,
        minHeight : toolbarHeight,
        display   : 'flex',
        alignItems: 'center'
    },
    content                  : {
        flex: '1 0 auto'
    },
    sidebarWrapper           : {
        overflow       : 'hidden',
        backgroundColor: 'transparent',
        position       : 'absolute',
        '&.permanent'  : {
            [theme.breakpoints.up('lg')]: {
                position: 'relative'
            }
        }
    },
    sidebar                  : {
        position     : 'absolute',
        '&.permanent': {
            [theme.breakpoints.up('lg')]: {
                backgroundColor: theme.palette.background.default,
                color          : theme.palette.text.primary,
                position       : 'relative'
            }
        },
        width        : drawerWidth,
        height       : '100%'
    },
    leftSidebar              : {
        [theme.breakpoints.up('lg')]: {
            borderRight: '1px solid ' + theme.palette.divider,
            borderLeft : 0
        }
    },
    rightSidebar             : {
        [theme.breakpoints.up('lg')]: {
            borderLeft : '1px solid ' + theme.palette.divider,
            borderRight: 0
        }
    },
    sidebarHeader            : {
        height         : headerHeight,
        minHeight      : headerHeight,
        backgroundColor: theme.palette.primary.dark,
        color          : theme.palette.primary.contrastText
    },
    sidebarHeaderInnerSidebar: {
        backgroundColor: 'transparent',
        color          : 'inherit',
        height         : 'auto',
        minHeight      : 'auto'
    },
    sidebarContent           : {},
    backdrop                 : {
        position: 'absolute'
    },
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
        marginRight: 4
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center'
    }
}));

function MySnackbarContentWrapper(props) {
    const classes = useStyles();
    const {className, message, variant, action} = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            style={{borderRadius: 0}}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={
                <Button variant="contained" size="small" color="secondary" className="normal-case">
                    <EmailIcon className={classes.icon} /> {action}
                </Button>
            }
        />
    );
}

MySnackbarContentWrapper.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired
};

const FusePageSimple = React.forwardRef(function (props, ref) {
    const config = useSelector(({fuse}) => fuse.settings.current.layout.config);

    // console.info("render::FusePageSimple");
    const leftSidebarRef = useRef(null);
    const rightSidebarRef = useRef(null);
    const rootRef = useRef(null);
    const classes = useStyles(props);

    React.useImperativeHandle(ref, () => {
        return {
            rootRef           : rootRef,
            toggleLeftSidebar : () => {
                leftSidebarRef.current.toggleSidebar()
            },
            toggleRightSidebar: () => {
                rightSidebarRef.current.toggleSidebar()
            }
        }
    });

    return (
        <div className={clsx(classes.root, props.innerScroll && classes.innerScroll)} ref={rootRef}>

            <div className={clsx(classes.header, classes.topBg)}/>

            <div className="flex flex-auto flex-col container z-10">

                {props.header && props.sidebarInner && (
                    <FusePageSimpleHeader header={props.header} classes={classes}/>
                )}

                {config.verifyMessage.display && (
                    <MySnackbarContentWrapper
                        variant={config.verifyMessage.variant}
                        message={config.verifyMessage.message}
                        action={config.verifyMessage.action}
                    />
                )}

                <div className={classes.wrapper}>

                    {(props.leftSidebarHeader || props.leftSidebarContent) && (
                        <FusePageSimpleSidebar
                            position="left"
                            header={props.leftSidebarHeader}
                            content={props.leftSidebarContent}
                            variant={props.leftSidebarVariant || 'permanent'}
                            innerScroll={props.innerScroll}
                            sidebarInner={props.sidebarInner}
                            classes={classes}
                            ref={leftSidebarRef}
                            rootRef={rootRef}
                        />
                    )}

                    {/*<FuseScrollbars*/}
                    {/*    className={clsx(classes.contentCardWrapper, props.sidebarInner && classes.contentCardWrapperInnerSidebar)}*/}
                    {/*    enable={props.innerScroll && props.sidebarInner}*/}
                    {/*>*/}
                    <FuseScrollbars className={classes.contentWrapper} enable={props.innerScroll && !props.sidebarInner}>

                        {props.header && !props.sidebarInner && (
                            <FusePageSimpleHeader header={props.header} classes={classes}/>
                        )}

                        {props.contentToolbar && (
                            <div className={classes.toolbar}>
                                {props.contentToolbar}
                            </div>
                        )}

                        {props.content && (
                            <div className={classes.content}>
                                {props.content}
                            </div>
                        )}
                    </FuseScrollbars>
                    {/*</FuseScrollbars>*/}

                    {(props.rightSidebarHeader || props.rightSidebarContent) && (
                        <FusePageSimpleSidebar
                            position="right"
                            header={props.rightSidebarHeader}
                            content={props.rightSidebarContent}
                            variant={props.rightSidebarVariant || 'permanent'}
                            innerScroll={props.innerScroll}
                            sidebarInner={props.sidebarInner}
                            classes={classes}
                            ref={rightSidebarRef}
                            rootRef={rootRef}
                        />
                    )}

                </div>
            </div>
        </div>
    );
});

FusePageSimple.propTypes = {
    leftSidebarHeader  : PropTypes.node,
    leftSidebarContent : PropTypes.node,
    leftSidebarVariant : PropTypes.node,
    rightSidebarHeader : PropTypes.node,
    rightSidebarContent: PropTypes.node,
    rightSidebarVariant: PropTypes.node,
    header             : PropTypes.node,
    content            : PropTypes.node,
    contentToolbar     : PropTypes.node,
    sidebarInner       : PropTypes.bool,
    innerScroll        : PropTypes.bool
};

FusePageSimple.defaultProps = {};

export default React.memo(FusePageSimple);
